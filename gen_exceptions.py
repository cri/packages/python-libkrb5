#!/usr/bin/env python3

from krb5 import lib

FILE_TEMPLATE = """
from ..exceptions import Krb5Error
{exceptions}
"""

EXCEPTION_TEMPLATE = """
class {exc_name}(Krb5Error):
    name = "{error_name}"
    error_code = {error_code}
"""

EXCLUSIONS = (
    "FALSE",
    "FLG",
    "KEYUSAGE",
    "OPT",
    "RESERVED",
    "TRUE",
    "TYPE",
)

exceptions = []
for error_name in lib.__all__:
    error_code = getattr(lib, error_name, None)
    if not isinstance(error_code, int):
        continue
    for exclusion in EXCLUSIONS:
        if exclusion in error_name:
            break
    else:
        exc_name = error_name.replace("_", " ").title().replace(" ", "")
        exceptions.append(
            EXCEPTION_TEMPLATE.format(
                exc_name=exc_name,
                error_name=error_name,
                error_code=error_code,
            ),
        )

print(FILE_TEMPLATE.format(exceptions="\n".join(exceptions)))
