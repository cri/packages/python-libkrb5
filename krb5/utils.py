from ctypes import cast, c_char_p


def to_string(value):
    from .wrappers.data import Krb5DataWrapper

    if value is None or isinstance(value, str):
        return value
    if isinstance(value, Krb5DataWrapper):
        value = value.object.contents.data[: value.object.contents.length]
    p = cast(value, c_char_p)
    if p is None:
        raise ValueError(f"Pointer {p} is NULL")
    return p.value.decode("utf-8")

def to_principal(context, principal):
    from .wrappers.principal import Krb5PrincipalWrapper

    if principal is None or isinstance(principal, Krb5PrincipalWrapper):
        return principal
    if isinstance(principal, str):
        return Krb5PrincipalWrapper.parse(context, principal)
    raise TypeError("{type(principal)} cannot be converted")

def to_data(context, data):
    from .wrappers import data as data_wrappers
    from .wrappers.principal import Krb5PrincipalWrapper

    if data is None or isinstance(data, data_wrappers.Krb5DataWrapper):
        return data
    if data is None or isinstance(data, Krb5PrincipalWrapper):
        data = str(data)
    if isinstance(data, str):
        return data_wrappers.Krb5StaticDataStringWrapper.from_string(
            context,
            data,
        )
    raise TypeError("{type(principal)} cannot be converted")
