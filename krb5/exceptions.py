from . import lib
from .utils import to_string

_EXCEPTIONS_MAP = {}

def register_exception(exc):
    error_code = getattr(exc, "error_code", None)
    if error_code is not None:
        _EXCEPTIONS_MAP.setdefault(error_code, exc)
    return exc


def get_exception(*args, **kwargs):
    if "error_code" not in kwargs:
        raise TypeError("mising error_code")
    exc = _EXCEPTIONS_MAP.get(kwargs["error_code"])
    if exc is None:
        exc = Krb5ErrorWithContext if "context" in kwargs else Krb5Error
        return exc(*args, **kwargs)
    bases = [exc]
    if "context" in kwargs:
        bases = [Krb5ErrorWithContextMixin] + bases
    return type(
        exc.__name__,
        tuple(bases),
        {},
    )(*args, **kwargs)

def check_code_without_context(code):
    if code == 0 or code is None:
        return
    raise get_exception(error_code=code)



class Krb5ErrorMeta(type):
    def __new__(cls, name, bases, dct):
        return register_exception(super().__new__(cls, name, bases, dct))


class Krb5Error(RuntimeError, metaclass=Krb5ErrorMeta):
    def __init__(self, *args, error_code, **kwargs):
        self.error_code = error_code
        super().__init__(*args, **kwargs)
        self.message = self.get_message()

    def __str__(self):
        return self.message

    def get_message(self):
        if hasattr(self, "name"):
            return f"Kerberos error: {self.name} ({self.error_code})"
        return f"krb5 error: code={self.error_code}"


class Krb5ErrorWithContextMixin:
    def __init__(self, *args, context, **kwargs):
        self.context = context
        super().__init__(*args, **kwargs)

    def get_message(self):
        _message = lib.krb5_get_error_message(self.context, self.error_code)
        message = to_string(_message)
        lib.krb5_free_error_message(self.context, _message)
        return f"Kerberos error: {message}"


class Krb5ErrorWithContext(Krb5ErrorWithContextMixin, Krb5Error):
    pass


from krb5.wrappers.exceptions import *  # noqa
