# -*- coding: utf-8 -*-
#
# TARGET arch is: ['-I', '/usr/include/']
# WORD_SIZE is: 8
# POINTER_SIZE is: 8
# LONGDOUBLE_SIZE is: 16
#
import ctypes


# if local wordsize is same as target, keep ctypes pointer function.
if ctypes.sizeof(ctypes.c_void_p) == 8:
    POINTER_T = ctypes.POINTER
else:
    # required to access _ctypes
    import _ctypes

    # Emulate a pointer class using the approriate c_int32/c_int64 type
    # The new class should have :
    # ['__module__', 'from_param', '_type_', '__dict__', '__weakref__', '__doc__']
    # but the class should be submitted to a unique instance for each base type
    # to that if A == B, POINTER_T(A) == POINTER_T(B)
    ctypes._pointer_t_type_cache = {}

    def POINTER_T(pointee):
        # a pointer should have the same length as LONG
        fake_ptr_base_type = ctypes.c_uint64
        # specific case for c_void_p
        if pointee is None:  # VOID pointer type. c_void_p.
            pointee = type(None)  # ctypes.c_void_p # ctypes.c_ulong
            clsname = "c_void"
        else:
            clsname = pointee.__name__
        if clsname in ctypes._pointer_t_type_cache:
            return ctypes._pointer_t_type_cache[clsname]
        # make template
        class _T(
            _ctypes._SimpleCData,
        ):
            _type_ = "L"
            _subtype_ = pointee

            def _sub_addr_(self):
                return self.value

            def __repr__(self):
                return "%s(%d)" % (clsname, self.value)

            def contents(self):
                raise TypeError("This is not a ctypes pointer.")

            def __init__(self, **args):
                raise TypeError("This is not a ctypes pointer. It is not instanciable.")

        _class = type("LP_%d_%s" % (8, clsname), (_T,), {})
        ctypes._pointer_t_type_cache[clsname] = _class
        return _class


c_int128 = ctypes.c_ubyte * 16
c_uint128 = c_int128
void = None
if ctypes.sizeof(ctypes.c_longdouble) == 16:
    c_long_double_t = ctypes.c_longdouble
else:
    c_long_double_t = ctypes.c_ubyte * 16

_libraries = {}
_libraries["libkrb5.so"] = ctypes.CDLL("libkrb5.so")


class struct_error_table(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("msgs", POINTER_T(POINTER_T(ctypes.c_char))),
        ("base", ctypes.c_int64),
        ("n_msgs", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
    ]


class struct_et_list(ctypes.Structure):
    pass


class struct__profile_t(ctypes.Structure):
    pass


krb5_octet = ctypes.c_ubyte
krb5_int16 = ctypes.c_int16
krb5_ui_2 = ctypes.c_uint16
krb5_int32 = ctypes.c_int32
krb5_ui_4 = ctypes.c_uint32
krb5_boolean = ctypes.c_uint32
krb5_msgtype = ctypes.c_uint32
krb5_kvno = ctypes.c_uint32
krb5_addrtype = ctypes.c_int32
krb5_enctype = ctypes.c_int32
krb5_cksumtype = ctypes.c_int32
krb5_authdatatype = ctypes.c_int32
krb5_keyusage = ctypes.c_int32
krb5_cryptotype = ctypes.c_int32
krb5_preauthtype = ctypes.c_int32
krb5_flags = ctypes.c_int32
krb5_timestamp = ctypes.c_int32
krb5_deltat = ctypes.c_int32
krb5_error_code = ctypes.c_int32
krb5_magic = ctypes.c_int32


class struct__krb5_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("length", ctypes.c_uint32),
        ("data", POINTER_T(ctypes.c_char)),
    ]


krb5_data = struct__krb5_data


class struct__krb5_octet_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("length", ctypes.c_uint32),
        ("data", POINTER_T(ctypes.c_ubyte)),
    ]


krb5_octet_data = struct__krb5_octet_data
krb5_pointer = POINTER_T(None)
krb5_const_pointer = POINTER_T(None)


class struct_krb5_principal_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("realm", krb5_data),
        ("data", POINTER_T(struct__krb5_data)),
        ("length", ctypes.c_int32),
        ("type", ctypes.c_int32),
    ]


krb5_principal_data = struct_krb5_principal_data
krb5_principal = POINTER_T(struct_krb5_principal_data)
krb5_const_principal = POINTER_T(struct_krb5_principal_data)
krb5_is_referral_realm = _libraries["libkrb5.so"].krb5_is_referral_realm
krb5_is_referral_realm.restype = krb5_boolean
krb5_is_referral_realm.argtypes = [POINTER_T(struct__krb5_data)]
krb5_anonymous_realm = _libraries["libkrb5.so"].krb5_anonymous_realm
krb5_anonymous_realm.restype = POINTER_T(struct__krb5_data)
krb5_anonymous_realm.argtypes = []
krb5_anonymous_principal = _libraries["libkrb5.so"].krb5_anonymous_principal
krb5_anonymous_principal.restype = krb5_const_principal
krb5_anonymous_principal.argtypes = []


class struct__krb5_address(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("addrtype", ctypes.c_int32),
        ("length", ctypes.c_uint32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("contents", POINTER_T(ctypes.c_ubyte)),
    ]


krb5_address = struct__krb5_address


class struct__krb5_context(ctypes.Structure):
    pass


krb5_context = POINTER_T(struct__krb5_context)


class struct__krb5_auth_context(ctypes.Structure):
    pass


krb5_auth_context = POINTER_T(struct__krb5_auth_context)


class struct__krb5_cryptosystem_entry(ctypes.Structure):
    pass


class struct__krb5_keyblock(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("enctype", ctypes.c_int32),
        ("length", ctypes.c_uint32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("contents", POINTER_T(ctypes.c_ubyte)),
    ]


krb5_keyblock = struct__krb5_keyblock


class struct_krb5_key_st(ctypes.Structure):
    pass


krb5_key = POINTER_T(struct_krb5_key_st)


class struct__krb5_encrypt_block(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("crypto_entry", ctypes.c_int32),
        ("key", POINTER_T(struct__krb5_keyblock)),
    ]


krb5_encrypt_block = struct__krb5_encrypt_block


class struct__krb5_checksum(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("checksum_type", ctypes.c_int32),
        ("length", ctypes.c_uint32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("contents", POINTER_T(ctypes.c_ubyte)),
    ]


krb5_checksum = struct__krb5_checksum


class struct__krb5_enc_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("enctype", ctypes.c_int32),
        ("kvno", ctypes.c_uint32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("ciphertext", krb5_data),
    ]


krb5_enc_data = struct__krb5_enc_data


class struct__krb5_crypto_iov(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("flags", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("data", krb5_data),
    ]


krb5_crypto_iov = struct__krb5_crypto_iov

# values for enumeration 'c__Ea_KRB5_C_RANDSOURCE_OLDAPI'
c__Ea_KRB5_C_RANDSOURCE_OLDAPI__enumvalues = {
    0: "KRB5_C_RANDSOURCE_OLDAPI",
    1: "KRB5_C_RANDSOURCE_OSRAND",
    2: "KRB5_C_RANDSOURCE_TRUSTEDPARTY",
    3: "KRB5_C_RANDSOURCE_TIMING",
    4: "KRB5_C_RANDSOURCE_EXTERNAL_PROTOCOL",
    5: "KRB5_C_RANDSOURCE_MAX",
}
KRB5_C_RANDSOURCE_OLDAPI = 0
KRB5_C_RANDSOURCE_OSRAND = 1
KRB5_C_RANDSOURCE_TRUSTEDPARTY = 2
KRB5_C_RANDSOURCE_TIMING = 3
KRB5_C_RANDSOURCE_EXTERNAL_PROTOCOL = 4
KRB5_C_RANDSOURCE_MAX = 5
c__Ea_KRB5_C_RANDSOURCE_OLDAPI = ctypes.c_int  # enum
krb5_c_encrypt = _libraries["libkrb5.so"].krb5_c_encrypt
krb5_c_encrypt.restype = krb5_error_code
krb5_c_encrypt.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_enc_data),
]
krb5_c_decrypt = _libraries["libkrb5.so"].krb5_c_decrypt
krb5_c_decrypt.restype = krb5_error_code
krb5_c_decrypt.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_enc_data),
    POINTER_T(struct__krb5_data),
]
krb5_c_encrypt_length = _libraries["libkrb5.so"].krb5_c_encrypt_length
krb5_c_encrypt_length.restype = krb5_error_code
krb5_c_encrypt_length.argtypes = [
    krb5_context,
    krb5_enctype,
    ctypes.c_int32,
    POINTER_T(ctypes.c_int32),
]
krb5_c_block_size = _libraries["libkrb5.so"].krb5_c_block_size
krb5_c_block_size.restype = krb5_error_code
krb5_c_block_size.argtypes = [krb5_context, krb5_enctype, POINTER_T(ctypes.c_int32)]
krb5_c_keylengths = _libraries["libkrb5.so"].krb5_c_keylengths
krb5_c_keylengths.restype = krb5_error_code
krb5_c_keylengths.argtypes = [
    krb5_context,
    krb5_enctype,
    POINTER_T(ctypes.c_int32),
    POINTER_T(ctypes.c_int32),
]
krb5_c_init_state = _libraries["libkrb5.so"].krb5_c_init_state
krb5_c_init_state.restype = krb5_error_code
krb5_c_init_state.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
]
krb5_c_free_state = _libraries["libkrb5.so"].krb5_c_free_state
krb5_c_free_state.restype = krb5_error_code
krb5_c_free_state.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_data),
]
krb5_c_prf = _libraries["libkrb5.so"].krb5_c_prf
krb5_c_prf.restype = krb5_error_code
krb5_c_prf.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
]
krb5_c_prf_length = _libraries["libkrb5.so"].krb5_c_prf_length
krb5_c_prf_length.restype = krb5_error_code
krb5_c_prf_length.argtypes = [krb5_context, krb5_enctype, POINTER_T(ctypes.c_int32)]
krb5_c_prfplus = _libraries["libkrb5.so"].krb5_c_prfplus
krb5_c_prfplus.restype = krb5_error_code
krb5_c_prfplus.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
]
krb5_c_derive_prfplus = _libraries["libkrb5.so"].krb5_c_derive_prfplus
krb5_c_derive_prfplus.restype = krb5_error_code
krb5_c_derive_prfplus.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_data),
    krb5_enctype,
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_c_fx_cf2_simple = _libraries["libkrb5.so"].krb5_c_fx_cf2_simple
krb5_c_fx_cf2_simple.restype = krb5_error_code
krb5_c_fx_cf2_simple.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(ctypes.c_char),
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_c_make_random_key = _libraries["libkrb5.so"].krb5_c_make_random_key
krb5_c_make_random_key.restype = krb5_error_code
krb5_c_make_random_key.argtypes = [
    krb5_context,
    krb5_enctype,
    POINTER_T(struct__krb5_keyblock),
]
krb5_c_random_to_key = _libraries["libkrb5.so"].krb5_c_random_to_key
krb5_c_random_to_key.restype = krb5_error_code
krb5_c_random_to_key.argtypes = [
    krb5_context,
    krb5_enctype,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_keyblock),
]
krb5_c_random_add_entropy = _libraries["libkrb5.so"].krb5_c_random_add_entropy
krb5_c_random_add_entropy.restype = krb5_error_code
krb5_c_random_add_entropy.argtypes = [
    krb5_context,
    ctypes.c_uint32,
    POINTER_T(struct__krb5_data),
]
krb5_c_random_make_octets = _libraries["libkrb5.so"].krb5_c_random_make_octets
krb5_c_random_make_octets.restype = krb5_error_code
krb5_c_random_make_octets.argtypes = [krb5_context, POINTER_T(struct__krb5_data)]
krb5_c_random_os_entropy = _libraries["libkrb5.so"].krb5_c_random_os_entropy
krb5_c_random_os_entropy.restype = krb5_error_code
krb5_c_random_os_entropy.argtypes = [
    krb5_context,
    ctypes.c_int32,
    POINTER_T(ctypes.c_int32),
]
krb5_c_random_seed = _libraries["libkrb5.so"].krb5_c_random_seed
krb5_c_random_seed.restype = krb5_error_code
krb5_c_random_seed.argtypes = [krb5_context, POINTER_T(struct__krb5_data)]
krb5_c_string_to_key = _libraries["libkrb5.so"].krb5_c_string_to_key
krb5_c_string_to_key.restype = krb5_error_code
krb5_c_string_to_key.argtypes = [
    krb5_context,
    krb5_enctype,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_keyblock),
]
krb5_c_string_to_key_with_params = _libraries[
    "libkrb5.so"
].krb5_c_string_to_key_with_params
krb5_c_string_to_key_with_params.restype = krb5_error_code
krb5_c_string_to_key_with_params.argtypes = [
    krb5_context,
    krb5_enctype,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_keyblock),
]
krb5_c_enctype_compare = _libraries["libkrb5.so"].krb5_c_enctype_compare
krb5_c_enctype_compare.restype = krb5_error_code
krb5_c_enctype_compare.argtypes = [
    krb5_context,
    krb5_enctype,
    krb5_enctype,
    POINTER_T(ctypes.c_uint32),
]
krb5_c_make_checksum = _libraries["libkrb5.so"].krb5_c_make_checksum
krb5_c_make_checksum.restype = krb5_error_code
krb5_c_make_checksum.argtypes = [
    krb5_context,
    krb5_cksumtype,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_checksum),
]
krb5_c_verify_checksum = _libraries["libkrb5.so"].krb5_c_verify_checksum
krb5_c_verify_checksum.restype = krb5_error_code
krb5_c_verify_checksum.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_checksum),
    POINTER_T(ctypes.c_uint32),
]
krb5_c_checksum_length = _libraries["libkrb5.so"].krb5_c_checksum_length
krb5_c_checksum_length.restype = krb5_error_code
krb5_c_checksum_length.argtypes = [
    krb5_context,
    krb5_cksumtype,
    POINTER_T(ctypes.c_int32),
]
krb5_c_keyed_checksum_types = _libraries["libkrb5.so"].krb5_c_keyed_checksum_types
krb5_c_keyed_checksum_types.restype = krb5_error_code
krb5_c_keyed_checksum_types.argtypes = [
    krb5_context,
    krb5_enctype,
    POINTER_T(ctypes.c_uint32),
    POINTER_T(POINTER_T(ctypes.c_int32)),
]
krb5_c_valid_enctype = _libraries["libkrb5.so"].krb5_c_valid_enctype
krb5_c_valid_enctype.restype = krb5_boolean
krb5_c_valid_enctype.argtypes = [krb5_enctype]
krb5_c_valid_cksumtype = _libraries["libkrb5.so"].krb5_c_valid_cksumtype
krb5_c_valid_cksumtype.restype = krb5_boolean
krb5_c_valid_cksumtype.argtypes = [krb5_cksumtype]
krb5_c_is_coll_proof_cksum = _libraries["libkrb5.so"].krb5_c_is_coll_proof_cksum
krb5_c_is_coll_proof_cksum.restype = krb5_boolean
krb5_c_is_coll_proof_cksum.argtypes = [krb5_cksumtype]
krb5_c_is_keyed_cksum = _libraries["libkrb5.so"].krb5_c_is_keyed_cksum
krb5_c_is_keyed_cksum.restype = krb5_boolean
krb5_c_is_keyed_cksum.argtypes = [krb5_cksumtype]
krb5_c_make_checksum_iov = _libraries["libkrb5.so"].krb5_c_make_checksum_iov
krb5_c_make_checksum_iov.restype = krb5_error_code
krb5_c_make_checksum_iov.argtypes = [
    krb5_context,
    krb5_cksumtype,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
]
krb5_c_verify_checksum_iov = _libraries["libkrb5.so"].krb5_c_verify_checksum_iov
krb5_c_verify_checksum_iov.restype = krb5_error_code
krb5_c_verify_checksum_iov.argtypes = [
    krb5_context,
    krb5_cksumtype,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
    POINTER_T(ctypes.c_uint32),
]
krb5_c_encrypt_iov = _libraries["libkrb5.so"].krb5_c_encrypt_iov
krb5_c_encrypt_iov.restype = krb5_error_code
krb5_c_encrypt_iov.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
]
krb5_c_decrypt_iov = _libraries["libkrb5.so"].krb5_c_decrypt_iov
krb5_c_decrypt_iov.restype = krb5_error_code
krb5_c_decrypt_iov.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
]
krb5_c_crypto_length = _libraries["libkrb5.so"].krb5_c_crypto_length
krb5_c_crypto_length.restype = krb5_error_code
krb5_c_crypto_length.argtypes = [
    krb5_context,
    krb5_enctype,
    krb5_cryptotype,
    POINTER_T(ctypes.c_uint32),
]
krb5_c_crypto_length_iov = _libraries["libkrb5.so"].krb5_c_crypto_length_iov
krb5_c_crypto_length_iov.restype = krb5_error_code
krb5_c_crypto_length_iov.argtypes = [
    krb5_context,
    krb5_enctype,
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
]
krb5_c_padding_length = _libraries["libkrb5.so"].krb5_c_padding_length
krb5_c_padding_length.restype = krb5_error_code
krb5_c_padding_length.argtypes = [
    krb5_context,
    krb5_enctype,
    ctypes.c_int32,
    POINTER_T(ctypes.c_uint32),
]
krb5_k_create_key = _libraries["libkrb5.so"].krb5_k_create_key
krb5_k_create_key.restype = krb5_error_code
krb5_k_create_key.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(POINTER_T(struct_krb5_key_st)),
]
krb5_k_reference_key = _libraries["libkrb5.so"].krb5_k_reference_key
krb5_k_reference_key.restype = None
krb5_k_reference_key.argtypes = [krb5_context, krb5_key]
krb5_k_free_key = _libraries["libkrb5.so"].krb5_k_free_key
krb5_k_free_key.restype = None
krb5_k_free_key.argtypes = [krb5_context, krb5_key]
krb5_k_key_keyblock = _libraries["libkrb5.so"].krb5_k_key_keyblock
krb5_k_key_keyblock.restype = krb5_error_code
krb5_k_key_keyblock.argtypes = [
    krb5_context,
    krb5_key,
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_k_key_enctype = _libraries["libkrb5.so"].krb5_k_key_enctype
krb5_k_key_enctype.restype = krb5_enctype
krb5_k_key_enctype.argtypes = [krb5_context, krb5_key]
krb5_k_encrypt = _libraries["libkrb5.so"].krb5_k_encrypt
krb5_k_encrypt.restype = krb5_error_code
krb5_k_encrypt.argtypes = [
    krb5_context,
    krb5_key,
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_enc_data),
]
krb5_k_encrypt_iov = _libraries["libkrb5.so"].krb5_k_encrypt_iov
krb5_k_encrypt_iov.restype = krb5_error_code
krb5_k_encrypt_iov.argtypes = [
    krb5_context,
    krb5_key,
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
]
krb5_k_decrypt = _libraries["libkrb5.so"].krb5_k_decrypt
krb5_k_decrypt.restype = krb5_error_code
krb5_k_decrypt.argtypes = [
    krb5_context,
    krb5_key,
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_enc_data),
    POINTER_T(struct__krb5_data),
]
krb5_k_decrypt_iov = _libraries["libkrb5.so"].krb5_k_decrypt_iov
krb5_k_decrypt_iov.restype = krb5_error_code
krb5_k_decrypt_iov.argtypes = [
    krb5_context,
    krb5_key,
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
]
krb5_k_make_checksum = _libraries["libkrb5.so"].krb5_k_make_checksum
krb5_k_make_checksum.restype = krb5_error_code
krb5_k_make_checksum.argtypes = [
    krb5_context,
    krb5_cksumtype,
    krb5_key,
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_checksum),
]
krb5_k_make_checksum_iov = _libraries["libkrb5.so"].krb5_k_make_checksum_iov
krb5_k_make_checksum_iov.restype = krb5_error_code
krb5_k_make_checksum_iov.argtypes = [
    krb5_context,
    krb5_cksumtype,
    krb5_key,
    krb5_keyusage,
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
]
krb5_k_verify_checksum = _libraries["libkrb5.so"].krb5_k_verify_checksum
krb5_k_verify_checksum.restype = krb5_error_code
krb5_k_verify_checksum.argtypes = [
    krb5_context,
    krb5_key,
    krb5_keyusage,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_checksum),
    POINTER_T(ctypes.c_uint32),
]
krb5_k_verify_checksum_iov = _libraries["libkrb5.so"].krb5_k_verify_checksum_iov
krb5_k_verify_checksum_iov.restype = krb5_error_code
krb5_k_verify_checksum_iov.argtypes = [
    krb5_context,
    krb5_cksumtype,
    krb5_key,
    krb5_keyusage,
    POINTER_T(struct__krb5_crypto_iov),
    ctypes.c_int32,
    POINTER_T(ctypes.c_uint32),
]
krb5_k_prf = _libraries["libkrb5.so"].krb5_k_prf
krb5_k_prf.restype = krb5_error_code
krb5_k_prf.argtypes = [
    krb5_context,
    krb5_key,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
]
krb5_encrypt = _libraries["libkrb5.so"].krb5_encrypt
krb5_encrypt.restype = krb5_error_code
krb5_encrypt.argtypes = [
    krb5_context,
    krb5_const_pointer,
    krb5_pointer,
    ctypes.c_int32,
    POINTER_T(struct__krb5_encrypt_block),
    krb5_pointer,
]
krb5_decrypt = _libraries["libkrb5.so"].krb5_decrypt
krb5_decrypt.restype = krb5_error_code
krb5_decrypt.argtypes = [
    krb5_context,
    krb5_const_pointer,
    krb5_pointer,
    ctypes.c_int32,
    POINTER_T(struct__krb5_encrypt_block),
    krb5_pointer,
]
krb5_process_key = _libraries["libkrb5.so"].krb5_process_key
krb5_process_key.restype = krb5_error_code
krb5_process_key.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_encrypt_block),
    POINTER_T(struct__krb5_keyblock),
]
krb5_finish_key = _libraries["libkrb5.so"].krb5_finish_key
krb5_finish_key.restype = krb5_error_code
krb5_finish_key.argtypes = [krb5_context, POINTER_T(struct__krb5_encrypt_block)]
krb5_string_to_key = _libraries["libkrb5.so"].krb5_string_to_key
krb5_string_to_key.restype = krb5_error_code
krb5_string_to_key.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_encrypt_block),
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
]
krb5_init_random_key = _libraries["libkrb5.so"].krb5_init_random_key
krb5_init_random_key.restype = krb5_error_code
krb5_init_random_key.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_encrypt_block),
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(POINTER_T(None)),
]
krb5_finish_random_key = _libraries["libkrb5.so"].krb5_finish_random_key
krb5_finish_random_key.restype = krb5_error_code
krb5_finish_random_key.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_encrypt_block),
    POINTER_T(POINTER_T(None)),
]
krb5_random_key = _libraries["libkrb5.so"].krb5_random_key
krb5_random_key.restype = krb5_error_code
krb5_random_key.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_encrypt_block),
    krb5_pointer,
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_eblock_enctype = _libraries["libkrb5.so"].krb5_eblock_enctype
krb5_eblock_enctype.restype = krb5_enctype
krb5_eblock_enctype.argtypes = [krb5_context, POINTER_T(struct__krb5_encrypt_block)]
krb5_use_enctype = _libraries["libkrb5.so"].krb5_use_enctype
krb5_use_enctype.restype = krb5_error_code
krb5_use_enctype.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_encrypt_block),
    krb5_enctype,
]
krb5_encrypt_size = _libraries["libkrb5.so"].krb5_encrypt_size
krb5_encrypt_size.restype = ctypes.c_int32
krb5_encrypt_size.argtypes = [ctypes.c_int32, krb5_enctype]
krb5_checksum_size = _libraries["libkrb5.so"].krb5_checksum_size
krb5_checksum_size.restype = ctypes.c_int32
krb5_checksum_size.argtypes = [krb5_context, krb5_cksumtype]
krb5_calculate_checksum = _libraries["libkrb5.so"].krb5_calculate_checksum
krb5_calculate_checksum.restype = krb5_error_code
krb5_calculate_checksum.argtypes = [
    krb5_context,
    krb5_cksumtype,
    krb5_const_pointer,
    ctypes.c_int32,
    krb5_const_pointer,
    ctypes.c_int32,
    POINTER_T(struct__krb5_checksum),
]
krb5_verify_checksum = _libraries["libkrb5.so"].krb5_verify_checksum
krb5_verify_checksum.restype = krb5_error_code
krb5_verify_checksum.argtypes = [
    krb5_context,
    krb5_cksumtype,
    POINTER_T(struct__krb5_checksum),
    krb5_const_pointer,
    ctypes.c_int32,
    krb5_const_pointer,
    ctypes.c_int32,
]


class struct__krb5_ticket_times(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("authtime", ctypes.c_int32),
        ("starttime", ctypes.c_int32),
        ("endtime", ctypes.c_int32),
        ("renew_till", ctypes.c_int32),
    ]


krb5_ticket_times = struct__krb5_ticket_times


class struct__krb5_authdata(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("ad_type", ctypes.c_int32),
        ("length", ctypes.c_uint32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("contents", POINTER_T(ctypes.c_ubyte)),
    ]


krb5_authdata = struct__krb5_authdata


class struct__krb5_transited(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("tr_type", ctypes.c_ubyte),
        ("PADDING_0", ctypes.c_ubyte * 3),
        ("tr_contents", krb5_data),
    ]


krb5_transited = struct__krb5_transited


class struct__krb5_enc_tkt_part(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("flags", ctypes.c_int32),
        ("session", POINTER_T(struct__krb5_keyblock)),
        ("client", POINTER_T(struct_krb5_principal_data)),
        ("transited", krb5_transited),
        ("times", krb5_ticket_times),
        ("caddrs", POINTER_T(POINTER_T(struct__krb5_address))),
        ("authorization_data", POINTER_T(POINTER_T(struct__krb5_authdata))),
    ]


krb5_enc_tkt_part = struct__krb5_enc_tkt_part


class struct__krb5_ticket(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("server", POINTER_T(struct_krb5_principal_data)),
        ("enc_part", krb5_enc_data),
        ("enc_part2", POINTER_T(struct__krb5_enc_tkt_part)),
    ]


krb5_ticket = struct__krb5_ticket


class struct__krb5_authenticator(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("client", POINTER_T(struct_krb5_principal_data)),
        ("checksum", POINTER_T(struct__krb5_checksum)),
        ("cusec", ctypes.c_int32),
        ("ctime", ctypes.c_int32),
        ("subkey", POINTER_T(struct__krb5_keyblock)),
        ("seq_number", ctypes.c_uint32),
        ("PADDING_1", ctypes.c_ubyte * 4),
        ("authorization_data", POINTER_T(POINTER_T(struct__krb5_authdata))),
    ]


krb5_authenticator = struct__krb5_authenticator


class struct__krb5_tkt_authent(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("ticket", POINTER_T(struct__krb5_ticket)),
        ("authenticator", POINTER_T(struct__krb5_authenticator)),
        ("ap_options", ctypes.c_int32),
        ("PADDING_1", ctypes.c_ubyte * 4),
    ]


krb5_tkt_authent = struct__krb5_tkt_authent


class struct__krb5_creds(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("client", POINTER_T(struct_krb5_principal_data)),
        ("server", POINTER_T(struct_krb5_principal_data)),
        ("keyblock", krb5_keyblock),
        ("times", krb5_ticket_times),
        ("is_skey", ctypes.c_uint32),
        ("ticket_flags", ctypes.c_int32),
        ("addresses", POINTER_T(POINTER_T(struct__krb5_address))),
        ("ticket", krb5_data),
        ("second_ticket", krb5_data),
        ("authdata", POINTER_T(POINTER_T(struct__krb5_authdata))),
    ]


krb5_creds = struct__krb5_creds


class struct__krb5_last_req_entry(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("lr_type", ctypes.c_int32),
        ("value", ctypes.c_int32),
    ]


krb5_last_req_entry = struct__krb5_last_req_entry


class struct__krb5_pa_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("pa_type", ctypes.c_int32),
        ("length", ctypes.c_uint32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("contents", POINTER_T(ctypes.c_ubyte)),
    ]


krb5_pa_data = struct__krb5_pa_data


class struct__krb5_typed_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("type", ctypes.c_int32),
        ("length", ctypes.c_uint32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("data", POINTER_T(ctypes.c_ubyte)),
    ]


krb5_typed_data = struct__krb5_typed_data


class struct__krb5_kdc_req(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("msg_type", ctypes.c_uint32),
        ("padata", POINTER_T(POINTER_T(struct__krb5_pa_data))),
        ("kdc_options", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("client", POINTER_T(struct_krb5_principal_data)),
        ("server", POINTER_T(struct_krb5_principal_data)),
        ("from", ctypes.c_int32),
        ("till", ctypes.c_int32),
        ("rtime", ctypes.c_int32),
        ("nonce", ctypes.c_int32),
        ("nktypes", ctypes.c_int32),
        ("PADDING_1", ctypes.c_ubyte * 4),
        ("ktype", POINTER_T(ctypes.c_int32)),
        ("addresses", POINTER_T(POINTER_T(struct__krb5_address))),
        ("authorization_data", krb5_enc_data),
        ("unenc_authdata", POINTER_T(POINTER_T(struct__krb5_authdata))),
        ("second_ticket", POINTER_T(POINTER_T(struct__krb5_ticket))),
    ]


krb5_kdc_req = struct__krb5_kdc_req


class struct__krb5_enc_kdc_rep_part(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("msg_type", ctypes.c_uint32),
        ("session", POINTER_T(struct__krb5_keyblock)),
        ("last_req", POINTER_T(POINTER_T(struct__krb5_last_req_entry))),
        ("nonce", ctypes.c_int32),
        ("key_exp", ctypes.c_int32),
        ("flags", ctypes.c_int32),
        ("times", krb5_ticket_times),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("server", POINTER_T(struct_krb5_principal_data)),
        ("caddrs", POINTER_T(POINTER_T(struct__krb5_address))),
        ("enc_padata", POINTER_T(POINTER_T(struct__krb5_pa_data))),
    ]


krb5_enc_kdc_rep_part = struct__krb5_enc_kdc_rep_part


class struct__krb5_kdc_rep(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("msg_type", ctypes.c_uint32),
        ("padata", POINTER_T(POINTER_T(struct__krb5_pa_data))),
        ("client", POINTER_T(struct_krb5_principal_data)),
        ("ticket", POINTER_T(struct__krb5_ticket)),
        ("enc_part", krb5_enc_data),
        ("enc_part2", POINTER_T(struct__krb5_enc_kdc_rep_part)),
    ]


krb5_kdc_rep = struct__krb5_kdc_rep


class struct__krb5_error(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("ctime", ctypes.c_int32),
        ("cusec", ctypes.c_int32),
        ("susec", ctypes.c_int32),
        ("stime", ctypes.c_int32),
        ("error", ctypes.c_uint32),
        ("client", POINTER_T(struct_krb5_principal_data)),
        ("server", POINTER_T(struct_krb5_principal_data)),
        ("text", krb5_data),
        ("e_data", krb5_data),
    ]


krb5_error = struct__krb5_error


class struct__krb5_ap_req(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("ap_options", ctypes.c_int32),
        ("ticket", POINTER_T(struct__krb5_ticket)),
        ("authenticator", krb5_enc_data),
    ]


krb5_ap_req = struct__krb5_ap_req


class struct__krb5_ap_rep(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("enc_part", krb5_enc_data),
    ]


krb5_ap_rep = struct__krb5_ap_rep


class struct__krb5_ap_rep_enc_part(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("ctime", ctypes.c_int32),
        ("cusec", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("subkey", POINTER_T(struct__krb5_keyblock)),
        ("seq_number", ctypes.c_uint32),
        ("PADDING_1", ctypes.c_ubyte * 4),
    ]


krb5_ap_rep_enc_part = struct__krb5_ap_rep_enc_part


class struct__krb5_response(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("message_type", ctypes.c_ubyte),
        ("PADDING_0", ctypes.c_ubyte * 3),
        ("response", krb5_data),
        ("expected_nonce", ctypes.c_int32),
        ("request_time", ctypes.c_int32),
    ]


krb5_response = struct__krb5_response


class struct__krb5_cred_info(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("session", POINTER_T(struct__krb5_keyblock)),
        ("client", POINTER_T(struct_krb5_principal_data)),
        ("server", POINTER_T(struct_krb5_principal_data)),
        ("flags", ctypes.c_int32),
        ("times", krb5_ticket_times),
        ("PADDING_1", ctypes.c_ubyte * 4),
        ("caddrs", POINTER_T(POINTER_T(struct__krb5_address))),
    ]


krb5_cred_info = struct__krb5_cred_info


class struct__krb5_cred_enc_part(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("nonce", ctypes.c_int32),
        ("timestamp", ctypes.c_int32),
        ("usec", ctypes.c_int32),
        ("s_address", POINTER_T(struct__krb5_address)),
        ("r_address", POINTER_T(struct__krb5_address)),
        ("ticket_info", POINTER_T(POINTER_T(struct__krb5_cred_info))),
    ]


krb5_cred_enc_part = struct__krb5_cred_enc_part


class struct__krb5_cred(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("tickets", POINTER_T(POINTER_T(struct__krb5_ticket))),
        ("enc_part", krb5_enc_data),
        ("enc_part2", POINTER_T(struct__krb5_cred_enc_part)),
    ]


krb5_cred = struct__krb5_cred


class struct__passwd_phrase_element(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("passwd", POINTER_T(struct__krb5_data)),
        ("phrase", POINTER_T(struct__krb5_data)),
    ]


passwd_phrase_element = struct__passwd_phrase_element


class struct__krb5_pwd_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("sequence_count", ctypes.c_int32),
        ("element", POINTER_T(POINTER_T(struct__passwd_phrase_element))),
    ]


krb5_pwd_data = struct__krb5_pwd_data


class struct__krb5_pa_svr_referral_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("principal", POINTER_T(struct_krb5_principal_data)),
    ]


krb5_pa_svr_referral_data = struct__krb5_pa_svr_referral_data


class struct__krb5_pa_server_referral_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("referred_realm", POINTER_T(struct__krb5_data)),
        ("true_principal_name", POINTER_T(struct_krb5_principal_data)),
        ("requested_principal_name", POINTER_T(struct_krb5_principal_data)),
        ("referral_valid_until", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("rep_cksum", krb5_checksum),
    ]


krb5_pa_server_referral_data = struct__krb5_pa_server_referral_data


class struct__krb5_pa_pac_req(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("include_pac", ctypes.c_uint32),
    ]


krb5_pa_pac_req = struct__krb5_pa_pac_req


class struct_krb5_replay_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("timestamp", ctypes.c_int32),
        ("usec", ctypes.c_int32),
        ("seq", ctypes.c_uint32),
    ]


krb5_replay_data = struct_krb5_replay_data
krb5_mk_req_checksum_func = POINTER_T(
    ctypes.CFUNCTYPE(
        ctypes.c_int32,
        POINTER_T(struct__krb5_context),
        POINTER_T(struct__krb5_auth_context),
        POINTER_T(None),
        POINTER_T(POINTER_T(struct__krb5_data)),
    )
)
krb5_cc_cursor = POINTER_T(None)


class struct__krb5_ccache(ctypes.Structure):
    pass


krb5_ccache = POINTER_T(struct__krb5_ccache)


class struct__krb5_cc_ops(ctypes.Structure):
    pass


krb5_cc_ops = struct__krb5_cc_ops


class struct__krb5_cccol_cursor(ctypes.Structure):
    pass


krb5_cccol_cursor = POINTER_T(struct__krb5_cccol_cursor)
krb5_cc_get_name = _libraries["libkrb5.so"].krb5_cc_get_name
krb5_cc_get_name.restype = POINTER_T(ctypes.c_char)
krb5_cc_get_name.argtypes = [krb5_context, krb5_ccache]
krb5_cc_get_full_name = _libraries["libkrb5.so"].krb5_cc_get_full_name
krb5_cc_get_full_name.restype = krb5_error_code
krb5_cc_get_full_name.argtypes = [
    krb5_context,
    krb5_ccache,
    POINTER_T(POINTER_T(ctypes.c_char)),
]
krb5_cc_initialize = _libraries["libkrb5.so"].krb5_cc_initialize
krb5_cc_initialize.restype = krb5_error_code
krb5_cc_initialize.argtypes = [krb5_context, krb5_ccache, krb5_principal]
krb5_cc_destroy = _libraries["libkrb5.so"].krb5_cc_destroy
krb5_cc_destroy.restype = krb5_error_code
krb5_cc_destroy.argtypes = [krb5_context, krb5_ccache]
krb5_cc_close = _libraries["libkrb5.so"].krb5_cc_close
krb5_cc_close.restype = krb5_error_code
krb5_cc_close.argtypes = [krb5_context, krb5_ccache]
krb5_cc_store_cred = _libraries["libkrb5.so"].krb5_cc_store_cred
krb5_cc_store_cred.restype = krb5_error_code
krb5_cc_store_cred.argtypes = [krb5_context, krb5_ccache, POINTER_T(struct__krb5_creds)]
krb5_cc_retrieve_cred = _libraries["libkrb5.so"].krb5_cc_retrieve_cred
krb5_cc_retrieve_cred.restype = krb5_error_code
krb5_cc_retrieve_cred.argtypes = [
    krb5_context,
    krb5_ccache,
    krb5_flags,
    POINTER_T(struct__krb5_creds),
    POINTER_T(struct__krb5_creds),
]
krb5_cc_get_principal = _libraries["libkrb5.so"].krb5_cc_get_principal
krb5_cc_get_principal.restype = krb5_error_code
krb5_cc_get_principal.argtypes = [
    krb5_context,
    krb5_ccache,
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
]
krb5_cc_start_seq_get = _libraries["libkrb5.so"].krb5_cc_start_seq_get
krb5_cc_start_seq_get.restype = krb5_error_code
krb5_cc_start_seq_get.argtypes = [krb5_context, krb5_ccache, POINTER_T(POINTER_T(None))]
krb5_cc_next_cred = _libraries["libkrb5.so"].krb5_cc_next_cred
krb5_cc_next_cred.restype = krb5_error_code
krb5_cc_next_cred.argtypes = [
    krb5_context,
    krb5_ccache,
    POINTER_T(POINTER_T(None)),
    POINTER_T(struct__krb5_creds),
]
krb5_cc_end_seq_get = _libraries["libkrb5.so"].krb5_cc_end_seq_get
krb5_cc_end_seq_get.restype = krb5_error_code
krb5_cc_end_seq_get.argtypes = [krb5_context, krb5_ccache, POINTER_T(POINTER_T(None))]
krb5_cc_remove_cred = _libraries["libkrb5.so"].krb5_cc_remove_cred
krb5_cc_remove_cred.restype = krb5_error_code
krb5_cc_remove_cred.argtypes = [
    krb5_context,
    krb5_ccache,
    krb5_flags,
    POINTER_T(struct__krb5_creds),
]
krb5_cc_set_flags = _libraries["libkrb5.so"].krb5_cc_set_flags
krb5_cc_set_flags.restype = krb5_error_code
krb5_cc_set_flags.argtypes = [krb5_context, krb5_ccache, krb5_flags]
krb5_cc_get_type = _libraries["libkrb5.so"].krb5_cc_get_type
krb5_cc_get_type.restype = POINTER_T(ctypes.c_char)
krb5_cc_get_type.argtypes = [krb5_context, krb5_ccache]
krb5_cc_move = _libraries["libkrb5.so"].krb5_cc_move
krb5_cc_move.restype = krb5_error_code
krb5_cc_move.argtypes = [krb5_context, krb5_ccache, krb5_ccache]
krb5_cccol_cursor_new = _libraries["libkrb5.so"].krb5_cccol_cursor_new
krb5_cccol_cursor_new.restype = krb5_error_code
krb5_cccol_cursor_new.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_cccol_cursor)),
]
krb5_cccol_cursor_next = _libraries["libkrb5.so"].krb5_cccol_cursor_next
krb5_cccol_cursor_next.restype = krb5_error_code
krb5_cccol_cursor_next.argtypes = [
    krb5_context,
    krb5_cccol_cursor,
    POINTER_T(POINTER_T(struct__krb5_ccache)),
]
krb5_cccol_cursor_free = _libraries["libkrb5.so"].krb5_cccol_cursor_free
krb5_cccol_cursor_free.restype = krb5_error_code
krb5_cccol_cursor_free.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_cccol_cursor)),
]
krb5_cccol_have_content = _libraries["libkrb5.so"].krb5_cccol_have_content
krb5_cccol_have_content.restype = krb5_error_code
krb5_cccol_have_content.argtypes = [krb5_context]
krb5_cc_new_unique = _libraries["libkrb5.so"].krb5_cc_new_unique
krb5_cc_new_unique.restype = krb5_error_code
krb5_cc_new_unique.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(struct__krb5_ccache)),
]


class struct_krb5_rc_st(ctypes.Structure):
    pass


krb5_rcache = POINTER_T(struct_krb5_rc_st)
krb5_kt_cursor = POINTER_T(None)


class struct_krb5_keytab_entry_st(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("magic", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("principal", POINTER_T(struct_krb5_principal_data)),
        ("timestamp", ctypes.c_int32),
        ("vno", ctypes.c_uint32),
        ("key", krb5_keyblock),
    ]


krb5_keytab_entry = struct_krb5_keytab_entry_st


class struct__krb5_kt(ctypes.Structure):
    pass


krb5_keytab = POINTER_T(struct__krb5_kt)
krb5_kt_get_type = _libraries["libkrb5.so"].krb5_kt_get_type
krb5_kt_get_type.restype = POINTER_T(ctypes.c_char)
krb5_kt_get_type.argtypes = [krb5_context, krb5_keytab]
krb5_kt_get_name = _libraries["libkrb5.so"].krb5_kt_get_name
krb5_kt_get_name.restype = krb5_error_code
krb5_kt_get_name.argtypes = [
    krb5_context,
    krb5_keytab,
    POINTER_T(ctypes.c_char),
    ctypes.c_uint32,
]
krb5_kt_close = _libraries["libkrb5.so"].krb5_kt_close
krb5_kt_close.restype = krb5_error_code
krb5_kt_close.argtypes = [krb5_context, krb5_keytab]
krb5_kt_get_entry = _libraries["libkrb5.so"].krb5_kt_get_entry
krb5_kt_get_entry.restype = krb5_error_code
krb5_kt_get_entry.argtypes = [
    krb5_context,
    krb5_keytab,
    krb5_const_principal,
    krb5_kvno,
    krb5_enctype,
    POINTER_T(struct_krb5_keytab_entry_st),
]
krb5_kt_start_seq_get = _libraries["libkrb5.so"].krb5_kt_start_seq_get
krb5_kt_start_seq_get.restype = krb5_error_code
krb5_kt_start_seq_get.argtypes = [krb5_context, krb5_keytab, POINTER_T(POINTER_T(None))]
krb5_kt_next_entry = _libraries["libkrb5.so"].krb5_kt_next_entry
krb5_kt_next_entry.restype = krb5_error_code
krb5_kt_next_entry.argtypes = [
    krb5_context,
    krb5_keytab,
    POINTER_T(struct_krb5_keytab_entry_st),
    POINTER_T(POINTER_T(None)),
]
krb5_kt_end_seq_get = _libraries["libkrb5.so"].krb5_kt_end_seq_get
krb5_kt_end_seq_get.restype = krb5_error_code
krb5_kt_end_seq_get.argtypes = [krb5_context, krb5_keytab, POINTER_T(POINTER_T(None))]
krb5_kt_have_content = _libraries["libkrb5.so"].krb5_kt_have_content
krb5_kt_have_content.restype = krb5_error_code
krb5_kt_have_content.argtypes = [krb5_context, krb5_keytab]
krb5_init_context = _libraries["libkrb5.so"].krb5_init_context
krb5_init_context.restype = krb5_error_code
krb5_init_context.argtypes = [POINTER_T(POINTER_T(struct__krb5_context))]
krb5_init_secure_context = _libraries["libkrb5.so"].krb5_init_secure_context
krb5_init_secure_context.restype = krb5_error_code
krb5_init_secure_context.argtypes = [POINTER_T(POINTER_T(struct__krb5_context))]
krb5_init_context_profile = _libraries["libkrb5.so"].krb5_init_context_profile
krb5_init_context_profile.restype = krb5_error_code
krb5_init_context_profile.argtypes = [
    POINTER_T(struct__profile_t),
    krb5_flags,
    POINTER_T(POINTER_T(struct__krb5_context)),
]
krb5_free_context = _libraries["libkrb5.so"].krb5_free_context
krb5_free_context.restype = None
krb5_free_context.argtypes = [krb5_context]
krb5_copy_context = _libraries["libkrb5.so"].krb5_copy_context
krb5_copy_context.restype = krb5_error_code
krb5_copy_context.argtypes = [krb5_context, POINTER_T(POINTER_T(struct__krb5_context))]
krb5_set_default_tgs_enctypes = _libraries["libkrb5.so"].krb5_set_default_tgs_enctypes
krb5_set_default_tgs_enctypes.restype = krb5_error_code
krb5_set_default_tgs_enctypes.argtypes = [krb5_context, POINTER_T(ctypes.c_int32)]
krb5_get_permitted_enctypes = _libraries["libkrb5.so"].krb5_get_permitted_enctypes
krb5_get_permitted_enctypes.restype = krb5_error_code
krb5_get_permitted_enctypes.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(ctypes.c_int32)),
]
krb5_is_thread_safe = _libraries["libkrb5.so"].krb5_is_thread_safe
krb5_is_thread_safe.restype = krb5_boolean
krb5_is_thread_safe.argtypes = []
krb5_server_decrypt_ticket_keytab = _libraries[
    "libkrb5.so"
].krb5_server_decrypt_ticket_keytab
krb5_server_decrypt_ticket_keytab.restype = krb5_error_code
krb5_server_decrypt_ticket_keytab.argtypes = [
    krb5_context,
    krb5_keytab,
    POINTER_T(struct__krb5_ticket),
]
krb5_free_tgt_creds = _libraries["libkrb5.so"].krb5_free_tgt_creds
krb5_free_tgt_creds.restype = None
krb5_free_tgt_creds.argtypes = [krb5_context, POINTER_T(POINTER_T(struct__krb5_creds))]
krb5_get_credentials = _libraries["libkrb5.so"].krb5_get_credentials
krb5_get_credentials.restype = krb5_error_code
krb5_get_credentials.argtypes = [
    krb5_context,
    krb5_flags,
    krb5_ccache,
    POINTER_T(struct__krb5_creds),
    POINTER_T(POINTER_T(struct__krb5_creds)),
]
krb5_get_credentials_validate = _libraries["libkrb5.so"].krb5_get_credentials_validate
krb5_get_credentials_validate.restype = krb5_error_code
krb5_get_credentials_validate.argtypes = [
    krb5_context,
    krb5_flags,
    krb5_ccache,
    POINTER_T(struct__krb5_creds),
    POINTER_T(POINTER_T(struct__krb5_creds)),
]
krb5_get_credentials_renew = _libraries["libkrb5.so"].krb5_get_credentials_renew
krb5_get_credentials_renew.restype = krb5_error_code
krb5_get_credentials_renew.argtypes = [
    krb5_context,
    krb5_flags,
    krb5_ccache,
    POINTER_T(struct__krb5_creds),
    POINTER_T(POINTER_T(struct__krb5_creds)),
]
krb5_mk_req = _libraries["libkrb5.so"].krb5_mk_req
krb5_mk_req.restype = krb5_error_code
krb5_mk_req.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_auth_context)),
    krb5_flags,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    POINTER_T(struct__krb5_data),
    krb5_ccache,
    POINTER_T(struct__krb5_data),
]
krb5_mk_req_extended = _libraries["libkrb5.so"].krb5_mk_req_extended
krb5_mk_req_extended.restype = krb5_error_code
krb5_mk_req_extended.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_auth_context)),
    krb5_flags,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_creds),
    POINTER_T(struct__krb5_data),
]
krb5_mk_rep = _libraries["libkrb5.so"].krb5_mk_rep
krb5_mk_rep.restype = krb5_error_code
krb5_mk_rep.argtypes = [krb5_context, krb5_auth_context, POINTER_T(struct__krb5_data)]
krb5_mk_rep_dce = _libraries["libkrb5.so"].krb5_mk_rep_dce
krb5_mk_rep_dce.restype = krb5_error_code
krb5_mk_rep_dce.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_data),
]
krb5_rd_rep = _libraries["libkrb5.so"].krb5_rd_rep
krb5_rd_rep.restype = krb5_error_code
krb5_rd_rep.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(struct__krb5_ap_rep_enc_part)),
]
krb5_rd_rep_dce = _libraries["libkrb5.so"].krb5_rd_rep_dce
krb5_rd_rep_dce.restype = krb5_error_code
krb5_rd_rep_dce.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(ctypes.c_uint32),
]
krb5_mk_error = _libraries["libkrb5.so"].krb5_mk_error
krb5_mk_error.restype = krb5_error_code
krb5_mk_error.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_error),
    POINTER_T(struct__krb5_data),
]
krb5_rd_error = _libraries["libkrb5.so"].krb5_rd_error
krb5_rd_error.restype = krb5_error_code
krb5_rd_error.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(struct__krb5_error)),
]
krb5_rd_safe = _libraries["libkrb5.so"].krb5_rd_safe
krb5_rd_safe.restype = krb5_error_code
krb5_rd_safe.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct_krb5_replay_data),
]
krb5_rd_priv = _libraries["libkrb5.so"].krb5_rd_priv
krb5_rd_priv.restype = krb5_error_code
krb5_rd_priv.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct_krb5_replay_data),
]
krb5_parse_name = _libraries["libkrb5.so"].krb5_parse_name
krb5_parse_name.restype = krb5_error_code
krb5_parse_name.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
]
krb5_parse_name_flags = _libraries["libkrb5.so"].krb5_parse_name_flags
krb5_parse_name_flags.restype = krb5_error_code
krb5_parse_name_flags.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
]
krb5_unparse_name = _libraries["libkrb5.so"].krb5_unparse_name
krb5_unparse_name.restype = krb5_error_code
krb5_unparse_name.argtypes = [
    krb5_context,
    krb5_const_principal,
    POINTER_T(POINTER_T(ctypes.c_char)),
]
krb5_unparse_name_ext = _libraries["libkrb5.so"].krb5_unparse_name_ext
krb5_unparse_name_ext.restype = krb5_error_code
krb5_unparse_name_ext.argtypes = [
    krb5_context,
    krb5_const_principal,
    POINTER_T(POINTER_T(ctypes.c_char)),
    POINTER_T(ctypes.c_uint32),
]
krb5_unparse_name_flags = _libraries["libkrb5.so"].krb5_unparse_name_flags
krb5_unparse_name_flags.restype = krb5_error_code
krb5_unparse_name_flags.argtypes = [
    krb5_context,
    krb5_const_principal,
    ctypes.c_int32,
    POINTER_T(POINTER_T(ctypes.c_char)),
]
krb5_unparse_name_flags_ext = _libraries["libkrb5.so"].krb5_unparse_name_flags_ext
krb5_unparse_name_flags_ext.restype = krb5_error_code
krb5_unparse_name_flags_ext.argtypes = [
    krb5_context,
    krb5_const_principal,
    ctypes.c_int32,
    POINTER_T(POINTER_T(ctypes.c_char)),
    POINTER_T(ctypes.c_uint32),
]
krb5_set_principal_realm = _libraries["libkrb5.so"].krb5_set_principal_realm
krb5_set_principal_realm.restype = krb5_error_code
krb5_set_principal_realm.argtypes = [
    krb5_context,
    krb5_principal,
    POINTER_T(ctypes.c_char),
]
krb5_address_search = _libraries["libkrb5.so"].krb5_address_search
krb5_address_search.restype = krb5_boolean
krb5_address_search.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_address),
    POINTER_T(POINTER_T(struct__krb5_address)),
]
krb5_address_compare = _libraries["libkrb5.so"].krb5_address_compare
krb5_address_compare.restype = krb5_boolean
krb5_address_compare.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_address),
    POINTER_T(struct__krb5_address),
]
krb5_address_order = _libraries["libkrb5.so"].krb5_address_order
krb5_address_order.restype = ctypes.c_int32
krb5_address_order.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_address),
    POINTER_T(struct__krb5_address),
]
krb5_realm_compare = _libraries["libkrb5.so"].krb5_realm_compare
krb5_realm_compare.restype = krb5_boolean
krb5_realm_compare.argtypes = [krb5_context, krb5_const_principal, krb5_const_principal]
krb5_principal_compare = _libraries["libkrb5.so"].krb5_principal_compare
krb5_principal_compare.restype = krb5_boolean
krb5_principal_compare.argtypes = [
    krb5_context,
    krb5_const_principal,
    krb5_const_principal,
]
krb5_principal_compare_any_realm = _libraries[
    "libkrb5.so"
].krb5_principal_compare_any_realm
krb5_principal_compare_any_realm.restype = krb5_boolean
krb5_principal_compare_any_realm.argtypes = [
    krb5_context,
    krb5_const_principal,
    krb5_const_principal,
]
krb5_principal_compare_flags = _libraries["libkrb5.so"].krb5_principal_compare_flags
krb5_principal_compare_flags.restype = krb5_boolean
krb5_principal_compare_flags.argtypes = [
    krb5_context,
    krb5_const_principal,
    krb5_const_principal,
    ctypes.c_int32,
]
krb5_init_keyblock = _libraries["libkrb5.so"].krb5_init_keyblock
krb5_init_keyblock.restype = krb5_error_code
krb5_init_keyblock.argtypes = [
    krb5_context,
    krb5_enctype,
    ctypes.c_int32,
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_copy_keyblock = _libraries["libkrb5.so"].krb5_copy_keyblock
krb5_copy_keyblock.restype = krb5_error_code
krb5_copy_keyblock.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_copy_keyblock_contents = _libraries["libkrb5.so"].krb5_copy_keyblock_contents
krb5_copy_keyblock_contents.restype = krb5_error_code
krb5_copy_keyblock_contents.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_keyblock),
]
krb5_copy_creds = _libraries["libkrb5.so"].krb5_copy_creds
krb5_copy_creds.restype = krb5_error_code
krb5_copy_creds.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    POINTER_T(POINTER_T(struct__krb5_creds)),
]
krb5_copy_data = _libraries["libkrb5.so"].krb5_copy_data
krb5_copy_data.restype = krb5_error_code
krb5_copy_data.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(struct__krb5_data)),
]
krb5_copy_principal = _libraries["libkrb5.so"].krb5_copy_principal
krb5_copy_principal.restype = krb5_error_code
krb5_copy_principal.argtypes = [
    krb5_context,
    krb5_const_principal,
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
]
krb5_copy_addresses = _libraries["libkrb5.so"].krb5_copy_addresses
krb5_copy_addresses.restype = krb5_error_code
krb5_copy_addresses.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_address)),
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_address))),
]
krb5_copy_ticket = _libraries["libkrb5.so"].krb5_copy_ticket
krb5_copy_ticket.restype = krb5_error_code
krb5_copy_ticket.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_ticket),
    POINTER_T(POINTER_T(struct__krb5_ticket)),
]
krb5_copy_authdata = _libraries["libkrb5.so"].krb5_copy_authdata
krb5_copy_authdata.restype = krb5_error_code
krb5_copy_authdata.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_authdata)),
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_authdata))),
]
krb5_find_authdata = _libraries["libkrb5.so"].krb5_find_authdata
krb5_find_authdata.restype = krb5_error_code
krb5_find_authdata.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_authdata)),
    POINTER_T(POINTER_T(struct__krb5_authdata)),
    krb5_authdatatype,
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_authdata))),
]
krb5_merge_authdata = _libraries["libkrb5.so"].krb5_merge_authdata
krb5_merge_authdata.restype = krb5_error_code
krb5_merge_authdata.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_authdata)),
    POINTER_T(POINTER_T(struct__krb5_authdata)),
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_authdata))),
]
krb5_copy_authenticator = _libraries["libkrb5.so"].krb5_copy_authenticator
krb5_copy_authenticator.restype = krb5_error_code
krb5_copy_authenticator.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_authenticator),
    POINTER_T(POINTER_T(struct__krb5_authenticator)),
]
krb5_copy_checksum = _libraries["libkrb5.so"].krb5_copy_checksum
krb5_copy_checksum.restype = krb5_error_code
krb5_copy_checksum.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_checksum),
    POINTER_T(POINTER_T(struct__krb5_checksum)),
]
krb5_get_server_rcache = _libraries["libkrb5.so"].krb5_get_server_rcache
krb5_get_server_rcache.restype = krb5_error_code
krb5_get_server_rcache.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(struct_krb5_rc_st)),
]
krb5_build_principal_ext = _libraries["libkrb5.so"].krb5_build_principal_ext
krb5_build_principal_ext.restype = krb5_error_code
krb5_build_principal_ext.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
    ctypes.c_uint32,
    POINTER_T(ctypes.c_char),
]
krb5_build_principal = _libraries["libkrb5.so"].krb5_build_principal
krb5_build_principal.restype = krb5_error_code
krb5_build_principal.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
    ctypes.c_uint32,
    POINTER_T(ctypes.c_char),
]
krb5_build_principal_alloc_va = _libraries["libkrb5.so"].krb5_build_principal_alloc_va
krb5_build_principal_alloc_va.restype = krb5_error_code
krb5_build_principal_alloc_va.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
    ctypes.c_uint32,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_425_conv_principal = _libraries["libkrb5.so"].krb5_425_conv_principal
krb5_425_conv_principal.restype = krb5_error_code
krb5_425_conv_principal.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
]
krb5_524_conv_principal = _libraries["libkrb5.so"].krb5_524_conv_principal
krb5_524_conv_principal.restype = krb5_error_code
krb5_524_conv_principal.argtypes = [
    krb5_context,
    krb5_const_principal,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
]


class struct_credentials(ctypes.Structure):
    pass


krb5_524_convert_creds = _libraries["libkrb5.so"].krb5_524_convert_creds
krb5_524_convert_creds.restype = ctypes.c_int32
krb5_524_convert_creds.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    POINTER_T(struct_credentials),
]
krb5_kt_resolve = _libraries["libkrb5.so"].krb5_kt_resolve
krb5_kt_resolve.restype = krb5_error_code
krb5_kt_resolve.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(struct__krb5_kt)),
]
krb5_kt_dup = _libraries["libkrb5.so"].krb5_kt_dup
krb5_kt_dup.restype = krb5_error_code
krb5_kt_dup.argtypes = [
    krb5_context,
    krb5_keytab,
    POINTER_T(POINTER_T(struct__krb5_kt)),
]
krb5_kt_default_name = _libraries["libkrb5.so"].krb5_kt_default_name
krb5_kt_default_name.restype = krb5_error_code
krb5_kt_default_name.argtypes = [krb5_context, POINTER_T(ctypes.c_char), ctypes.c_int32]
krb5_kt_default = _libraries["libkrb5.so"].krb5_kt_default
krb5_kt_default.restype = krb5_error_code
krb5_kt_default.argtypes = [krb5_context, POINTER_T(POINTER_T(struct__krb5_kt))]
krb5_kt_client_default = _libraries["libkrb5.so"].krb5_kt_client_default
krb5_kt_client_default.restype = krb5_error_code
krb5_kt_client_default.argtypes = [krb5_context, POINTER_T(POINTER_T(struct__krb5_kt))]
krb5_free_keytab_entry_contents = _libraries[
    "libkrb5.so"
].krb5_free_keytab_entry_contents
krb5_free_keytab_entry_contents.restype = krb5_error_code
krb5_free_keytab_entry_contents.argtypes = [
    krb5_context,
    POINTER_T(struct_krb5_keytab_entry_st),
]
krb5_kt_free_entry = _libraries["libkrb5.so"].krb5_kt_free_entry
krb5_kt_free_entry.restype = krb5_error_code
krb5_kt_free_entry.argtypes = [krb5_context, POINTER_T(struct_krb5_keytab_entry_st)]
krb5_kt_remove_entry = _libraries["libkrb5.so"].krb5_kt_remove_entry
krb5_kt_remove_entry.restype = krb5_error_code
krb5_kt_remove_entry.argtypes = [
    krb5_context,
    krb5_keytab,
    POINTER_T(struct_krb5_keytab_entry_st),
]
krb5_kt_add_entry = _libraries["libkrb5.so"].krb5_kt_add_entry
krb5_kt_add_entry.restype = krb5_error_code
krb5_kt_add_entry.argtypes = [
    krb5_context,
    krb5_keytab,
    POINTER_T(struct_krb5_keytab_entry_st),
]
krb5_principal2salt = _libraries["libkrb5.so"].krb5_principal2salt
krb5_principal2salt.restype = krb5_error_code
krb5_principal2salt.argtypes = [
    krb5_context,
    krb5_const_principal,
    POINTER_T(struct__krb5_data),
]
krb5_cc_resolve = _libraries["libkrb5.so"].krb5_cc_resolve
krb5_cc_resolve.restype = krb5_error_code
krb5_cc_resolve.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(struct__krb5_ccache)),
]
krb5_cc_dup = _libraries["libkrb5.so"].krb5_cc_dup
krb5_cc_dup.restype = krb5_error_code
krb5_cc_dup.argtypes = [
    krb5_context,
    krb5_ccache,
    POINTER_T(POINTER_T(struct__krb5_ccache)),
]
krb5_cc_default_name = _libraries["libkrb5.so"].krb5_cc_default_name
krb5_cc_default_name.restype = POINTER_T(ctypes.c_char)
krb5_cc_default_name.argtypes = [krb5_context]
krb5_cc_set_default_name = _libraries["libkrb5.so"].krb5_cc_set_default_name
krb5_cc_set_default_name.restype = krb5_error_code
krb5_cc_set_default_name.argtypes = [krb5_context, POINTER_T(ctypes.c_char)]
krb5_cc_default = _libraries["libkrb5.so"].krb5_cc_default
krb5_cc_default.restype = krb5_error_code
krb5_cc_default.argtypes = [krb5_context, POINTER_T(POINTER_T(struct__krb5_ccache))]
krb5_cc_copy_creds = _libraries["libkrb5.so"].krb5_cc_copy_creds
krb5_cc_copy_creds.restype = krb5_error_code
krb5_cc_copy_creds.argtypes = [krb5_context, krb5_ccache, krb5_ccache]
krb5_cc_get_config = _libraries["libkrb5.so"].krb5_cc_get_config
krb5_cc_get_config.restype = krb5_error_code
krb5_cc_get_config.argtypes = [
    krb5_context,
    krb5_ccache,
    krb5_const_principal,
    POINTER_T(ctypes.c_char),
    POINTER_T(struct__krb5_data),
]
krb5_cc_set_config = _libraries["libkrb5.so"].krb5_cc_set_config
krb5_cc_set_config.restype = krb5_error_code
krb5_cc_set_config.argtypes = [
    krb5_context,
    krb5_ccache,
    krb5_const_principal,
    POINTER_T(ctypes.c_char),
    POINTER_T(struct__krb5_data),
]
krb5_is_config_principal = _libraries["libkrb5.so"].krb5_is_config_principal
krb5_is_config_principal.restype = krb5_boolean
krb5_is_config_principal.argtypes = [krb5_context, krb5_const_principal]
krb5_cc_switch = _libraries["libkrb5.so"].krb5_cc_switch
krb5_cc_switch.restype = krb5_error_code
krb5_cc_switch.argtypes = [krb5_context, krb5_ccache]
krb5_cc_support_switch = _libraries["libkrb5.so"].krb5_cc_support_switch
krb5_cc_support_switch.restype = krb5_boolean
krb5_cc_support_switch.argtypes = [krb5_context, POINTER_T(ctypes.c_char)]
krb5_cc_cache_match = _libraries["libkrb5.so"].krb5_cc_cache_match
krb5_cc_cache_match.restype = krb5_error_code
krb5_cc_cache_match.argtypes = [
    krb5_context,
    krb5_principal,
    POINTER_T(POINTER_T(struct__krb5_ccache)),
]
krb5_cc_select = _libraries["libkrb5.so"].krb5_cc_select
krb5_cc_select.restype = krb5_error_code
krb5_cc_select.argtypes = [
    krb5_context,
    krb5_principal,
    POINTER_T(POINTER_T(struct__krb5_ccache)),
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
]
krb5_free_principal = _libraries["libkrb5.so"].krb5_free_principal
krb5_free_principal.restype = None
krb5_free_principal.argtypes = [krb5_context, krb5_principal]
krb5_free_authenticator = _libraries["libkrb5.so"].krb5_free_authenticator
krb5_free_authenticator.restype = None
krb5_free_authenticator.argtypes = [krb5_context, POINTER_T(struct__krb5_authenticator)]
krb5_free_addresses = _libraries["libkrb5.so"].krb5_free_addresses
krb5_free_addresses.restype = None
krb5_free_addresses.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_address)),
]
krb5_free_authdata = _libraries["libkrb5.so"].krb5_free_authdata
krb5_free_authdata.restype = None
krb5_free_authdata.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_authdata)),
]
krb5_free_ticket = _libraries["libkrb5.so"].krb5_free_ticket
krb5_free_ticket.restype = None
krb5_free_ticket.argtypes = [krb5_context, POINTER_T(struct__krb5_ticket)]
krb5_free_error = _libraries["libkrb5.so"].krb5_free_error
krb5_free_error.restype = None
krb5_free_error.argtypes = [krb5_context, POINTER_T(struct__krb5_error)]
krb5_free_creds = _libraries["libkrb5.so"].krb5_free_creds
krb5_free_creds.restype = None
krb5_free_creds.argtypes = [krb5_context, POINTER_T(struct__krb5_creds)]
krb5_free_cred_contents = _libraries["libkrb5.so"].krb5_free_cred_contents
krb5_free_cred_contents.restype = None
krb5_free_cred_contents.argtypes = [krb5_context, POINTER_T(struct__krb5_creds)]
krb5_free_checksum = _libraries["libkrb5.so"].krb5_free_checksum
krb5_free_checksum.restype = None
krb5_free_checksum.argtypes = [krb5_context, POINTER_T(struct__krb5_checksum)]
krb5_free_checksum_contents = _libraries["libkrb5.so"].krb5_free_checksum_contents
krb5_free_checksum_contents.restype = None
krb5_free_checksum_contents.argtypes = [krb5_context, POINTER_T(struct__krb5_checksum)]
krb5_free_keyblock = _libraries["libkrb5.so"].krb5_free_keyblock
krb5_free_keyblock.restype = None
krb5_free_keyblock.argtypes = [krb5_context, POINTER_T(struct__krb5_keyblock)]
krb5_free_keyblock_contents = _libraries["libkrb5.so"].krb5_free_keyblock_contents
krb5_free_keyblock_contents.restype = None
krb5_free_keyblock_contents.argtypes = [krb5_context, POINTER_T(struct__krb5_keyblock)]
krb5_free_ap_rep_enc_part = _libraries["libkrb5.so"].krb5_free_ap_rep_enc_part
krb5_free_ap_rep_enc_part.restype = None
krb5_free_ap_rep_enc_part.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_ap_rep_enc_part),
]
krb5_free_data = _libraries["libkrb5.so"].krb5_free_data
krb5_free_data.restype = None
krb5_free_data.argtypes = [krb5_context, POINTER_T(struct__krb5_data)]
krb5_free_octet_data = _libraries["libkrb5.so"].krb5_free_octet_data
krb5_free_octet_data.restype = None
krb5_free_octet_data.argtypes = [krb5_context, POINTER_T(struct__krb5_octet_data)]
krb5_free_data_contents = _libraries["libkrb5.so"].krb5_free_data_contents
krb5_free_data_contents.restype = None
krb5_free_data_contents.argtypes = [krb5_context, POINTER_T(struct__krb5_data)]
krb5_free_unparsed_name = _libraries["libkrb5.so"].krb5_free_unparsed_name
krb5_free_unparsed_name.restype = None
krb5_free_unparsed_name.argtypes = [krb5_context, POINTER_T(ctypes.c_char)]
krb5_free_string = _libraries["libkrb5.so"].krb5_free_string
krb5_free_string.restype = None
krb5_free_string.argtypes = [krb5_context, POINTER_T(ctypes.c_char)]
krb5_free_enctypes = _libraries["libkrb5.so"].krb5_free_enctypes
krb5_free_enctypes.restype = None
krb5_free_enctypes.argtypes = [krb5_context, POINTER_T(ctypes.c_int32)]
krb5_free_cksumtypes = _libraries["libkrb5.so"].krb5_free_cksumtypes
krb5_free_cksumtypes.restype = None
krb5_free_cksumtypes.argtypes = [krb5_context, POINTER_T(ctypes.c_int32)]
krb5_us_timeofday = _libraries["libkrb5.so"].krb5_us_timeofday
krb5_us_timeofday.restype = krb5_error_code
krb5_us_timeofday.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_int32),
    POINTER_T(ctypes.c_int32),
]
krb5_timeofday = _libraries["libkrb5.so"].krb5_timeofday
krb5_timeofday.restype = krb5_error_code
krb5_timeofday.argtypes = [krb5_context, POINTER_T(ctypes.c_int32)]
krb5_check_clockskew = _libraries["libkrb5.so"].krb5_check_clockskew
krb5_check_clockskew.restype = krb5_error_code
krb5_check_clockskew.argtypes = [krb5_context, krb5_timestamp]
krb5_os_localaddr = _libraries["libkrb5.so"].krb5_os_localaddr
krb5_os_localaddr.restype = krb5_error_code
krb5_os_localaddr.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_address))),
]
krb5_get_default_realm = _libraries["libkrb5.so"].krb5_get_default_realm
krb5_get_default_realm.restype = krb5_error_code
krb5_get_default_realm.argtypes = [krb5_context, POINTER_T(POINTER_T(ctypes.c_char))]
krb5_set_default_realm = _libraries["libkrb5.so"].krb5_set_default_realm
krb5_set_default_realm.restype = krb5_error_code
krb5_set_default_realm.argtypes = [krb5_context, POINTER_T(ctypes.c_char)]
krb5_free_default_realm = _libraries["libkrb5.so"].krb5_free_default_realm
krb5_free_default_realm.restype = None
krb5_free_default_realm.argtypes = [krb5_context, POINTER_T(ctypes.c_char)]
krb5_expand_hostname = _libraries["libkrb5.so"].krb5_expand_hostname
krb5_expand_hostname.restype = krb5_error_code
krb5_expand_hostname.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(ctypes.c_char)),
]
krb5_sname_to_principal = _libraries["libkrb5.so"].krb5_sname_to_principal
krb5_sname_to_principal.restype = krb5_error_code
krb5_sname_to_principal.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    krb5_int32,
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
]
krb5_sname_match = _libraries["libkrb5.so"].krb5_sname_match
krb5_sname_match.restype = krb5_boolean
krb5_sname_match.argtypes = [krb5_context, krb5_const_principal, krb5_const_principal]
krb5_change_password = _libraries["libkrb5.so"].krb5_change_password
krb5_change_password.restype = krb5_error_code
krb5_change_password.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_int32),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
]
krb5_set_password = _libraries["libkrb5.so"].krb5_set_password
krb5_set_password.restype = krb5_error_code
krb5_set_password.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    POINTER_T(ctypes.c_char),
    krb5_principal,
    POINTER_T(ctypes.c_int32),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
]
krb5_set_password_using_ccache = _libraries["libkrb5.so"].krb5_set_password_using_ccache
krb5_set_password_using_ccache.restype = krb5_error_code
krb5_set_password_using_ccache.argtypes = [
    krb5_context,
    krb5_ccache,
    POINTER_T(ctypes.c_char),
    krb5_principal,
    POINTER_T(ctypes.c_int32),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
]
krb5_chpw_message = _libraries["libkrb5.so"].krb5_chpw_message
krb5_chpw_message.restype = krb5_error_code
krb5_chpw_message.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(ctypes.c_char)),
]
krb5_get_profile = _libraries["libkrb5.so"].krb5_get_profile
krb5_get_profile.restype = krb5_error_code
krb5_get_profile.argtypes = [krb5_context, POINTER_T(POINTER_T(struct__profile_t))]
krb5_rd_req = _libraries["libkrb5.so"].krb5_rd_req
krb5_rd_req.restype = krb5_error_code
krb5_rd_req.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_auth_context)),
    POINTER_T(struct__krb5_data),
    krb5_const_principal,
    krb5_keytab,
    POINTER_T(ctypes.c_int32),
    POINTER_T(POINTER_T(struct__krb5_ticket)),
]
krb5_kt_read_service_key = _libraries["libkrb5.so"].krb5_kt_read_service_key
krb5_kt_read_service_key.restype = krb5_error_code
krb5_kt_read_service_key.argtypes = [
    krb5_context,
    krb5_pointer,
    krb5_principal,
    krb5_kvno,
    krb5_enctype,
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_mk_safe = _libraries["libkrb5.so"].krb5_mk_safe
krb5_mk_safe.restype = krb5_error_code
krb5_mk_safe.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct_krb5_replay_data),
]
krb5_mk_priv = _libraries["libkrb5.so"].krb5_mk_priv
krb5_mk_priv.restype = krb5_error_code
krb5_mk_priv.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct_krb5_replay_data),
]
krb5_sendauth = _libraries["libkrb5.so"].krb5_sendauth
krb5_sendauth.restype = krb5_error_code
krb5_sendauth.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_auth_context)),
    krb5_pointer,
    POINTER_T(ctypes.c_char),
    krb5_principal,
    krb5_principal,
    krb5_flags,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_creds),
    krb5_ccache,
    POINTER_T(POINTER_T(struct__krb5_error)),
    POINTER_T(POINTER_T(struct__krb5_ap_rep_enc_part)),
    POINTER_T(POINTER_T(struct__krb5_creds)),
]
krb5_recvauth = _libraries["libkrb5.so"].krb5_recvauth
krb5_recvauth.restype = krb5_error_code
krb5_recvauth.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_auth_context)),
    krb5_pointer,
    POINTER_T(ctypes.c_char),
    krb5_principal,
    krb5_int32,
    krb5_keytab,
    POINTER_T(POINTER_T(struct__krb5_ticket)),
]
krb5_recvauth_version = _libraries["libkrb5.so"].krb5_recvauth_version
krb5_recvauth_version.restype = krb5_error_code
krb5_recvauth_version.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_auth_context)),
    krb5_pointer,
    krb5_principal,
    krb5_int32,
    krb5_keytab,
    POINTER_T(POINTER_T(struct__krb5_ticket)),
    POINTER_T(struct__krb5_data),
]
krb5_mk_ncred = _libraries["libkrb5.so"].krb5_mk_ncred
krb5_mk_ncred.restype = krb5_error_code
krb5_mk_ncred.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct__krb5_creds)),
    POINTER_T(POINTER_T(struct__krb5_data)),
    POINTER_T(struct_krb5_replay_data),
]
krb5_mk_1cred = _libraries["libkrb5.so"].krb5_mk_1cred
krb5_mk_1cred.restype = krb5_error_code
krb5_mk_1cred.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_creds),
    POINTER_T(POINTER_T(struct__krb5_data)),
    POINTER_T(struct_krb5_replay_data),
]
krb5_rd_cred = _libraries["libkrb5.so"].krb5_rd_cred
krb5_rd_cred.restype = krb5_error_code
krb5_rd_cred.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_creds))),
    POINTER_T(struct_krb5_replay_data),
]
krb5_fwd_tgt_creds = _libraries["libkrb5.so"].krb5_fwd_tgt_creds
krb5_fwd_tgt_creds.restype = krb5_error_code
krb5_fwd_tgt_creds.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(ctypes.c_char),
    krb5_principal,
    krb5_principal,
    krb5_ccache,
    ctypes.c_int32,
    POINTER_T(struct__krb5_data),
]
krb5_auth_con_init = _libraries["libkrb5.so"].krb5_auth_con_init
krb5_auth_con_init.restype = krb5_error_code
krb5_auth_con_init.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_auth_context)),
]
krb5_auth_con_free = _libraries["libkrb5.so"].krb5_auth_con_free
krb5_auth_con_free.restype = krb5_error_code
krb5_auth_con_free.argtypes = [krb5_context, krb5_auth_context]
krb5_auth_con_setflags = _libraries["libkrb5.so"].krb5_auth_con_setflags
krb5_auth_con_setflags.restype = krb5_error_code
krb5_auth_con_setflags.argtypes = [krb5_context, krb5_auth_context, krb5_int32]
krb5_auth_con_getflags = _libraries["libkrb5.so"].krb5_auth_con_getflags
krb5_auth_con_getflags.restype = krb5_error_code
krb5_auth_con_getflags.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(ctypes.c_int32),
]
krb5_auth_con_set_checksum_func = _libraries[
    "libkrb5.so"
].krb5_auth_con_set_checksum_func
krb5_auth_con_set_checksum_func.restype = krb5_error_code
krb5_auth_con_set_checksum_func.argtypes = [
    krb5_context,
    krb5_auth_context,
    krb5_mk_req_checksum_func,
    POINTER_T(None),
]
krb5_auth_con_get_checksum_func = _libraries[
    "libkrb5.so"
].krb5_auth_con_get_checksum_func
krb5_auth_con_get_checksum_func.restype = krb5_error_code
krb5_auth_con_get_checksum_func.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(
        POINTER_T(
            ctypes.CFUNCTYPE(
                ctypes.c_int32,
                POINTER_T(struct__krb5_context),
                POINTER_T(struct__krb5_auth_context),
                POINTER_T(None),
                POINTER_T(POINTER_T(struct__krb5_data)),
            )
        )
    ),
    POINTER_T(POINTER_T(None)),
]
krb5_auth_con_setaddrs = _libraries["libkrb5.so"].krb5_auth_con_setaddrs
krb5_auth_con_setaddrs.restype = krb5_error_code
krb5_auth_con_setaddrs.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_address),
    POINTER_T(struct__krb5_address),
]
krb5_auth_con_getaddrs = _libraries["libkrb5.so"].krb5_auth_con_getaddrs
krb5_auth_con_getaddrs.restype = krb5_error_code
krb5_auth_con_getaddrs.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct__krb5_address)),
    POINTER_T(POINTER_T(struct__krb5_address)),
]
krb5_auth_con_setports = _libraries["libkrb5.so"].krb5_auth_con_setports
krb5_auth_con_setports.restype = krb5_error_code
krb5_auth_con_setports.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_address),
    POINTER_T(struct__krb5_address),
]
krb5_auth_con_setuseruserkey = _libraries["libkrb5.so"].krb5_auth_con_setuseruserkey
krb5_auth_con_setuseruserkey.restype = krb5_error_code
krb5_auth_con_setuseruserkey.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_keyblock),
]
krb5_auth_con_getkey = _libraries["libkrb5.so"].krb5_auth_con_getkey
krb5_auth_con_getkey.restype = krb5_error_code
krb5_auth_con_getkey.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_auth_con_getkey_k = _libraries["libkrb5.so"].krb5_auth_con_getkey_k
krb5_auth_con_getkey_k.restype = krb5_error_code
krb5_auth_con_getkey_k.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct_krb5_key_st)),
]
krb5_auth_con_getsendsubkey = _libraries["libkrb5.so"].krb5_auth_con_getsendsubkey
krb5_auth_con_getsendsubkey.restype = krb5_error_code
krb5_auth_con_getsendsubkey.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_auth_con_getsendsubkey_k = _libraries["libkrb5.so"].krb5_auth_con_getsendsubkey_k
krb5_auth_con_getsendsubkey_k.restype = krb5_error_code
krb5_auth_con_getsendsubkey_k.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct_krb5_key_st)),
]
krb5_auth_con_getrecvsubkey = _libraries["libkrb5.so"].krb5_auth_con_getrecvsubkey
krb5_auth_con_getrecvsubkey.restype = krb5_error_code
krb5_auth_con_getrecvsubkey.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct__krb5_keyblock)),
]
krb5_auth_con_getrecvsubkey_k = _libraries["libkrb5.so"].krb5_auth_con_getrecvsubkey_k
krb5_auth_con_getrecvsubkey_k.restype = krb5_error_code
krb5_auth_con_getrecvsubkey_k.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct_krb5_key_st)),
]
krb5_auth_con_setsendsubkey = _libraries["libkrb5.so"].krb5_auth_con_setsendsubkey
krb5_auth_con_setsendsubkey.restype = krb5_error_code
krb5_auth_con_setsendsubkey.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_keyblock),
]
krb5_auth_con_setsendsubkey_k = _libraries["libkrb5.so"].krb5_auth_con_setsendsubkey_k
krb5_auth_con_setsendsubkey_k.restype = krb5_error_code
krb5_auth_con_setsendsubkey_k.argtypes = [krb5_context, krb5_auth_context, krb5_key]
krb5_auth_con_setrecvsubkey = _libraries["libkrb5.so"].krb5_auth_con_setrecvsubkey
krb5_auth_con_setrecvsubkey.restype = krb5_error_code
krb5_auth_con_setrecvsubkey.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(struct__krb5_keyblock),
]
krb5_auth_con_setrecvsubkey_k = _libraries["libkrb5.so"].krb5_auth_con_setrecvsubkey_k
krb5_auth_con_setrecvsubkey_k.restype = krb5_error_code
krb5_auth_con_setrecvsubkey_k.argtypes = [krb5_context, krb5_auth_context, krb5_key]
krb5_auth_con_getlocalseqnumber = _libraries[
    "libkrb5.so"
].krb5_auth_con_getlocalseqnumber
krb5_auth_con_getlocalseqnumber.restype = krb5_error_code
krb5_auth_con_getlocalseqnumber.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(ctypes.c_int32),
]
krb5_auth_con_getremoteseqnumber = _libraries[
    "libkrb5.so"
].krb5_auth_con_getremoteseqnumber
krb5_auth_con_getremoteseqnumber.restype = krb5_error_code
krb5_auth_con_getremoteseqnumber.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(ctypes.c_int32),
]
krb5_auth_con_initivector = _libraries["libkrb5.so"].krb5_auth_con_initivector
krb5_auth_con_initivector.restype = krb5_error_code
krb5_auth_con_initivector.argtypes = [krb5_context, krb5_auth_context]
krb5_auth_con_setrcache = _libraries["libkrb5.so"].krb5_auth_con_setrcache
krb5_auth_con_setrcache.restype = krb5_error_code
krb5_auth_con_setrcache.argtypes = [krb5_context, krb5_auth_context, krb5_rcache]
krb5_auth_con_getrcache = _libraries["libkrb5.so"].krb5_auth_con_getrcache
krb5_auth_con_getrcache.restype = krb5_error_code
krb5_auth_con_getrcache.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct_krb5_rc_st)),
]
krb5_auth_con_getauthenticator = _libraries["libkrb5.so"].krb5_auth_con_getauthenticator
krb5_auth_con_getauthenticator.restype = krb5_error_code
krb5_auth_con_getauthenticator.argtypes = [
    krb5_context,
    krb5_auth_context,
    POINTER_T(POINTER_T(struct__krb5_authenticator)),
]
krb5_auth_con_set_req_cksumtype = _libraries[
    "libkrb5.so"
].krb5_auth_con_set_req_cksumtype
krb5_auth_con_set_req_cksumtype.restype = krb5_error_code
krb5_auth_con_set_req_cksumtype.argtypes = [
    krb5_context,
    krb5_auth_context,
    krb5_cksumtype,
]
krb5_read_password = _libraries["libkrb5.so"].krb5_read_password
krb5_read_password.restype = krb5_error_code
krb5_read_password.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_uint32),
]
krb5_aname_to_localname = _libraries["libkrb5.so"].krb5_aname_to_localname
krb5_aname_to_localname.restype = krb5_error_code
krb5_aname_to_localname.argtypes = [
    krb5_context,
    krb5_const_principal,
    ctypes.c_int32,
    POINTER_T(ctypes.c_char),
]
krb5_get_host_realm = _libraries["libkrb5.so"].krb5_get_host_realm
krb5_get_host_realm.restype = krb5_error_code
krb5_get_host_realm.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(POINTER_T(ctypes.c_char))),
]
krb5_get_fallback_host_realm = _libraries["libkrb5.so"].krb5_get_fallback_host_realm
krb5_get_fallback_host_realm.restype = krb5_error_code
krb5_get_fallback_host_realm.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(POINTER_T(ctypes.c_char))),
]
krb5_free_host_realm = _libraries["libkrb5.so"].krb5_free_host_realm
krb5_free_host_realm.restype = krb5_error_code
krb5_free_host_realm.argtypes = [krb5_context, POINTER_T(POINTER_T(ctypes.c_char))]
krb5_kuserok = _libraries["libkrb5.so"].krb5_kuserok
krb5_kuserok.restype = krb5_boolean
krb5_kuserok.argtypes = [krb5_context, krb5_principal, POINTER_T(ctypes.c_char)]
krb5_auth_con_genaddrs = _libraries["libkrb5.so"].krb5_auth_con_genaddrs
krb5_auth_con_genaddrs.restype = krb5_error_code
krb5_auth_con_genaddrs.argtypes = [
    krb5_context,
    krb5_auth_context,
    ctypes.c_int32,
    ctypes.c_int32,
]
krb5_set_real_time = _libraries["libkrb5.so"].krb5_set_real_time
krb5_set_real_time.restype = krb5_error_code
krb5_set_real_time.argtypes = [krb5_context, krb5_timestamp, krb5_int32]
krb5_get_time_offsets = _libraries["libkrb5.so"].krb5_get_time_offsets
krb5_get_time_offsets.restype = krb5_error_code
krb5_get_time_offsets.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_int32),
    POINTER_T(ctypes.c_int32),
]
krb5_string_to_enctype = _libraries["libkrb5.so"].krb5_string_to_enctype
krb5_string_to_enctype.restype = krb5_error_code
krb5_string_to_enctype.argtypes = [POINTER_T(ctypes.c_char), POINTER_T(ctypes.c_int32)]
krb5_string_to_salttype = _libraries["libkrb5.so"].krb5_string_to_salttype
krb5_string_to_salttype.restype = krb5_error_code
krb5_string_to_salttype.argtypes = [POINTER_T(ctypes.c_char), POINTER_T(ctypes.c_int32)]
krb5_string_to_cksumtype = _libraries["libkrb5.so"].krb5_string_to_cksumtype
krb5_string_to_cksumtype.restype = krb5_error_code
krb5_string_to_cksumtype.argtypes = [
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_int32),
]
krb5_string_to_timestamp = _libraries["libkrb5.so"].krb5_string_to_timestamp
krb5_string_to_timestamp.restype = krb5_error_code
krb5_string_to_timestamp.argtypes = [
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_int32),
]
krb5_string_to_deltat = _libraries["libkrb5.so"].krb5_string_to_deltat
krb5_string_to_deltat.restype = krb5_error_code
krb5_string_to_deltat.argtypes = [POINTER_T(ctypes.c_char), POINTER_T(ctypes.c_int32)]
krb5_enctype_to_string = _libraries["libkrb5.so"].krb5_enctype_to_string
krb5_enctype_to_string.restype = krb5_error_code
krb5_enctype_to_string.argtypes = [
    krb5_enctype,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_enctype_to_name = _libraries["libkrb5.so"].krb5_enctype_to_name
krb5_enctype_to_name.restype = krb5_error_code
krb5_enctype_to_name.argtypes = [
    krb5_enctype,
    krb5_boolean,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_salttype_to_string = _libraries["libkrb5.so"].krb5_salttype_to_string
krb5_salttype_to_string.restype = krb5_error_code
krb5_salttype_to_string.argtypes = [
    krb5_int32,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_cksumtype_to_string = _libraries["libkrb5.so"].krb5_cksumtype_to_string
krb5_cksumtype_to_string.restype = krb5_error_code
krb5_cksumtype_to_string.argtypes = [
    krb5_cksumtype,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_timestamp_to_string = _libraries["libkrb5.so"].krb5_timestamp_to_string
krb5_timestamp_to_string.restype = krb5_error_code
krb5_timestamp_to_string.argtypes = [
    krb5_timestamp,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_timestamp_to_sfstring = _libraries["libkrb5.so"].krb5_timestamp_to_sfstring
krb5_timestamp_to_sfstring.restype = krb5_error_code
krb5_timestamp_to_sfstring.argtypes = [
    krb5_timestamp,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
    POINTER_T(ctypes.c_char),
]
krb5_deltat_to_string = _libraries["libkrb5.so"].krb5_deltat_to_string
krb5_deltat_to_string.restype = krb5_error_code
krb5_deltat_to_string.argtypes = [krb5_deltat, POINTER_T(ctypes.c_char), ctypes.c_int32]


class struct__krb5_prompt(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("prompt", POINTER_T(ctypes.c_char)),
        ("hidden", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("reply", POINTER_T(struct__krb5_data)),
    ]


krb5_prompt = struct__krb5_prompt
krb5_prompter_fct = POINTER_T(
    ctypes.CFUNCTYPE(
        ctypes.c_int32,
        POINTER_T(struct__krb5_context),
        POINTER_T(None),
        POINTER_T(ctypes.c_char),
        POINTER_T(ctypes.c_char),
        ctypes.c_int32,
        POINTER_T(struct__krb5_prompt),
    )
)
krb5_prompter_posix = _libraries["libkrb5.so"].krb5_prompter_posix
krb5_prompter_posix.restype = krb5_error_code
krb5_prompter_posix.argtypes = [
    krb5_context,
    POINTER_T(None),
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
    struct__krb5_prompt * 0,
]


class struct_krb5_responder_context_st(ctypes.Structure):
    pass


krb5_responder_context = POINTER_T(struct_krb5_responder_context_st)
krb5_responder_list_questions = _libraries["libkrb5.so"].krb5_responder_list_questions
krb5_responder_list_questions.restype = POINTER_T(POINTER_T(ctypes.c_char))
krb5_responder_list_questions.argtypes = [krb5_context, krb5_responder_context]
krb5_responder_get_challenge = _libraries["libkrb5.so"].krb5_responder_get_challenge
krb5_responder_get_challenge.restype = POINTER_T(ctypes.c_char)
krb5_responder_get_challenge.argtypes = [
    krb5_context,
    krb5_responder_context,
    POINTER_T(ctypes.c_char),
]
krb5_responder_set_answer = _libraries["libkrb5.so"].krb5_responder_set_answer
krb5_responder_set_answer.restype = krb5_error_code
krb5_responder_set_answer.argtypes = [
    krb5_context,
    krb5_responder_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
]
krb5_responder_fn = POINTER_T(
    ctypes.CFUNCTYPE(
        ctypes.c_int32,
        POINTER_T(struct__krb5_context),
        POINTER_T(None),
        POINTER_T(struct_krb5_responder_context_st),
    )
)


class struct__krb5_responder_otp_tokeninfo(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("flags", ctypes.c_int32),
        ("format", ctypes.c_int32),
        ("length", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("vendor", POINTER_T(ctypes.c_char)),
        ("challenge", POINTER_T(ctypes.c_char)),
        ("token_id", POINTER_T(ctypes.c_char)),
        ("alg_id", POINTER_T(ctypes.c_char)),
    ]


krb5_responder_otp_tokeninfo = struct__krb5_responder_otp_tokeninfo


class struct__krb5_responder_otp_challenge(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("service", POINTER_T(ctypes.c_char)),
        ("tokeninfo", POINTER_T(POINTER_T(struct__krb5_responder_otp_tokeninfo))),
    ]


krb5_responder_otp_challenge = struct__krb5_responder_otp_challenge
krb5_responder_otp_get_challenge = _libraries[
    "libkrb5.so"
].krb5_responder_otp_get_challenge
krb5_responder_otp_get_challenge.restype = krb5_error_code
krb5_responder_otp_get_challenge.argtypes = [
    krb5_context,
    krb5_responder_context,
    POINTER_T(POINTER_T(struct__krb5_responder_otp_challenge)),
]
krb5_responder_otp_set_answer = _libraries["libkrb5.so"].krb5_responder_otp_set_answer
krb5_responder_otp_set_answer.restype = krb5_error_code
krb5_responder_otp_set_answer.argtypes = [
    krb5_context,
    krb5_responder_context,
    ctypes.c_int32,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
]
krb5_responder_otp_challenge_free = _libraries[
    "libkrb5.so"
].krb5_responder_otp_challenge_free
krb5_responder_otp_challenge_free.restype = None
krb5_responder_otp_challenge_free.argtypes = [
    krb5_context,
    krb5_responder_context,
    POINTER_T(struct__krb5_responder_otp_challenge),
]


class struct__krb5_responder_pkinit_identity(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("identity", POINTER_T(ctypes.c_char)),
        ("token_flags", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
    ]


krb5_responder_pkinit_identity = struct__krb5_responder_pkinit_identity


class struct__krb5_responder_pkinit_challenge(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("identities", POINTER_T(POINTER_T(struct__krb5_responder_pkinit_identity))),
    ]


krb5_responder_pkinit_challenge = struct__krb5_responder_pkinit_challenge
krb5_responder_pkinit_get_challenge = _libraries[
    "libkrb5.so"
].krb5_responder_pkinit_get_challenge
krb5_responder_pkinit_get_challenge.restype = krb5_error_code
krb5_responder_pkinit_get_challenge.argtypes = [
    krb5_context,
    krb5_responder_context,
    POINTER_T(POINTER_T(struct__krb5_responder_pkinit_challenge)),
]
krb5_responder_pkinit_set_answer = _libraries[
    "libkrb5.so"
].krb5_responder_pkinit_set_answer
krb5_responder_pkinit_set_answer.restype = krb5_error_code
krb5_responder_pkinit_set_answer.argtypes = [
    krb5_context,
    krb5_responder_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
]
krb5_responder_pkinit_challenge_free = _libraries[
    "libkrb5.so"
].krb5_responder_pkinit_challenge_free
krb5_responder_pkinit_challenge_free.restype = None
krb5_responder_pkinit_challenge_free.argtypes = [
    krb5_context,
    krb5_responder_context,
    POINTER_T(struct__krb5_responder_pkinit_challenge),
]


class struct__krb5_get_init_creds_opt(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("flags", ctypes.c_int32),
        ("tkt_life", ctypes.c_int32),
        ("renew_life", ctypes.c_int32),
        ("forwardable", ctypes.c_int32),
        ("proxiable", ctypes.c_int32),
        ("PADDING_0", ctypes.c_ubyte * 4),
        ("etype_list", POINTER_T(ctypes.c_int32)),
        ("etype_list_length", ctypes.c_int32),
        ("PADDING_1", ctypes.c_ubyte * 4),
        ("address_list", POINTER_T(POINTER_T(struct__krb5_address))),
        ("preauth_list", POINTER_T(ctypes.c_int32)),
        ("preauth_list_length", ctypes.c_int32),
        ("PADDING_2", ctypes.c_ubyte * 4),
        ("salt", POINTER_T(struct__krb5_data)),
    ]


krb5_get_init_creds_opt = struct__krb5_get_init_creds_opt
krb5_get_init_creds_opt_alloc = _libraries["libkrb5.so"].krb5_get_init_creds_opt_alloc
krb5_get_init_creds_opt_alloc.restype = krb5_error_code
krb5_get_init_creds_opt_alloc.argtypes = [
    krb5_context,
    POINTER_T(POINTER_T(struct__krb5_get_init_creds_opt)),
]
krb5_get_init_creds_opt_free = _libraries["libkrb5.so"].krb5_get_init_creds_opt_free
krb5_get_init_creds_opt_free.restype = None
krb5_get_init_creds_opt_free.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
]
krb5_get_init_creds_opt_init = _libraries["libkrb5.so"].krb5_get_init_creds_opt_init
krb5_get_init_creds_opt_init.restype = None
krb5_get_init_creds_opt_init.argtypes = [POINTER_T(struct__krb5_get_init_creds_opt)]
krb5_get_init_creds_opt_set_tkt_life = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_tkt_life
krb5_get_init_creds_opt_set_tkt_life.restype = None
krb5_get_init_creds_opt_set_tkt_life.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_deltat,
]
krb5_get_init_creds_opt_set_renew_life = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_renew_life
krb5_get_init_creds_opt_set_renew_life.restype = None
krb5_get_init_creds_opt_set_renew_life.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_deltat,
]
krb5_get_init_creds_opt_set_forwardable = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_forwardable
krb5_get_init_creds_opt_set_forwardable.restype = None
krb5_get_init_creds_opt_set_forwardable.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    ctypes.c_int32,
]
krb5_get_init_creds_opt_set_proxiable = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_proxiable
krb5_get_init_creds_opt_set_proxiable.restype = None
krb5_get_init_creds_opt_set_proxiable.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    ctypes.c_int32,
]
krb5_get_init_creds_opt_set_canonicalize = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_canonicalize
krb5_get_init_creds_opt_set_canonicalize.restype = None
krb5_get_init_creds_opt_set_canonicalize.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    ctypes.c_int32,
]
krb5_get_init_creds_opt_set_anonymous = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_anonymous
krb5_get_init_creds_opt_set_anonymous.restype = None
krb5_get_init_creds_opt_set_anonymous.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    ctypes.c_int32,
]
krb5_get_init_creds_opt_set_etype_list = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_etype_list
krb5_get_init_creds_opt_set_etype_list.restype = None
krb5_get_init_creds_opt_set_etype_list.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(ctypes.c_int32),
    ctypes.c_int32,
]
krb5_get_init_creds_opt_set_address_list = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_address_list
krb5_get_init_creds_opt_set_address_list.restype = None
krb5_get_init_creds_opt_set_address_list.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(POINTER_T(struct__krb5_address)),
]
krb5_get_init_creds_opt_set_preauth_list = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_preauth_list
krb5_get_init_creds_opt_set_preauth_list.restype = None
krb5_get_init_creds_opt_set_preauth_list.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(ctypes.c_int32),
    ctypes.c_int32,
]
krb5_get_init_creds_opt_set_salt = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_salt
krb5_get_init_creds_opt_set_salt.restype = None
krb5_get_init_creds_opt_set_salt.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(struct__krb5_data),
]
krb5_get_init_creds_opt_set_change_password_prompt = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_change_password_prompt
krb5_get_init_creds_opt_set_change_password_prompt.restype = None
krb5_get_init_creds_opt_set_change_password_prompt.argtypes = [
    POINTER_T(struct__krb5_get_init_creds_opt),
    ctypes.c_int32,
]


class struct__krb5_gic_opt_pa_data(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("attr", POINTER_T(ctypes.c_char)),
        ("value", POINTER_T(ctypes.c_char)),
    ]


krb5_gic_opt_pa_data = struct__krb5_gic_opt_pa_data
krb5_get_init_creds_opt_set_pa = _libraries["libkrb5.so"].krb5_get_init_creds_opt_set_pa
krb5_get_init_creds_opt_set_pa.restype = krb5_error_code
krb5_get_init_creds_opt_set_pa.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
]
krb5_get_init_creds_opt_set_fast_ccache_name = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_fast_ccache_name
krb5_get_init_creds_opt_set_fast_ccache_name.restype = krb5_error_code
krb5_get_init_creds_opt_set_fast_ccache_name.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(ctypes.c_char),
]
krb5_get_init_creds_opt_set_fast_ccache = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_fast_ccache
krb5_get_init_creds_opt_set_fast_ccache.restype = krb5_error_code
krb5_get_init_creds_opt_set_fast_ccache.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_ccache,
]
krb5_get_init_creds_opt_set_in_ccache = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_in_ccache
krb5_get_init_creds_opt_set_in_ccache.restype = krb5_error_code
krb5_get_init_creds_opt_set_in_ccache.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_ccache,
]
krb5_get_init_creds_opt_set_out_ccache = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_out_ccache
krb5_get_init_creds_opt_set_out_ccache.restype = krb5_error_code
krb5_get_init_creds_opt_set_out_ccache.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_ccache,
]
krb5_get_init_creds_opt_set_pac_request = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_pac_request
krb5_get_init_creds_opt_set_pac_request.restype = krb5_error_code
krb5_get_init_creds_opt_set_pac_request.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_boolean,
]
krb5_get_init_creds_opt_set_fast_flags = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_fast_flags
krb5_get_init_creds_opt_set_fast_flags.restype = krb5_error_code
krb5_get_init_creds_opt_set_fast_flags.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_flags,
]
krb5_get_init_creds_opt_get_fast_flags = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_get_fast_flags
krb5_get_init_creds_opt_get_fast_flags.restype = krb5_error_code
krb5_get_init_creds_opt_get_fast_flags.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(ctypes.c_int32),
]
krb5_expire_callback_func = POINTER_T(
    ctypes.CFUNCTYPE(
        None,
        POINTER_T(struct__krb5_context),
        POINTER_T(None),
        ctypes.c_int32,
        ctypes.c_int32,
        ctypes.c_uint32,
    )
)
krb5_get_init_creds_opt_set_expire_callback = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_expire_callback
krb5_get_init_creds_opt_set_expire_callback.restype = krb5_error_code
krb5_get_init_creds_opt_set_expire_callback.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_expire_callback_func,
    POINTER_T(None),
]
krb5_get_init_creds_opt_set_responder = _libraries[
    "libkrb5.so"
].krb5_get_init_creds_opt_set_responder
krb5_get_init_creds_opt_set_responder.restype = krb5_error_code
krb5_get_init_creds_opt_set_responder.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_get_init_creds_opt),
    krb5_responder_fn,
    POINTER_T(None),
]
krb5_get_init_creds_password = _libraries["libkrb5.so"].krb5_get_init_creds_password
krb5_get_init_creds_password.restype = krb5_error_code
krb5_get_init_creds_password.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    krb5_principal,
    POINTER_T(ctypes.c_char),
    krb5_prompter_fct,
    POINTER_T(None),
    krb5_deltat,
    POINTER_T(ctypes.c_char),
    POINTER_T(struct__krb5_get_init_creds_opt),
]
krb5_get_etype_info = _libraries["libkrb5.so"].krb5_get_etype_info
krb5_get_etype_info.restype = krb5_error_code
krb5_get_etype_info.argtypes = [
    krb5_context,
    krb5_principal,
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(ctypes.c_int32),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
]


class struct__krb5_init_creds_context(ctypes.Structure):
    pass


krb5_init_creds_context = POINTER_T(struct__krb5_init_creds_context)
krb5_init_creds_free = _libraries["libkrb5.so"].krb5_init_creds_free
krb5_init_creds_free.restype = None
krb5_init_creds_free.argtypes = [krb5_context, krb5_init_creds_context]
krb5_init_creds_get = _libraries["libkrb5.so"].krb5_init_creds_get
krb5_init_creds_get.restype = krb5_error_code
krb5_init_creds_get.argtypes = [krb5_context, krb5_init_creds_context]
krb5_init_creds_get_creds = _libraries["libkrb5.so"].krb5_init_creds_get_creds
krb5_init_creds_get_creds.restype = krb5_error_code
krb5_init_creds_get_creds.argtypes = [
    krb5_context,
    krb5_init_creds_context,
    POINTER_T(struct__krb5_creds),
]
krb5_init_creds_get_error = _libraries["libkrb5.so"].krb5_init_creds_get_error
krb5_init_creds_get_error.restype = krb5_error_code
krb5_init_creds_get_error.argtypes = [
    krb5_context,
    krb5_init_creds_context,
    POINTER_T(POINTER_T(struct__krb5_error)),
]
krb5_init_creds_init = _libraries["libkrb5.so"].krb5_init_creds_init
krb5_init_creds_init.restype = krb5_error_code
krb5_init_creds_init.argtypes = [
    krb5_context,
    krb5_principal,
    krb5_prompter_fct,
    POINTER_T(None),
    krb5_deltat,
    POINTER_T(struct__krb5_get_init_creds_opt),
    POINTER_T(POINTER_T(struct__krb5_init_creds_context)),
]
krb5_init_creds_set_keytab = _libraries["libkrb5.so"].krb5_init_creds_set_keytab
krb5_init_creds_set_keytab.restype = krb5_error_code
krb5_init_creds_set_keytab.argtypes = [
    krb5_context,
    krb5_init_creds_context,
    krb5_keytab,
]
krb5_init_creds_step = _libraries["libkrb5.so"].krb5_init_creds_step
krb5_init_creds_step.restype = krb5_error_code
krb5_init_creds_step.argtypes = [
    krb5_context,
    krb5_init_creds_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(ctypes.c_uint32),
]
krb5_init_creds_set_password = _libraries["libkrb5.so"].krb5_init_creds_set_password
krb5_init_creds_set_password.restype = krb5_error_code
krb5_init_creds_set_password.argtypes = [
    krb5_context,
    krb5_init_creds_context,
    POINTER_T(ctypes.c_char),
]
krb5_init_creds_set_service = _libraries["libkrb5.so"].krb5_init_creds_set_service
krb5_init_creds_set_service.restype = krb5_error_code
krb5_init_creds_set_service.argtypes = [
    krb5_context,
    krb5_init_creds_context,
    POINTER_T(ctypes.c_char),
]
krb5_init_creds_get_times = _libraries["libkrb5.so"].krb5_init_creds_get_times
krb5_init_creds_get_times.restype = krb5_error_code
krb5_init_creds_get_times.argtypes = [
    krb5_context,
    krb5_init_creds_context,
    POINTER_T(struct__krb5_ticket_times),
]


class struct__krb5_tkt_creds_context(ctypes.Structure):
    pass


krb5_tkt_creds_context = POINTER_T(struct__krb5_tkt_creds_context)
krb5_tkt_creds_init = _libraries["libkrb5.so"].krb5_tkt_creds_init
krb5_tkt_creds_init.restype = krb5_error_code
krb5_tkt_creds_init.argtypes = [
    krb5_context,
    krb5_ccache,
    POINTER_T(struct__krb5_creds),
    krb5_flags,
    POINTER_T(POINTER_T(struct__krb5_tkt_creds_context)),
]
krb5_tkt_creds_get = _libraries["libkrb5.so"].krb5_tkt_creds_get
krb5_tkt_creds_get.restype = krb5_error_code
krb5_tkt_creds_get.argtypes = [krb5_context, krb5_tkt_creds_context]
krb5_tkt_creds_get_creds = _libraries["libkrb5.so"].krb5_tkt_creds_get_creds
krb5_tkt_creds_get_creds.restype = krb5_error_code
krb5_tkt_creds_get_creds.argtypes = [
    krb5_context,
    krb5_tkt_creds_context,
    POINTER_T(struct__krb5_creds),
]
krb5_tkt_creds_free = _libraries["libkrb5.so"].krb5_tkt_creds_free
krb5_tkt_creds_free.restype = None
krb5_tkt_creds_free.argtypes = [krb5_context, krb5_tkt_creds_context]
krb5_tkt_creds_step = _libraries["libkrb5.so"].krb5_tkt_creds_step
krb5_tkt_creds_step.restype = krb5_error_code
krb5_tkt_creds_step.argtypes = [
    krb5_context,
    krb5_tkt_creds_context,
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(struct__krb5_data),
    POINTER_T(ctypes.c_uint32),
]
krb5_tkt_creds_get_times = _libraries["libkrb5.so"].krb5_tkt_creds_get_times
krb5_tkt_creds_get_times.restype = krb5_error_code
krb5_tkt_creds_get_times.argtypes = [
    krb5_context,
    krb5_tkt_creds_context,
    POINTER_T(struct__krb5_ticket_times),
]
krb5_get_init_creds_keytab = _libraries["libkrb5.so"].krb5_get_init_creds_keytab
krb5_get_init_creds_keytab.restype = krb5_error_code
krb5_get_init_creds_keytab.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    krb5_principal,
    krb5_keytab,
    krb5_deltat,
    POINTER_T(ctypes.c_char),
    POINTER_T(struct__krb5_get_init_creds_opt),
]


class struct__krb5_verify_init_creds_opt(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("flags", ctypes.c_int32),
        ("ap_req_nofail", ctypes.c_int32),
    ]


krb5_verify_init_creds_opt = struct__krb5_verify_init_creds_opt
krb5_verify_init_creds_opt_init = _libraries[
    "libkrb5.so"
].krb5_verify_init_creds_opt_init
krb5_verify_init_creds_opt_init.restype = None
krb5_verify_init_creds_opt_init.argtypes = [
    POINTER_T(struct__krb5_verify_init_creds_opt)
]
krb5_verify_init_creds_opt_set_ap_req_nofail = _libraries[
    "libkrb5.so"
].krb5_verify_init_creds_opt_set_ap_req_nofail
krb5_verify_init_creds_opt_set_ap_req_nofail.restype = None
krb5_verify_init_creds_opt_set_ap_req_nofail.argtypes = [
    POINTER_T(struct__krb5_verify_init_creds_opt),
    ctypes.c_int32,
]
krb5_verify_init_creds = _libraries["libkrb5.so"].krb5_verify_init_creds
krb5_verify_init_creds.restype = krb5_error_code
krb5_verify_init_creds.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    krb5_principal,
    krb5_keytab,
    POINTER_T(POINTER_T(struct__krb5_ccache)),
    POINTER_T(struct__krb5_verify_init_creds_opt),
]
krb5_get_validated_creds = _libraries["libkrb5.so"].krb5_get_validated_creds
krb5_get_validated_creds.restype = krb5_error_code
krb5_get_validated_creds.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    krb5_principal,
    krb5_ccache,
    POINTER_T(ctypes.c_char),
]
krb5_get_renewed_creds = _libraries["libkrb5.so"].krb5_get_renewed_creds
krb5_get_renewed_creds.restype = krb5_error_code
krb5_get_renewed_creds.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_creds),
    krb5_principal,
    krb5_ccache,
    POINTER_T(ctypes.c_char),
]
krb5_decode_ticket = _libraries["libkrb5.so"].krb5_decode_ticket
krb5_decode_ticket.restype = krb5_error_code
krb5_decode_ticket.argtypes = [
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(struct__krb5_ticket)),
]
krb5_appdefault_string = _libraries["libkrb5.so"].krb5_appdefault_string
krb5_appdefault_string.restype = None
krb5_appdefault_string.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(struct__krb5_data),
    POINTER_T(ctypes.c_char),
    POINTER_T(ctypes.c_char),
    POINTER_T(POINTER_T(ctypes.c_char)),
]
krb5_appdefault_boolean = _libraries["libkrb5.so"].krb5_appdefault_boolean
krb5_appdefault_boolean.restype = None
krb5_appdefault_boolean.argtypes = [
    krb5_context,
    POINTER_T(ctypes.c_char),
    POINTER_T(struct__krb5_data),
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
    POINTER_T(ctypes.c_int32),
]
krb5_prompt_type = ctypes.c_int32
krb5_get_prompt_types = _libraries["libkrb5.so"].krb5_get_prompt_types
krb5_get_prompt_types.restype = POINTER_T(ctypes.c_int32)
krb5_get_prompt_types.argtypes = [krb5_context]
krb5_set_error_message = _libraries["libkrb5.so"].krb5_set_error_message
krb5_set_error_message.restype = None
krb5_set_error_message.argtypes = [
    krb5_context,
    krb5_error_code,
    POINTER_T(ctypes.c_char),
]
krb5_vset_error_message = _libraries["libkrb5.so"].krb5_vset_error_message
krb5_vset_error_message.restype = None
krb5_vset_error_message.argtypes = [
    krb5_context,
    krb5_error_code,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_prepend_error_message = _libraries["libkrb5.so"].krb5_prepend_error_message
krb5_prepend_error_message.restype = None
krb5_prepend_error_message.argtypes = [
    krb5_context,
    krb5_error_code,
    POINTER_T(ctypes.c_char),
]
krb5_vprepend_error_message = _libraries["libkrb5.so"].krb5_vprepend_error_message
krb5_vprepend_error_message.restype = None
krb5_vprepend_error_message.argtypes = [
    krb5_context,
    krb5_error_code,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_wrap_error_message = _libraries["libkrb5.so"].krb5_wrap_error_message
krb5_wrap_error_message.restype = None
krb5_wrap_error_message.argtypes = [
    krb5_context,
    krb5_error_code,
    krb5_error_code,
    POINTER_T(ctypes.c_char),
]
krb5_vwrap_error_message = _libraries["libkrb5.so"].krb5_vwrap_error_message
krb5_vwrap_error_message.restype = None
krb5_vwrap_error_message.argtypes = [
    krb5_context,
    krb5_error_code,
    krb5_error_code,
    POINTER_T(ctypes.c_char),
    ctypes.c_int32,
]
krb5_copy_error_message = _libraries["libkrb5.so"].krb5_copy_error_message
krb5_copy_error_message.restype = None
krb5_copy_error_message.argtypes = [krb5_context, krb5_context]
krb5_get_error_message = _libraries["libkrb5.so"].krb5_get_error_message
krb5_get_error_message.restype = POINTER_T(ctypes.c_char)
krb5_get_error_message.argtypes = [krb5_context, krb5_error_code]
krb5_free_error_message = _libraries["libkrb5.so"].krb5_free_error_message
krb5_free_error_message.restype = None
krb5_free_error_message.argtypes = [krb5_context, POINTER_T(ctypes.c_char)]
krb5_clear_error_message = _libraries["libkrb5.so"].krb5_clear_error_message
krb5_clear_error_message.restype = None
krb5_clear_error_message.argtypes = [krb5_context]
krb5_decode_authdata_container = _libraries["libkrb5.so"].krb5_decode_authdata_container
krb5_decode_authdata_container.restype = krb5_error_code
krb5_decode_authdata_container.argtypes = [
    krb5_context,
    krb5_authdatatype,
    POINTER_T(struct__krb5_authdata),
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_authdata))),
]
krb5_encode_authdata_container = _libraries["libkrb5.so"].krb5_encode_authdata_container
krb5_encode_authdata_container.restype = krb5_error_code
krb5_encode_authdata_container.argtypes = [
    krb5_context,
    krb5_authdatatype,
    POINTER_T(POINTER_T(struct__krb5_authdata)),
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_authdata))),
]
krb5_make_authdata_kdc_issued = _libraries["libkrb5.so"].krb5_make_authdata_kdc_issued
krb5_make_authdata_kdc_issued.restype = krb5_error_code
krb5_make_authdata_kdc_issued.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    krb5_const_principal,
    POINTER_T(POINTER_T(struct__krb5_authdata)),
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_authdata))),
]
krb5_verify_authdata_kdc_issued = _libraries[
    "libkrb5.so"
].krb5_verify_authdata_kdc_issued
krb5_verify_authdata_kdc_issued.restype = krb5_error_code
krb5_verify_authdata_kdc_issued.argtypes = [
    krb5_context,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_authdata),
    POINTER_T(POINTER_T(struct_krb5_principal_data)),
    POINTER_T(POINTER_T(POINTER_T(struct__krb5_authdata))),
]


class struct_krb5_pac_data(ctypes.Structure):
    pass


krb5_pac = POINTER_T(struct_krb5_pac_data)
krb5_pac_add_buffer = _libraries["libkrb5.so"].krb5_pac_add_buffer
krb5_pac_add_buffer.restype = krb5_error_code
krb5_pac_add_buffer.argtypes = [
    krb5_context,
    krb5_pac,
    krb5_ui_4,
    POINTER_T(struct__krb5_data),
]
krb5_pac_free = _libraries["libkrb5.so"].krb5_pac_free
krb5_pac_free.restype = None
krb5_pac_free.argtypes = [krb5_context, krb5_pac]
krb5_pac_get_buffer = _libraries["libkrb5.so"].krb5_pac_get_buffer
krb5_pac_get_buffer.restype = krb5_error_code
krb5_pac_get_buffer.argtypes = [
    krb5_context,
    krb5_pac,
    krb5_ui_4,
    POINTER_T(struct__krb5_data),
]
krb5_pac_get_types = _libraries["libkrb5.so"].krb5_pac_get_types
krb5_pac_get_types.restype = krb5_error_code
krb5_pac_get_types.argtypes = [
    krb5_context,
    krb5_pac,
    POINTER_T(ctypes.c_int32),
    POINTER_T(POINTER_T(ctypes.c_uint32)),
]
krb5_pac_init = _libraries["libkrb5.so"].krb5_pac_init
krb5_pac_init.restype = krb5_error_code
krb5_pac_init.argtypes = [krb5_context, POINTER_T(POINTER_T(struct_krb5_pac_data))]
krb5_pac_parse = _libraries["libkrb5.so"].krb5_pac_parse
krb5_pac_parse.restype = krb5_error_code
krb5_pac_parse.argtypes = [
    krb5_context,
    POINTER_T(None),
    ctypes.c_int32,
    POINTER_T(POINTER_T(struct_krb5_pac_data)),
]
krb5_pac_verify = _libraries["libkrb5.so"].krb5_pac_verify
krb5_pac_verify.restype = krb5_error_code
krb5_pac_verify.argtypes = [
    krb5_context,
    krb5_pac,
    krb5_timestamp,
    krb5_const_principal,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_keyblock),
]
krb5_pac_verify_ext = _libraries["libkrb5.so"].krb5_pac_verify_ext
krb5_pac_verify_ext.restype = krb5_error_code
krb5_pac_verify_ext.argtypes = [
    krb5_context,
    krb5_pac,
    krb5_timestamp,
    krb5_const_principal,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_keyblock),
    krb5_boolean,
]
krb5_pac_sign = _libraries["libkrb5.so"].krb5_pac_sign
krb5_pac_sign.restype = krb5_error_code
krb5_pac_sign.argtypes = [
    krb5_context,
    krb5_pac,
    krb5_timestamp,
    krb5_const_principal,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_data),
]
krb5_pac_sign_ext = _libraries["libkrb5.so"].krb5_pac_sign_ext
krb5_pac_sign_ext.restype = krb5_error_code
krb5_pac_sign_ext.argtypes = [
    krb5_context,
    krb5_pac,
    krb5_timestamp,
    krb5_const_principal,
    POINTER_T(struct__krb5_keyblock),
    POINTER_T(struct__krb5_keyblock),
    krb5_boolean,
    POINTER_T(struct__krb5_data),
]
krb5_pac_get_client_info = _libraries["libkrb5.so"].krb5_pac_get_client_info
krb5_pac_get_client_info.restype = krb5_error_code
krb5_pac_get_client_info.argtypes = [
    krb5_context,
    krb5_pac,
    POINTER_T(ctypes.c_int32),
    POINTER_T(POINTER_T(ctypes.c_char)),
]
krb5_allow_weak_crypto = _libraries["libkrb5.so"].krb5_allow_weak_crypto
krb5_allow_weak_crypto.restype = krb5_error_code
krb5_allow_weak_crypto.argtypes = [krb5_context, krb5_boolean]


class struct__krb5_trace_info(ctypes.Structure):
    _pack_ = True  # source:False
    _fields_ = [
        ("message", POINTER_T(ctypes.c_char)),
    ]


krb5_trace_info = struct__krb5_trace_info
krb5_trace_callback = POINTER_T(
    ctypes.CFUNCTYPE(
        None,
        POINTER_T(struct__krb5_context),
        POINTER_T(struct__krb5_trace_info),
        POINTER_T(None),
    )
)
krb5_set_trace_callback = _libraries["libkrb5.so"].krb5_set_trace_callback
krb5_set_trace_callback.restype = krb5_error_code
krb5_set_trace_callback.argtypes = [krb5_context, krb5_trace_callback, POINTER_T(None)]
krb5_set_trace_filename = _libraries["libkrb5.so"].krb5_set_trace_filename
krb5_set_trace_filename.restype = krb5_error_code
krb5_set_trace_filename.argtypes = [krb5_context, POINTER_T(ctypes.c_char)]
krb5_pre_send_fn = POINTER_T(
    ctypes.CFUNCTYPE(
        ctypes.c_int32,
        POINTER_T(struct__krb5_context),
        POINTER_T(None),
        POINTER_T(struct__krb5_data),
        POINTER_T(struct__krb5_data),
        POINTER_T(POINTER_T(struct__krb5_data)),
        POINTER_T(POINTER_T(struct__krb5_data)),
    )
)
krb5_post_recv_fn = POINTER_T(
    ctypes.CFUNCTYPE(
        ctypes.c_int32,
        POINTER_T(struct__krb5_context),
        POINTER_T(None),
        ctypes.c_int32,
        POINTER_T(struct__krb5_data),
        POINTER_T(struct__krb5_data),
        POINTER_T(struct__krb5_data),
        POINTER_T(POINTER_T(struct__krb5_data)),
    )
)
krb5_set_kdc_send_hook = _libraries["libkrb5.so"].krb5_set_kdc_send_hook
krb5_set_kdc_send_hook.restype = None
krb5_set_kdc_send_hook.argtypes = [krb5_context, krb5_pre_send_fn, POINTER_T(None)]
krb5_set_kdc_recv_hook = _libraries["libkrb5.so"].krb5_set_kdc_recv_hook
krb5_set_kdc_recv_hook.restype = None
krb5_set_kdc_recv_hook.argtypes = [krb5_context, krb5_post_recv_fn, POINTER_T(None)]
et_krb5_error_table = (struct_error_table).in_dll(
    _libraries["libkrb5.so"], "et_krb5_error_table"
)
initialize_krb5_error_table = _libraries["libkrb5.so"].initialize_krb5_error_table
initialize_krb5_error_table.restype = None
initialize_krb5_error_table.argtypes = []
et_k5e1_error_table = struct_error_table  # Variable struct_error_table
initialize_k5e1_error_table = _libraries["libkrb5.so"].initialize_k5e1_error_table
initialize_k5e1_error_table.restype = None
initialize_k5e1_error_table.argtypes = []
et_kdb5_error_table = (struct_error_table).in_dll(
    _libraries["libkrb5.so"], "et_kdb5_error_table"
)
initialize_kdb5_error_table = _libraries["libkrb5.so"].initialize_kdb5_error_table
initialize_kdb5_error_table.restype = None
initialize_kdb5_error_table.argtypes = []
et_kv5m_error_table = (struct_error_table).in_dll(
    _libraries["libkrb5.so"], "et_kv5m_error_table"
)
initialize_kv5m_error_table = _libraries["libkrb5.so"].initialize_kv5m_error_table
initialize_kv5m_error_table.restype = None
initialize_kv5m_error_table.argtypes = []
et_k524_error_table = (struct_error_table).in_dll(
    _libraries["libkrb5.so"], "et_k524_error_table"
)
initialize_k524_error_table = _libraries["libkrb5.so"].initialize_k524_error_table
initialize_k524_error_table.restype = None
initialize_k524_error_table.argtypes = []
et_asn1_error_table = (struct_error_table).in_dll(
    _libraries["libkrb5.so"], "et_asn1_error_table"
)
initialize_asn1_error_table = _libraries["libkrb5.so"].initialize_asn1_error_table
initialize_asn1_error_table.restype = None
initialize_asn1_error_table.argtypes = []
__all__ = [
    "KRB5_C_RANDSOURCE_EXTERNAL_PROTOCOL",
    "KRB5_C_RANDSOURCE_MAX",
    "KRB5_C_RANDSOURCE_OLDAPI",
    "KRB5_C_RANDSOURCE_OSRAND",
    "KRB5_C_RANDSOURCE_TIMING",
    "KRB5_C_RANDSOURCE_TRUSTEDPARTY",
    "c__Ea_KRB5_C_RANDSOURCE_OLDAPI",
    "et_asn1_error_table",
    "et_k524_error_table",
    "et_k5e1_error_table",
    "et_kdb5_error_table",
    "et_krb5_error_table",
    "et_kv5m_error_table",
    "initialize_asn1_error_table",
    "initialize_k524_error_table",
    "initialize_k5e1_error_table",
    "initialize_kdb5_error_table",
    "initialize_krb5_error_table",
    "initialize_kv5m_error_table",
    "krb5_425_conv_principal",
    "krb5_524_conv_principal",
    "krb5_524_convert_creds",
    "krb5_address",
    "krb5_address_compare",
    "krb5_address_order",
    "krb5_address_search",
    "krb5_addrtype",
    "krb5_allow_weak_crypto",
    "krb5_aname_to_localname",
    "krb5_anonymous_principal",
    "krb5_anonymous_realm",
    "krb5_ap_rep",
    "krb5_ap_rep_enc_part",
    "krb5_ap_req",
    "krb5_appdefault_boolean",
    "krb5_appdefault_string",
    "krb5_auth_con_free",
    "krb5_auth_con_genaddrs",
    "krb5_auth_con_get_checksum_func",
    "krb5_auth_con_getaddrs",
    "krb5_auth_con_getauthenticator",
    "krb5_auth_con_getflags",
    "krb5_auth_con_getkey",
    "krb5_auth_con_getkey_k",
    "krb5_auth_con_getlocalseqnumber",
    "krb5_auth_con_getrcache",
    "krb5_auth_con_getrecvsubkey",
    "krb5_auth_con_getrecvsubkey_k",
    "krb5_auth_con_getremoteseqnumber",
    "krb5_auth_con_getsendsubkey",
    "krb5_auth_con_getsendsubkey_k",
    "krb5_auth_con_init",
    "krb5_auth_con_initivector",
    "krb5_auth_con_set_checksum_func",
    "krb5_auth_con_set_req_cksumtype",
    "krb5_auth_con_setaddrs",
    "krb5_auth_con_setflags",
    "krb5_auth_con_setports",
    "krb5_auth_con_setrcache",
    "krb5_auth_con_setrecvsubkey",
    "krb5_auth_con_setrecvsubkey_k",
    "krb5_auth_con_setsendsubkey",
    "krb5_auth_con_setsendsubkey_k",
    "krb5_auth_con_setuseruserkey",
    "krb5_auth_context",
    "krb5_authdata",
    "krb5_authdatatype",
    "krb5_authenticator",
    "krb5_boolean",
    "krb5_build_principal",
    "krb5_build_principal_alloc_va",
    "krb5_build_principal_ext",
    "krb5_c_block_size",
    "krb5_c_checksum_length",
    "krb5_c_crypto_length",
    "krb5_c_crypto_length_iov",
    "krb5_c_decrypt",
    "krb5_c_decrypt_iov",
    "krb5_c_derive_prfplus",
    "krb5_c_encrypt",
    "krb5_c_encrypt_iov",
    "krb5_c_encrypt_length",
    "krb5_c_enctype_compare",
    "krb5_c_free_state",
    "krb5_c_fx_cf2_simple",
    "krb5_c_init_state",
    "krb5_c_is_coll_proof_cksum",
    "krb5_c_is_keyed_cksum",
    "krb5_c_keyed_checksum_types",
    "krb5_c_keylengths",
    "krb5_c_make_checksum",
    "krb5_c_make_checksum_iov",
    "krb5_c_make_random_key",
    "krb5_c_padding_length",
    "krb5_c_prf",
    "krb5_c_prf_length",
    "krb5_c_prfplus",
    "krb5_c_random_add_entropy",
    "krb5_c_random_make_octets",
    "krb5_c_random_os_entropy",
    "krb5_c_random_seed",
    "krb5_c_random_to_key",
    "krb5_c_string_to_key",
    "krb5_c_string_to_key_with_params",
    "krb5_c_valid_cksumtype",
    "krb5_c_valid_enctype",
    "krb5_c_verify_checksum",
    "krb5_c_verify_checksum_iov",
    "krb5_calculate_checksum",
    "krb5_cc_cache_match",
    "krb5_cc_close",
    "krb5_cc_copy_creds",
    "krb5_cc_cursor",
    "krb5_cc_default",
    "krb5_cc_default_name",
    "krb5_cc_destroy",
    "krb5_cc_dup",
    "krb5_cc_end_seq_get",
    "krb5_cc_get_config",
    "krb5_cc_get_full_name",
    "krb5_cc_get_name",
    "krb5_cc_get_principal",
    "krb5_cc_get_type",
    "krb5_cc_initialize",
    "krb5_cc_move",
    "krb5_cc_new_unique",
    "krb5_cc_next_cred",
    "krb5_cc_ops",
    "krb5_cc_remove_cred",
    "krb5_cc_resolve",
    "krb5_cc_retrieve_cred",
    "krb5_cc_select",
    "krb5_cc_set_config",
    "krb5_cc_set_default_name",
    "krb5_cc_set_flags",
    "krb5_cc_start_seq_get",
    "krb5_cc_store_cred",
    "krb5_cc_support_switch",
    "krb5_cc_switch",
    "krb5_ccache",
    "krb5_cccol_cursor",
    "krb5_cccol_cursor_free",
    "krb5_cccol_cursor_new",
    "krb5_cccol_cursor_next",
    "krb5_cccol_have_content",
    "krb5_change_password",
    "krb5_check_clockskew",
    "krb5_checksum",
    "krb5_checksum_size",
    "krb5_chpw_message",
    "krb5_cksumtype",
    "krb5_cksumtype_to_string",
    "krb5_clear_error_message",
    "krb5_const_pointer",
    "krb5_const_principal",
    "krb5_context",
    "krb5_copy_addresses",
    "krb5_copy_authdata",
    "krb5_copy_authenticator",
    "krb5_copy_checksum",
    "krb5_copy_context",
    "krb5_copy_creds",
    "krb5_copy_data",
    "krb5_copy_error_message",
    "krb5_copy_keyblock",
    "krb5_copy_keyblock_contents",
    "krb5_copy_principal",
    "krb5_copy_ticket",
    "krb5_cred",
    "krb5_cred_enc_part",
    "krb5_cred_info",
    "krb5_creds",
    "krb5_crypto_iov",
    "krb5_cryptotype",
    "krb5_data",
    "krb5_decode_authdata_container",
    "krb5_decode_ticket",
    "krb5_decrypt",
    "krb5_deltat",
    "krb5_deltat_to_string",
    "krb5_eblock_enctype",
    "krb5_enc_data",
    "krb5_enc_kdc_rep_part",
    "krb5_enc_tkt_part",
    "krb5_encode_authdata_container",
    "krb5_encrypt",
    "krb5_encrypt_block",
    "krb5_encrypt_size",
    "krb5_enctype",
    "krb5_enctype_to_name",
    "krb5_enctype_to_string",
    "krb5_error",
    "krb5_error_code",
    "krb5_expand_hostname",
    "krb5_expire_callback_func",
    "krb5_find_authdata",
    "krb5_finish_key",
    "krb5_finish_random_key",
    "krb5_flags",
    "krb5_free_addresses",
    "krb5_free_ap_rep_enc_part",
    "krb5_free_authdata",
    "krb5_free_authenticator",
    "krb5_free_checksum",
    "krb5_free_checksum_contents",
    "krb5_free_cksumtypes",
    "krb5_free_context",
    "krb5_free_cred_contents",
    "krb5_free_creds",
    "krb5_free_data",
    "krb5_free_data_contents",
    "krb5_free_default_realm",
    "krb5_free_enctypes",
    "krb5_free_error",
    "krb5_free_error_message",
    "krb5_free_host_realm",
    "krb5_free_keyblock",
    "krb5_free_keyblock_contents",
    "krb5_free_keytab_entry_contents",
    "krb5_free_octet_data",
    "krb5_free_principal",
    "krb5_free_string",
    "krb5_free_tgt_creds",
    "krb5_free_ticket",
    "krb5_free_unparsed_name",
    "krb5_fwd_tgt_creds",
    "krb5_get_credentials",
    "krb5_get_credentials_renew",
    "krb5_get_credentials_validate",
    "krb5_get_default_realm",
    "krb5_get_error_message",
    "krb5_get_etype_info",
    "krb5_get_fallback_host_realm",
    "krb5_get_host_realm",
    "krb5_get_init_creds_keytab",
    "krb5_get_init_creds_opt",
    "krb5_get_init_creds_opt_alloc",
    "krb5_get_init_creds_opt_free",
    "krb5_get_init_creds_opt_get_fast_flags",
    "krb5_get_init_creds_opt_init",
    "krb5_get_init_creds_opt_set_address_list",
    "krb5_get_init_creds_opt_set_anonymous",
    "krb5_get_init_creds_opt_set_canonicalize",
    "krb5_get_init_creds_opt_set_change_password_prompt",
    "krb5_get_init_creds_opt_set_etype_list",
    "krb5_get_init_creds_opt_set_expire_callback",
    "krb5_get_init_creds_opt_set_fast_ccache",
    "krb5_get_init_creds_opt_set_fast_ccache_name",
    "krb5_get_init_creds_opt_set_fast_flags",
    "krb5_get_init_creds_opt_set_forwardable",
    "krb5_get_init_creds_opt_set_in_ccache",
    "krb5_get_init_creds_opt_set_out_ccache",
    "krb5_get_init_creds_opt_set_pa",
    "krb5_get_init_creds_opt_set_pac_request",
    "krb5_get_init_creds_opt_set_preauth_list",
    "krb5_get_init_creds_opt_set_proxiable",
    "krb5_get_init_creds_opt_set_renew_life",
    "krb5_get_init_creds_opt_set_responder",
    "krb5_get_init_creds_opt_set_salt",
    "krb5_get_init_creds_opt_set_tkt_life",
    "krb5_get_init_creds_password",
    "krb5_get_permitted_enctypes",
    "krb5_get_profile",
    "krb5_get_prompt_types",
    "krb5_get_renewed_creds",
    "krb5_get_server_rcache",
    "krb5_get_time_offsets",
    "krb5_get_validated_creds",
    "krb5_gic_opt_pa_data",
    "krb5_init_context",
    "krb5_init_context_profile",
    "krb5_init_creds_context",
    "krb5_init_creds_free",
    "krb5_init_creds_get",
    "krb5_init_creds_get_creds",
    "krb5_init_creds_get_error",
    "krb5_init_creds_get_times",
    "krb5_init_creds_init",
    "krb5_init_creds_set_keytab",
    "krb5_init_creds_set_password",
    "krb5_init_creds_set_service",
    "krb5_init_creds_step",
    "krb5_init_keyblock",
    "krb5_init_random_key",
    "krb5_init_secure_context",
    "krb5_int16",
    "krb5_int32",
    "krb5_is_config_principal",
    "krb5_is_referral_realm",
    "krb5_is_thread_safe",
    "krb5_k_create_key",
    "krb5_k_decrypt",
    "krb5_k_decrypt_iov",
    "krb5_k_encrypt",
    "krb5_k_encrypt_iov",
    "krb5_k_free_key",
    "krb5_k_key_enctype",
    "krb5_k_key_keyblock",
    "krb5_k_make_checksum",
    "krb5_k_make_checksum_iov",
    "krb5_k_prf",
    "krb5_k_reference_key",
    "krb5_k_verify_checksum",
    "krb5_k_verify_checksum_iov",
    "krb5_kdc_rep",
    "krb5_kdc_req",
    "krb5_key",
    "krb5_keyblock",
    "krb5_keytab",
    "krb5_keytab_entry",
    "krb5_keyusage",
    "krb5_kt_add_entry",
    "krb5_kt_client_default",
    "krb5_kt_close",
    "krb5_kt_cursor",
    "krb5_kt_default",
    "krb5_kt_default_name",
    "krb5_kt_dup",
    "krb5_kt_end_seq_get",
    "krb5_kt_free_entry",
    "krb5_kt_get_entry",
    "krb5_kt_get_name",
    "krb5_kt_get_type",
    "krb5_kt_have_content",
    "krb5_kt_next_entry",
    "krb5_kt_read_service_key",
    "krb5_kt_remove_entry",
    "krb5_kt_resolve",
    "krb5_kt_start_seq_get",
    "krb5_kuserok",
    "krb5_kvno",
    "krb5_last_req_entry",
    "krb5_magic",
    "krb5_make_authdata_kdc_issued",
    "krb5_merge_authdata",
    "krb5_mk_1cred",
    "krb5_mk_error",
    "krb5_mk_ncred",
    "krb5_mk_priv",
    "krb5_mk_rep",
    "krb5_mk_rep_dce",
    "krb5_mk_req",
    "krb5_mk_req_checksum_func",
    "krb5_mk_req_extended",
    "krb5_mk_safe",
    "krb5_msgtype",
    "krb5_octet",
    "krb5_octet_data",
    "krb5_os_localaddr",
    "krb5_pa_data",
    "krb5_pa_pac_req",
    "krb5_pa_server_referral_data",
    "krb5_pa_svr_referral_data",
    "krb5_pac",
    "krb5_pac_add_buffer",
    "krb5_pac_free",
    "krb5_pac_get_buffer",
    "krb5_pac_get_client_info",
    "krb5_pac_get_types",
    "krb5_pac_init",
    "krb5_pac_parse",
    "krb5_pac_sign",
    "krb5_pac_sign_ext",
    "krb5_pac_verify",
    "krb5_pac_verify_ext",
    "krb5_parse_name",
    "krb5_parse_name_flags",
    "krb5_pointer",
    "krb5_post_recv_fn",
    "krb5_pre_send_fn",
    "krb5_preauthtype",
    "krb5_prepend_error_message",
    "krb5_principal",
    "krb5_principal2salt",
    "krb5_principal_compare",
    "krb5_principal_compare_any_realm",
    "krb5_principal_compare_flags",
    "krb5_principal_data",
    "krb5_process_key",
    "krb5_prompt",
    "krb5_prompt_type",
    "krb5_prompter_fct",
    "krb5_prompter_posix",
    "krb5_pwd_data",
    "krb5_random_key",
    "krb5_rcache",
    "krb5_rd_cred",
    "krb5_rd_error",
    "krb5_rd_priv",
    "krb5_rd_rep",
    "krb5_rd_rep_dce",
    "krb5_rd_req",
    "krb5_rd_safe",
    "krb5_read_password",
    "krb5_realm_compare",
    "krb5_recvauth",
    "krb5_recvauth_version",
    "krb5_replay_data",
    "krb5_responder_context",
    "krb5_responder_fn",
    "krb5_responder_get_challenge",
    "krb5_responder_list_questions",
    "krb5_responder_otp_challenge",
    "krb5_responder_otp_challenge_free",
    "krb5_responder_otp_get_challenge",
    "krb5_responder_otp_set_answer",
    "krb5_responder_otp_tokeninfo",
    "krb5_responder_pkinit_challenge",
    "krb5_responder_pkinit_challenge_free",
    "krb5_responder_pkinit_get_challenge",
    "krb5_responder_pkinit_identity",
    "krb5_responder_pkinit_set_answer",
    "krb5_responder_set_answer",
    "krb5_response",
    "krb5_salttype_to_string",
    "krb5_sendauth",
    "krb5_server_decrypt_ticket_keytab",
    "krb5_set_default_realm",
    "krb5_set_default_tgs_enctypes",
    "krb5_set_error_message",
    "krb5_set_kdc_recv_hook",
    "krb5_set_kdc_send_hook",
    "krb5_set_password",
    "krb5_set_password_using_ccache",
    "krb5_set_principal_realm",
    "krb5_set_real_time",
    "krb5_set_trace_callback",
    "krb5_set_trace_filename",
    "krb5_sname_match",
    "krb5_sname_to_principal",
    "krb5_string_to_cksumtype",
    "krb5_string_to_deltat",
    "krb5_string_to_enctype",
    "krb5_string_to_key",
    "krb5_string_to_salttype",
    "krb5_string_to_timestamp",
    "krb5_ticket",
    "krb5_ticket_times",
    "krb5_timeofday",
    "krb5_timestamp",
    "krb5_timestamp_to_sfstring",
    "krb5_timestamp_to_string",
    "krb5_tkt_authent",
    "krb5_tkt_creds_context",
    "krb5_tkt_creds_free",
    "krb5_tkt_creds_get",
    "krb5_tkt_creds_get_creds",
    "krb5_tkt_creds_get_times",
    "krb5_tkt_creds_init",
    "krb5_tkt_creds_step",
    "krb5_trace_callback",
    "krb5_trace_info",
    "krb5_transited",
    "krb5_typed_data",
    "krb5_ui_2",
    "krb5_ui_4",
    "krb5_unparse_name",
    "krb5_unparse_name_ext",
    "krb5_unparse_name_flags",
    "krb5_unparse_name_flags_ext",
    "krb5_us_timeofday",
    "krb5_use_enctype",
    "krb5_verify_authdata_kdc_issued",
    "krb5_verify_checksum",
    "krb5_verify_init_creds",
    "krb5_verify_init_creds_opt",
    "krb5_verify_init_creds_opt_init",
    "krb5_verify_init_creds_opt_set_ap_req_nofail",
    "krb5_vprepend_error_message",
    "krb5_vset_error_message",
    "krb5_vwrap_error_message",
    "krb5_wrap_error_message",
    "passwd_phrase_element",
    "struct__krb5_address",
    "struct__krb5_ap_rep",
    "struct__krb5_ap_rep_enc_part",
    "struct__krb5_ap_req",
    "struct__krb5_auth_context",
    "struct__krb5_authdata",
    "struct__krb5_authenticator",
    "struct__krb5_cc_ops",
    "struct__krb5_ccache",
    "struct__krb5_cccol_cursor",
    "struct__krb5_checksum",
    "struct__krb5_context",
    "struct__krb5_cred",
    "struct__krb5_cred_enc_part",
    "struct__krb5_cred_info",
    "struct__krb5_creds",
    "struct__krb5_crypto_iov",
    "struct__krb5_cryptosystem_entry",
    "struct__krb5_data",
    "struct__krb5_enc_data",
    "struct__krb5_enc_kdc_rep_part",
    "struct__krb5_enc_tkt_part",
    "struct__krb5_encrypt_block",
    "struct__krb5_error",
    "struct__krb5_get_init_creds_opt",
    "struct__krb5_gic_opt_pa_data",
    "struct__krb5_init_creds_context",
    "struct__krb5_kdc_rep",
    "struct__krb5_kdc_req",
    "struct__krb5_keyblock",
    "struct__krb5_kt",
    "struct__krb5_last_req_entry",
    "struct__krb5_octet_data",
    "struct__krb5_pa_data",
    "struct__krb5_pa_pac_req",
    "struct__krb5_pa_server_referral_data",
    "struct__krb5_pa_svr_referral_data",
    "struct__krb5_prompt",
    "struct__krb5_pwd_data",
    "struct__krb5_responder_otp_challenge",
    "struct__krb5_responder_otp_tokeninfo",
    "struct__krb5_responder_pkinit_challenge",
    "struct__krb5_responder_pkinit_identity",
    "struct__krb5_response",
    "struct__krb5_ticket",
    "struct__krb5_ticket_times",
    "struct__krb5_tkt_authent",
    "struct__krb5_tkt_creds_context",
    "struct__krb5_trace_info",
    "struct__krb5_transited",
    "struct__krb5_typed_data",
    "struct__krb5_verify_init_creds_opt",
    "struct__passwd_phrase_element",
    "struct__profile_t",
    "struct_credentials",
    "struct_error_table",
    "struct_et_list",
    "struct_krb5_key_st",
    "struct_krb5_keytab_entry_st",
    "struct_krb5_pac_data",
    "struct_krb5_principal_data",
    "struct_krb5_rc_st",
    "struct_krb5_replay_data",
    "struct_krb5_responder_context_st",
]
KRB5_DEPRECATED = 0
KRB5_INT32_MAX = 2147483647
KRB5_INT16_MAX = 65535
FALSE = 0
TRUE = 1
KRB5_NT_UNKNOWN = 0
KRB5_NT_PRINCIPAL = 1
KRB5_NT_SRV_INST = 2
KRB5_NT_SRV_HST = 3
KRB5_NT_SRV_XHST = 4
KRB5_NT_UID = 5
KRB5_NT_X500_PRINCIPAL = 6
KRB5_NT_SMTP_NAME = 7
KRB5_NT_ENTERPRISE_PRINCIPAL = 10
KRB5_NT_WELLKNOWN = 11
KRB5_NT_MS_PRINCIPAL = -128
KRB5_NT_MS_PRINCIPAL_AND_ID = -129
KRB5_NT_ENT_PRINCIPAL_AND_ID = -130
ADDRTYPE_INET = 0x0002
ADDRTYPE_CHAOS = 0x0005
ADDRTYPE_XNS = 0x0006
ADDRTYPE_ISO = 0x0007
ADDRTYPE_DDP = 0x0010
ADDRTYPE_NETBIOS = 0x0014
ADDRTYPE_INET6 = 0x0018
ADDRTYPE_ADDRPORT = 0x0100
ADDRTYPE_IPPORT = 0x0101
ENCTYPE_NULL = 0x0000
ENCTYPE_DES_CBC_CRC = 0x0001
ENCTYPE_DES_CBC_MD4 = 0x0002
ENCTYPE_DES_CBC_MD5 = 0x0003
ENCTYPE_DES_CBC_RAW = 0x0004
ENCTYPE_DES3_CBC_SHA = 0x0005
ENCTYPE_DES3_CBC_RAW = 0x0006
ENCTYPE_DES_HMAC_SHA1 = 0x0008
ENCTYPE_DSA_SHA1_CMS = 0x0009
ENCTYPE_MD5_RSA_CMS = 0x000A
ENCTYPE_SHA1_RSA_CMS = 0x000B
ENCTYPE_RC2_CBC_ENV = 0x000C
ENCTYPE_RSA_ENV = 0x000D
ENCTYPE_RSA_ES_OAEP_ENV = 0x000E
ENCTYPE_DES3_CBC_ENV = 0x000F
ENCTYPE_DES3_CBC_SHA1 = 0x0010
ENCTYPE_AES128_CTS_HMAC_SHA1_96 = 0x0011
ENCTYPE_AES256_CTS_HMAC_SHA1_96 = 0x0012
ENCTYPE_AES128_CTS_HMAC_SHA256_128 = 0x0013
ENCTYPE_AES256_CTS_HMAC_SHA384_192 = 0x0014
ENCTYPE_ARCFOUR_HMAC = 0x0017
ENCTYPE_ARCFOUR_HMAC_EXP = 0x0018
ENCTYPE_CAMELLIA128_CTS_CMAC = 0x0019
ENCTYPE_CAMELLIA256_CTS_CMAC = 0x001A
ENCTYPE_UNKNOWN = 0x01FF
CKSUMTYPE_CRC32 = 0x0001
CKSUMTYPE_RSA_MD4 = 0x0002
CKSUMTYPE_RSA_MD4_DES = 0x0003
CKSUMTYPE_DESCBC = 0x0004
CKSUMTYPE_RSA_MD5 = 0x0007
CKSUMTYPE_RSA_MD5_DES = 0x0008
CKSUMTYPE_NIST_SHA = 0x0009
CKSUMTYPE_HMAC_SHA1_DES3 = 0x000C
CKSUMTYPE_HMAC_SHA1_96_AES128 = 0x000F
CKSUMTYPE_HMAC_SHA1_96_AES256 = 0x0010
CKSUMTYPE_HMAC_SHA256_128_AES128 = 0x0013
CKSUMTYPE_HMAC_SHA384_192_AES256 = 0x0014
CKSUMTYPE_CMAC_CAMELLIA128 = 0x0011
CKSUMTYPE_CMAC_CAMELLIA256 = 0x0012
CKSUMTYPE_MD5_HMAC_ARCFOUR = -137
CKSUMTYPE_HMAC_MD5_ARCFOUR = -138
KRB5_KEYUSAGE_AS_REQ_PA_ENC_TS = 1
KRB5_KEYUSAGE_KDC_REP_TICKET = 2
KRB5_KEYUSAGE_AS_REP_ENCPART = 3
KRB5_KEYUSAGE_TGS_REQ_AD_SESSKEY = 4
KRB5_KEYUSAGE_TGS_REQ_AD_SUBKEY = 5
KRB5_KEYUSAGE_TGS_REQ_AUTH_CKSUM = 6
KRB5_KEYUSAGE_TGS_REQ_AUTH = 7
KRB5_KEYUSAGE_TGS_REP_ENCPART_SESSKEY = 8
KRB5_KEYUSAGE_TGS_REP_ENCPART_SUBKEY = 9
KRB5_KEYUSAGE_AP_REQ_AUTH_CKSUM = 10
KRB5_KEYUSAGE_AP_REQ_AUTH = 11
KRB5_KEYUSAGE_AP_REP_ENCPART = 12
KRB5_KEYUSAGE_KRB_PRIV_ENCPART = 13
KRB5_KEYUSAGE_KRB_CRED_ENCPART = 14
KRB5_KEYUSAGE_KRB_SAFE_CKSUM = 15
KRB5_KEYUSAGE_APP_DATA_ENCRYPT = 16
KRB5_KEYUSAGE_APP_DATA_CKSUM = 17
KRB5_KEYUSAGE_KRB_ERROR_CKSUM = 18
KRB5_KEYUSAGE_AD_KDCISSUED_CKSUM = 19
KRB5_KEYUSAGE_AD_MTE = 20
KRB5_KEYUSAGE_AD_ITE = 21
KRB5_KEYUSAGE_GSS_TOK_MIC = 22
KRB5_KEYUSAGE_GSS_TOK_WRAP_INTEG = 23
KRB5_KEYUSAGE_GSS_TOK_WRAP_PRIV = 24
KRB5_KEYUSAGE_PA_SAM_CHALLENGE_CKSUM = 25
KRB5_KEYUSAGE_PA_SAM_CHALLENGE_TRACKID = 26
KRB5_KEYUSAGE_PA_SAM_RESPONSE = 27
KRB5_KEYUSAGE_PA_S4U_X509_USER_REQUEST = 26
KRB5_KEYUSAGE_PA_S4U_X509_USER_REPLY = 27
KRB5_KEYUSAGE_PA_REFERRAL = 26
KRB5_KEYUSAGE_AD_SIGNEDPATH = -21
KRB5_KEYUSAGE_IAKERB_FINISHED = 42
KRB5_KEYUSAGE_PA_PKINIT_KX = 44
KRB5_KEYUSAGE_PA_OTP_REQUEST = 45
KRB5_KEYUSAGE_FAST_REQ_CHKSUM = 50
KRB5_KEYUSAGE_FAST_ENC = 51
KRB5_KEYUSAGE_FAST_REP = 52
KRB5_KEYUSAGE_FAST_FINISHED = 53
KRB5_KEYUSAGE_ENC_CHALLENGE_CLIENT = 54
KRB5_KEYUSAGE_ENC_CHALLENGE_KDC = 55
KRB5_KEYUSAGE_AS_REQ = 56
KRB5_KEYUSAGE_CAMMAC = 64
KRB5_KEYUSAGE_SPAKE = 65
KRB5_KEYUSAGE_PA_FX_COOKIE = 513
KRB5_KEYUSAGE_PA_AS_FRESHNESS = 514
KRB5_CRYPTO_TYPE_EMPTY = 0
KRB5_CRYPTO_TYPE_HEADER = 1
KRB5_CRYPTO_TYPE_DATA = 2
KRB5_CRYPTO_TYPE_SIGN_ONLY = 3
KRB5_CRYPTO_TYPE_PADDING = 4
KRB5_CRYPTO_TYPE_TRAILER = 5
KRB5_CRYPTO_TYPE_CHECKSUM = 6
KRB5_CRYPTO_TYPE_STREAM = 7
KDC_OPT_RESERVED = 0x80000000
KDC_OPT_FORWARDABLE = 0x40000000
KDC_OPT_FORWARDED = 0x20000000
KDC_OPT_PROXIABLE = 0x10000000
KDC_OPT_PROXY = 0x08000000
KDC_OPT_ALLOW_POSTDATE = 0x04000000
KDC_OPT_POSTDATED = 0x02000000
KDC_OPT_UNUSED = 0x01000000
KDC_OPT_RENEWABLE = 0x00800000
KDC_OPT_UNUSED = 0x00400000
KDC_OPT_RESERVED = 0x00200000
KDC_OPT_RESERVED = 0x00100000
KDC_OPT_RESERVED = 0x00080000
KDC_OPT_RESERVED = 0x00040000
KDC_OPT_CNAME_IN_ADDL_TKT = 0x00020000
KDC_OPT_CANONICALIZE = 0x00010000
KDC_OPT_REQUEST_ANONYMOUS = 0x00008000
KDC_OPT_RESERVED = 0x00004000
KDC_OPT_RESERVED = 0x00002000
KDC_OPT_RESERVED = 0x00001000
KDC_OPT_RESERVED = 0x00000800
KDC_OPT_RESERVED = 0x00000400
KDC_OPT_RESERVED = 0x00000200
KDC_OPT_RESERVED = 0x00000100
KDC_OPT_RESERVED = 0x00000080
KDC_OPT_RESERVED = 0x00000040
KDC_OPT_DISABLE_TRANSITED_CHECK = 0x00000020
KDC_OPT_RENEWABLE_OK = 0x00000010
KDC_OPT_ENC_TKT_IN_SKEY = 0x00000008
KDC_OPT_UNUSED = 0x00000004
KDC_OPT_RENEW = 0x00000002
KDC_OPT_VALIDATE = 0x00000001
KDC_TKT_COMMON_MASK = 0x54800000
AP_OPTS_RESERVED = 0x80000000
AP_OPTS_USE_SESSION_KEY = 0x40000000
AP_OPTS_MUTUAL_REQUIRED = 0x20000000
AP_OPTS_ETYPE_NEGOTIATION = 0x00000002
AP_OPTS_USE_SUBKEY = 0x00000001
AP_OPTS_RESERVED = 0x10000000
AP_OPTS_RESERVED = 0x08000000
AP_OPTS_RESERVED = 0x04000000
AP_OPTS_RESERVED = 0x02000000
AP_OPTS_RESERVED = 0x01000000
AP_OPTS_RESERVED = 0x00800000
AP_OPTS_RESERVED = 0x00400000
AP_OPTS_RESERVED = 0x00200000
AP_OPTS_RESERVED = 0x00100000
AP_OPTS_RESERVED = 0x00080000
AP_OPTS_RESERVED = 0x00040000
AP_OPTS_RESERVED = 0x00020000
AP_OPTS_RESERVED = 0x00010000
AP_OPTS_RESERVED = 0x00008000
AP_OPTS_RESERVED = 0x00004000
AP_OPTS_RESERVED = 0x00002000
AP_OPTS_RESERVED = 0x00001000
AP_OPTS_RESERVED = 0x00000800
AP_OPTS_RESERVED = 0x00000400
AP_OPTS_RESERVED = 0x00000200
AP_OPTS_RESERVED = 0x00000100
AP_OPTS_RESERVED = 0x00000080
AP_OPTS_RESERVED = 0x00000040
AP_OPTS_RESERVED = 0x00000020
AP_OPTS_RESERVED = 0x00000010
AP_OPTS_RESERVED = 0x00000008
AP_OPTS_RESERVED = 0x00000004
AP_OPTS_WIRE_MASK = 0xFFFFFFF0
AD_TYPE_RESERVED = 0x8000
AD_TYPE_EXTERNAL = 0x4000
AD_TYPE_REGISTERED = 0x2000
AD_TYPE_FIELD_TYPE_MASK = 0x1FFF
TKT_FLG_RESERVED = 0x80000000
TKT_FLG_FORWARDABLE = 0x40000000
TKT_FLG_FORWARDED = 0x20000000
TKT_FLG_PROXIABLE = 0x10000000
TKT_FLG_PROXY = 0x08000000
TKT_FLG_MAY_POSTDATE = 0x04000000
TKT_FLG_POSTDATED = 0x02000000
TKT_FLG_INVALID = 0x01000000
TKT_FLG_RENEWABLE = 0x00800000
TKT_FLG_INITIAL = 0x00400000
TKT_FLG_PRE_AUTH = 0x00200000
TKT_FLG_HW_AUTH = 0x00100000
TKT_FLG_TRANSIT_POLICY_CHECKED = 0x00080000
TKT_FLG_OK_AS_DELEGATE = 0x00040000
TKT_FLG_ENC_PA_REP = 0x00010000
TKT_FLG_ANONYMOUS = 0x00008000
TKT_FLG_RESERVED = 0x00004000
TKT_FLG_RESERVED = 0x00002000
TKT_FLG_RESERVED = 0x00001000
TKT_FLG_RESERVED = 0x00000800
TKT_FLG_RESERVED = 0x00000400
TKT_FLG_RESERVED = 0x00000200
TKT_FLG_RESERVED = 0x00000100
TKT_FLG_RESERVED = 0x00000080
TKT_FLG_RESERVED = 0x00000040
TKT_FLG_RESERVED = 0x00000020
TKT_FLG_RESERVED = 0x00000010
TKT_FLG_RESERVED = 0x00000008
TKT_FLG_RESERVED = 0x00000004
TKT_FLG_RESERVED = 0x00000002
TKT_FLG_RESERVED = 0x00000001
LR_TYPE_THIS_SERVER_ONLY = 0x8000
LR_TYPE_INTERPRETATION_MASK = 0x7FFF
MSEC_DIRBIT = 0x8000
MSEC_VAL_MASK = 0x7FFF
KRB5_PVNO = 5
KRB5_LRQ_NONE = 0
KRB5_LRQ_ALL_LAST_TGT = 1
KRB5_LRQ_ONE_LAST_TGT = -1
KRB5_LRQ_ALL_LAST_INITIAL = 2
KRB5_LRQ_ONE_LAST_INITIAL = -2
KRB5_LRQ_ALL_LAST_TGT_ISSUED = 3
KRB5_LRQ_ONE_LAST_TGT_ISSUED = -3
KRB5_LRQ_ALL_LAST_RENEWAL = 4
KRB5_LRQ_ONE_LAST_RENEWAL = -4
KRB5_LRQ_ALL_LAST_REQ = 5
KRB5_LRQ_ONE_LAST_REQ = -5
KRB5_LRQ_ALL_PW_EXPTIME = 6
KRB5_LRQ_ONE_PW_EXPTIME = -6
KRB5_LRQ_ALL_ACCT_EXPTIME = 7
KRB5_LRQ_ONE_ACCT_EXPTIME = -7
KRB5_PADATA_NONE = 0
KRB5_PADATA_AP_REQ = 1
KRB5_PADATA_ENC_TIMESTAMP = 2
KRB5_PADATA_PW_SALT = 3
KRB5_PADATA_ENC_ENCKEY = 4
KRB5_PADATA_ENC_UNIX_TIME = 5
KRB5_PADATA_ENC_SANDIA_SECURID = 6
KRB5_PADATA_SESAME = 7
KRB5_PADATA_OSF_DCE = 8
KRB5_CYBERSAFE_SECUREID = 9
KRB5_PADATA_AFS3_SALT = 10
KRB5_PADATA_ETYPE_INFO = 11
KRB5_PADATA_SAM_CHALLENGE = 12
KRB5_PADATA_SAM_RESPONSE = 13
KRB5_PADATA_PK_AS_REQ_OLD = 14
KRB5_PADATA_PK_AS_REP_OLD = 15
KRB5_PADATA_PK_AS_REQ = 16
KRB5_PADATA_PK_AS_REP = 17
KRB5_PADATA_ETYPE_INFO2 = 19
KRB5_PADATA_USE_SPECIFIED_KVNO = 20
KRB5_PADATA_SVR_REFERRAL_INFO = 20
KRB5_PADATA_SAM_REDIRECT = 21
KRB5_PADATA_GET_FROM_TYPED_DATA = 22
KRB5_PADATA_REFERRAL = 25
KRB5_PADATA_SAM_CHALLENGE_2 = 30
KRB5_PADATA_SAM_RESPONSE_2 = 31
KRB5_PADATA_PAC_REQUEST = 128
KRB5_PADATA_FOR_USER = 129
KRB5_PADATA_S4U_X509_USER = 130
KRB5_PADATA_AS_CHECKSUM = 132
KRB5_PADATA_FX_COOKIE = 133
KRB5_PADATA_FX_FAST = 136
KRB5_PADATA_FX_ERROR = 137
KRB5_PADATA_ENCRYPTED_CHALLENGE = 138
KRB5_PADATA_OTP_CHALLENGE = 141
KRB5_PADATA_OTP_REQUEST = 142
KRB5_PADATA_OTP_PIN_CHANGE = 144
KRB5_PADATA_PKINIT_KX = 147
KRB5_ENCPADATA_REQ_ENC_PA_REP = 149
KRB5_PADATA_AS_FRESHNESS = 150
KRB5_PADATA_SPAKE = 151
KRB5_PADATA_PAC_OPTIONS = 167
KRB5_SAM_USE_SAD_AS_KEY = 0x80000000
KRB5_SAM_SEND_ENCRYPTED_SAD = 0x40000000
KRB5_SAM_MUST_PK_ENCRYPT_SAD = 0x20000000
KRB5_DOMAIN_X500_COMPRESS = 1
KRB5_ALTAUTH_ATT_CHALLENGE_RESPONSE = 64
KRB5_AUTHDATA_IF_RELEVANT = 1
KRB5_AUTHDATA_KDC_ISSUED = 4
KRB5_AUTHDATA_AND_OR = 5
KRB5_AUTHDATA_MANDATORY_FOR_KDC = 8
KRB5_AUTHDATA_INITIAL_VERIFIED_CAS = 9
KRB5_AUTHDATA_OSF_DCE = 64
KRB5_AUTHDATA_SESAME = 65
KRB5_AUTHDATA_CAMMAC = 96
KRB5_AUTHDATA_WIN2K_PAC = 128
KRB5_AUTHDATA_ETYPE_NEGOTIATION = 129
KRB5_AUTHDATA_SIGNTICKET = 512
KRB5_AUTHDATA_FX_ARMOR = 71
KRB5_AUTHDATA_AUTH_INDICATOR = 97
KRB5_KPASSWD_SUCCESS = 0
KRB5_KPASSWD_MALFORMED = 1
KRB5_KPASSWD_HARDERROR = 2
KRB5_KPASSWD_AUTHERROR = 3
KRB5_KPASSWD_SOFTERROR = 4
KRB5_KPASSWD_ACCESSDENIED = 5
KRB5_KPASSWD_BAD_VERSION = 6
KRB5_KPASSWD_INITIAL_FLAG_NEEDED = 7
KRB5_AUTH_CONTEXT_DO_TIME = 0x00000001
KRB5_AUTH_CONTEXT_RET_TIME = 0x00000002
KRB5_AUTH_CONTEXT_DO_SEQUENCE = 0x00000004
KRB5_AUTH_CONTEXT_RET_SEQUENCE = 0x00000008
KRB5_AUTH_CONTEXT_PERMIT_ALL = 0x00000010
KRB5_AUTH_CONTEXT_USE_SUBKEY = 0x00000020
KRB5_AUTH_CONTEXT_GENERATE_LOCAL_ADDR = 0x00000001
KRB5_AUTH_CONTEXT_GENERATE_REMOTE_ADDR = 0x00000002
KRB5_AUTH_CONTEXT_GENERATE_LOCAL_FULL_ADDR = 0x00000004
KRB5_AUTH_CONTEXT_GENERATE_REMOTE_FULL_ADDR = 0x00000008
KRB5_TC_MATCH_TIMES = 0x00000001
KRB5_TC_MATCH_IS_SKEY = 0x00000002
KRB5_TC_MATCH_FLAGS = 0x00000004
KRB5_TC_MATCH_TIMES_EXACT = 0x00000008
KRB5_TC_MATCH_FLAGS_EXACT = 0x00000010
KRB5_TC_MATCH_AUTHDATA = 0x00000020
KRB5_TC_MATCH_SRV_NAMEONLY = 0x00000040
KRB5_TC_MATCH_2ND_TKT = 0x00000080
KRB5_TC_MATCH_KTYPE = 0x00000100
KRB5_TC_SUPPORTED_KTYPES = 0x00000200
KRB5_TC_OPENCLOSE = 0x00000001
KRB5_TC_NOTICKET = 0x00000002
MAX_KEYTAB_NAME_LEN = 1100
KRB5_INIT_CONTEXT_SECURE = 0x1
KRB5_INIT_CONTEXT_KDC = 0x2
KRB5_GC_USER_USER = 1
KRB5_GC_CACHED = 2
KRB5_GC_CANONICALIZE = 4
KRB5_GC_NO_STORE = 8
KRB5_GC_FORWARDABLE = 16
KRB5_GC_NO_TRANSIT_CHECK = 32
KRB5_GC_CONSTRAINED_DELEGATION = 64
KRB5_PRINCIPAL_PARSE_NO_REALM = 0x1
KRB5_PRINCIPAL_PARSE_REQUIRE_REALM = 0x2
KRB5_PRINCIPAL_PARSE_ENTERPRISE = 0x4
KRB5_PRINCIPAL_PARSE_IGNORE_REALM = 0x8
KRB5_PRINCIPAL_UNPARSE_SHORT = 0x1
KRB5_PRINCIPAL_UNPARSE_NO_REALM = 0x2
KRB5_PRINCIPAL_UNPARSE_DISPLAY = 0x4
KRB5_PRINCIPAL_COMPARE_IGNORE_REALM = 1
KRB5_PRINCIPAL_COMPARE_ENTERPRISE = 2
KRB5_PRINCIPAL_COMPARE_CASEFOLD = 4
KRB5_PRINCIPAL_COMPARE_UTF8 = 8
KRB5_TGS_NAME_SIZE = 6
KRB5_RECVAUTH_SKIP_VERSION = 0x0001
KRB5_RECVAUTH_BADAUTHVERS = 0x0002
KRB5_RESPONDER_OTP_FORMAT_DECIMAL = 0
KRB5_RESPONDER_OTP_FORMAT_HEXADECIMAL = 1
KRB5_RESPONDER_OTP_FORMAT_ALPHANUMERIC = 2
KRB5_RESPONDER_OTP_FLAGS_COLLECT_TOKEN = 0x0001
KRB5_RESPONDER_OTP_FLAGS_COLLECT_PIN = 0x0002
KRB5_RESPONDER_OTP_FLAGS_NEXTOTP = 0x0004
KRB5_RESPONDER_OTP_FLAGS_SEPARATE_PIN = 0x0008
KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_COUNT_LOW = 1
KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_FINAL_TRY = 1
KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_LOCKED = 1
KRB5_GET_INIT_CREDS_OPT_TKT_LIFE = 0x0001
KRB5_GET_INIT_CREDS_OPT_RENEW_LIFE = 0x0002
KRB5_GET_INIT_CREDS_OPT_FORWARDABLE = 0x0004
KRB5_GET_INIT_CREDS_OPT_PROXIABLE = 0x0008
KRB5_GET_INIT_CREDS_OPT_ETYPE_LIST = 0x0010
KRB5_GET_INIT_CREDS_OPT_ADDRESS_LIST = 0x0020
KRB5_GET_INIT_CREDS_OPT_PREAUTH_LIST = 0x0040
KRB5_GET_INIT_CREDS_OPT_SALT = 0x0080
KRB5_GET_INIT_CREDS_OPT_CHG_PWD_PRMPT = 0x0100
KRB5_GET_INIT_CREDS_OPT_CANONICALIZE = 0x0200
KRB5_GET_INIT_CREDS_OPT_ANONYMOUS = 0x0400
KRB5_FAST_REQUIRED = 0x0001
KRB5_INIT_CREDS_STEP_FLAG_CONTINUE = 0x1
KRB5_TKT_CREDS_STEP_FLAG_CONTINUE = 0x1
KRB5_VERIFY_INIT_CREDS_OPT_AP_REQ_NOFAIL = 0x0001
KRB5_PROMPT_TYPE_PASSWORD = 0x1
KRB5_PROMPT_TYPE_NEW_PASSWORD = 0x2
KRB5_PROMPT_TYPE_NEW_PASSWORD_AGAIN = 0x3
KRB5_PROMPT_TYPE_PREAUTH = 0x4
KRB5_PAC_LOGON_INFO = 1
KRB5_PAC_CREDENTIALS_INFO = 2
KRB5_PAC_SERVER_CHECKSUM = 6
KRB5_PAC_PRIVSVR_CHECKSUM = 7
KRB5_PAC_CLIENT_INFO = 10
KRB5_PAC_DELEGATION_INFO = 11
KRB5_PAC_UPN_DNS_INFO = 12
KRB5KDC_ERR_NONE = -1765328384
KRB5KDC_ERR_NAME_EXP = -1765328383
KRB5KDC_ERR_SERVICE_EXP = -1765328382
KRB5KDC_ERR_BAD_PVNO = -1765328381
KRB5KDC_ERR_C_OLD_MAST_KVNO = -1765328380
KRB5KDC_ERR_S_OLD_MAST_KVNO = -1765328379
KRB5KDC_ERR_C_PRINCIPAL_UNKNOWN = -1765328378
KRB5KDC_ERR_S_PRINCIPAL_UNKNOWN = -1765328377
KRB5KDC_ERR_PRINCIPAL_NOT_UNIQUE = -1765328376
KRB5KDC_ERR_NULL_KEY = -1765328375
KRB5KDC_ERR_CANNOT_POSTDATE = -1765328374
KRB5KDC_ERR_NEVER_VALID = -1765328373
KRB5KDC_ERR_POLICY = -1765328372
KRB5KDC_ERR_BADOPTION = -1765328371
KRB5KDC_ERR_ETYPE_NOSUPP = -1765328370
KRB5KDC_ERR_SUMTYPE_NOSUPP = -1765328369
KRB5KDC_ERR_PADATA_TYPE_NOSUPP = -1765328368
KRB5KDC_ERR_TRTYPE_NOSUPP = -1765328367
KRB5KDC_ERR_CLIENT_REVOKED = -1765328366
KRB5KDC_ERR_SERVICE_REVOKED = -1765328365
KRB5KDC_ERR_TGT_REVOKED = -1765328364
KRB5KDC_ERR_CLIENT_NOTYET = -1765328363
KRB5KDC_ERR_SERVICE_NOTYET = -1765328362
KRB5KDC_ERR_KEY_EXP = -1765328361
KRB5KDC_ERR_PREAUTH_FAILED = -1765328360
KRB5KDC_ERR_PREAUTH_REQUIRED = -1765328359
KRB5KDC_ERR_SERVER_NOMATCH = -1765328358
KRB5KDC_ERR_MUST_USE_USER2USER = -1765328357
KRB5KDC_ERR_PATH_NOT_ACCEPTED = -1765328356
KRB5KDC_ERR_SVC_UNAVAILABLE = -1765328355
KRB5PLACEHOLD_30 = -1765328354
KRB5KRB_AP_ERR_BAD_INTEGRITY = -1765328353
KRB5KRB_AP_ERR_TKT_EXPIRED = -1765328352
KRB5KRB_AP_ERR_TKT_NYV = -1765328351
KRB5KRB_AP_ERR_REPEAT = -1765328350
KRB5KRB_AP_ERR_NOT_US = -1765328349
KRB5KRB_AP_ERR_BADMATCH = -1765328348
KRB5KRB_AP_ERR_SKEW = -1765328347
KRB5KRB_AP_ERR_BADADDR = -1765328346
KRB5KRB_AP_ERR_BADVERSION = -1765328345
KRB5KRB_AP_ERR_MSG_TYPE = -1765328344
KRB5KRB_AP_ERR_MODIFIED = -1765328343
KRB5KRB_AP_ERR_BADORDER = -1765328342
KRB5KRB_AP_ERR_ILL_CR_TKT = -1765328341
KRB5KRB_AP_ERR_BADKEYVER = -1765328340
KRB5KRB_AP_ERR_NOKEY = -1765328339
KRB5KRB_AP_ERR_MUT_FAIL = -1765328338
KRB5KRB_AP_ERR_BADDIRECTION = -1765328337
KRB5KRB_AP_ERR_METHOD = -1765328336
KRB5KRB_AP_ERR_BADSEQ = -1765328335
KRB5KRB_AP_ERR_INAPP_CKSUM = -1765328334
KRB5KRB_AP_PATH_NOT_ACCEPTED = -1765328333
KRB5KRB_ERR_RESPONSE_TOO_BIG = -1765328332
KRB5PLACEHOLD_53 = -1765328331
KRB5PLACEHOLD_54 = -1765328330
KRB5PLACEHOLD_55 = -1765328329
KRB5PLACEHOLD_56 = -1765328328
KRB5PLACEHOLD_57 = -1765328327
KRB5PLACEHOLD_58 = -1765328326
KRB5PLACEHOLD_59 = -1765328325
KRB5KRB_ERR_GENERIC = -1765328324
KRB5KRB_ERR_FIELD_TOOLONG = -1765328323
KRB5KDC_ERR_CLIENT_NOT_TRUSTED = -1765328322
KRB5KDC_ERR_KDC_NOT_TRUSTED = -1765328321
KRB5KDC_ERR_INVALID_SIG = -1765328320
KRB5KDC_ERR_DH_KEY_PARAMETERS_NOT_ACCEPTED = -1765328319
KRB5KDC_ERR_CERTIFICATE_MISMATCH = -1765328318
KRB5KRB_AP_ERR_NO_TGT = -1765328317
KRB5KDC_ERR_WRONG_REALM = -1765328316
KRB5KRB_AP_ERR_USER_TO_USER_REQUIRED = -1765328315
KRB5KDC_ERR_CANT_VERIFY_CERTIFICATE = -1765328314
KRB5KDC_ERR_INVALID_CERTIFICATE = -1765328313
KRB5KDC_ERR_REVOKED_CERTIFICATE = -1765328312
KRB5KDC_ERR_REVOCATION_STATUS_UNKNOWN = -1765328311
KRB5KDC_ERR_REVOCATION_STATUS_UNAVAILABLE = -1765328310
KRB5KDC_ERR_CLIENT_NAME_MISMATCH = -1765328309
KRB5KDC_ERR_KDC_NAME_MISMATCH = -1765328308
KRB5KDC_ERR_INCONSISTENT_KEY_PURPOSE = -1765328307
KRB5KDC_ERR_DIGEST_IN_CERT_NOT_ACCEPTED = -1765328306
KRB5KDC_ERR_PA_CHECKSUM_MUST_BE_INCLUDED = -1765328305
KRB5KDC_ERR_DIGEST_IN_SIGNED_DATA_NOT_ACCEPTED = -1765328304
KRB5KDC_ERR_PUBLIC_KEY_ENCRYPTION_NOT_SUPPORTED = -1765328303
KRB5PLACEHOLD_82 = -1765328302
KRB5PLACEHOLD_83 = -1765328301
KRB5PLACEHOLD_84 = -1765328300
KRB5KRB_AP_ERR_IAKERB_KDC_NOT_FOUND = -1765328299
KRB5KRB_AP_ERR_IAKERB_KDC_NO_RESPONSE = -1765328298
KRB5PLACEHOLD_87 = -1765328297
KRB5PLACEHOLD_88 = -1765328296
KRB5PLACEHOLD_89 = -1765328295
KRB5KDC_ERR_PREAUTH_EXPIRED = -1765328294
KRB5KDC_ERR_MORE_PREAUTH_DATA_REQUIRED = -1765328293
KRB5PLACEHOLD_92 = -1765328292
KRB5KDC_ERR_UNKNOWN_CRITICAL_FAST_OPTION = -1765328291
KRB5PLACEHOLD_94 = -1765328290
KRB5PLACEHOLD_95 = -1765328289
KRB5PLACEHOLD_96 = -1765328288
KRB5PLACEHOLD_97 = -1765328287
KRB5PLACEHOLD_98 = -1765328286
KRB5PLACEHOLD_99 = -1765328285
KRB5KDC_ERR_NO_ACCEPTABLE_KDF = -1765328284
KRB5PLACEHOLD_101 = -1765328283
KRB5PLACEHOLD_102 = -1765328282
KRB5PLACEHOLD_103 = -1765328281
KRB5PLACEHOLD_104 = -1765328280
KRB5PLACEHOLD_105 = -1765328279
KRB5PLACEHOLD_106 = -1765328278
KRB5PLACEHOLD_107 = -1765328277
KRB5PLACEHOLD_108 = -1765328276
KRB5PLACEHOLD_109 = -1765328275
KRB5PLACEHOLD_110 = -1765328274
KRB5PLACEHOLD_111 = -1765328273
KRB5PLACEHOLD_112 = -1765328272
KRB5PLACEHOLD_113 = -1765328271
KRB5PLACEHOLD_114 = -1765328270
KRB5PLACEHOLD_115 = -1765328269
KRB5PLACEHOLD_116 = -1765328268
KRB5PLACEHOLD_117 = -1765328267
KRB5PLACEHOLD_118 = -1765328266
KRB5PLACEHOLD_119 = -1765328265
KRB5PLACEHOLD_120 = -1765328264
KRB5PLACEHOLD_121 = -1765328263
KRB5PLACEHOLD_122 = -1765328262
KRB5PLACEHOLD_123 = -1765328261
KRB5PLACEHOLD_124 = -1765328260
KRB5PLACEHOLD_125 = -1765328259
KRB5PLACEHOLD_126 = -1765328258
KRB5PLACEHOLD_127 = -1765328257
KRB5_ERR_RCSID = -1765328256
KRB5_LIBOS_BADLOCKFLAG = -1765328255
KRB5_LIBOS_CANTREADPWD = -1765328254
KRB5_LIBOS_BADPWDMATCH = -1765328253
KRB5_LIBOS_PWDINTR = -1765328252
KRB5_PARSE_ILLCHAR = -1765328251
KRB5_PARSE_MALFORMED = -1765328250
KRB5_CONFIG_CANTOPEN = -1765328249
KRB5_CONFIG_BADFORMAT = -1765328248
KRB5_CONFIG_NOTENUFSPACE = -1765328247
KRB5_BADMSGTYPE = -1765328246
KRB5_CC_BADNAME = -1765328245
KRB5_CC_UNKNOWN_TYPE = -1765328244
KRB5_CC_NOTFOUND = -1765328243
KRB5_CC_END = -1765328242
KRB5_NO_TKT_SUPPLIED = -1765328241
KRB5KRB_AP_WRONG_PRINC = -1765328240
KRB5KRB_AP_ERR_TKT_INVALID = -1765328239
KRB5_PRINC_NOMATCH = -1765328238
KRB5_KDCREP_MODIFIED = -1765328237
KRB5_KDCREP_SKEW = -1765328236
KRB5_IN_TKT_REALM_MISMATCH = -1765328235
KRB5_PROG_ETYPE_NOSUPP = -1765328234
KRB5_PROG_KEYTYPE_NOSUPP = -1765328233
KRB5_WRONG_ETYPE = -1765328232
KRB5_PROG_SUMTYPE_NOSUPP = -1765328231
KRB5_REALM_UNKNOWN = -1765328230
KRB5_SERVICE_UNKNOWN = -1765328229
KRB5_KDC_UNREACH = -1765328228
KRB5_NO_LOCALNAME = -1765328227
KRB5_MUTUAL_FAILED = -1765328226
KRB5_RC_TYPE_EXISTS = -1765328225
KRB5_RC_MALLOC = -1765328224
KRB5_RC_TYPE_NOTFOUND = -1765328223
KRB5_RC_UNKNOWN = -1765328222
KRB5_RC_REPLAY = -1765328221
KRB5_RC_IO = -1765328220
KRB5_RC_NOIO = -1765328219
KRB5_RC_PARSE = -1765328218
KRB5_RC_IO_EOF = -1765328217
KRB5_RC_IO_MALLOC = -1765328216
KRB5_RC_IO_PERM = -1765328215
KRB5_RC_IO_IO = -1765328214
KRB5_RC_IO_UNKNOWN = -1765328213
KRB5_RC_IO_SPACE = -1765328212
KRB5_TRANS_CANTOPEN = -1765328211
KRB5_TRANS_BADFORMAT = -1765328210
KRB5_LNAME_CANTOPEN = -1765328209
KRB5_LNAME_NOTRANS = -1765328208
KRB5_LNAME_BADFORMAT = -1765328207
KRB5_CRYPTO_INTERNAL = -1765328206
KRB5_KT_BADNAME = -1765328205
KRB5_KT_UNKNOWN_TYPE = -1765328204
KRB5_KT_NOTFOUND = -1765328203
KRB5_KT_END = -1765328202
KRB5_KT_NOWRITE = -1765328201
KRB5_KT_IOERR = -1765328200
KRB5_NO_TKT_IN_RLM = -1765328199
KRB5DES_BAD_KEYPAR = -1765328198
KRB5DES_WEAK_KEY = -1765328197
KRB5_BAD_ENCTYPE = -1765328196
KRB5_BAD_KEYSIZE = -1765328195
KRB5_BAD_MSIZE = -1765328194
KRB5_CC_TYPE_EXISTS = -1765328193
KRB5_KT_TYPE_EXISTS = -1765328192
KRB5_CC_IO = -1765328191
KRB5_FCC_PERM = -1765328190
KRB5_FCC_NOFILE = -1765328189
KRB5_FCC_INTERNAL = -1765328188
KRB5_CC_WRITE = -1765328187
KRB5_CC_NOMEM = -1765328186
KRB5_CC_FORMAT = -1765328185
KRB5_CC_NOT_KTYPE = -1765328184
KRB5_INVALID_FLAGS = -1765328183
KRB5_NO_2ND_TKT = -1765328182
KRB5_NOCREDS_SUPPLIED = -1765328181
KRB5_SENDAUTH_BADAUTHVERS = -1765328180
KRB5_SENDAUTH_BADAPPLVERS = -1765328179
KRB5_SENDAUTH_BADRESPONSE = -1765328178
KRB5_SENDAUTH_REJECTED = -1765328177
KRB5_PREAUTH_BAD_TYPE = -1765328176
KRB5_PREAUTH_NO_KEY = -1765328175
KRB5_PREAUTH_FAILED = -1765328174
KRB5_RCACHE_BADVNO = -1765328173
KRB5_CCACHE_BADVNO = -1765328172
KRB5_KEYTAB_BADVNO = -1765328171
KRB5_PROG_ATYPE_NOSUPP = -1765328170
KRB5_RC_REQUIRED = -1765328169
KRB5_ERR_BAD_HOSTNAME = -1765328168
KRB5_ERR_HOST_REALM_UNKNOWN = -1765328167
KRB5_SNAME_UNSUPP_NAMETYPE = -1765328166
KRB5KRB_AP_ERR_V4_REPLY = -1765328165
KRB5_REALM_CANT_RESOLVE = -1765328164
KRB5_TKT_NOT_FORWARDABLE = -1765328163
KRB5_FWD_BAD_PRINCIPAL = -1765328162
KRB5_GET_IN_TKT_LOOP = -1765328161
KRB5_CONFIG_NODEFREALM = -1765328160
KRB5_SAM_UNSUPPORTED = -1765328159
KRB5_SAM_INVALID_ETYPE = -1765328158
KRB5_SAM_NO_CHECKSUM = -1765328157
KRB5_SAM_BAD_CHECKSUM = -1765328156
KRB5_KT_NAME_TOOLONG = -1765328155
KRB5_KT_KVNONOTFOUND = -1765328154
KRB5_APPL_EXPIRED = -1765328153
KRB5_LIB_EXPIRED = -1765328152
KRB5_CHPW_PWDNULL = -1765328151
KRB5_CHPW_FAIL = -1765328150
KRB5_KT_FORMAT = -1765328149
KRB5_NOPERM_ETYPE = -1765328148
KRB5_CONFIG_ETYPE_NOSUPP = -1765328147
KRB5_OBSOLETE_FN = -1765328146
KRB5_EAI_FAIL = -1765328145
KRB5_EAI_NODATA = -1765328144
KRB5_EAI_NONAME = -1765328143
KRB5_EAI_SERVICE = -1765328142
KRB5_ERR_NUMERIC_REALM = -1765328141
KRB5_ERR_BAD_S2K_PARAMS = -1765328140
KRB5_ERR_NO_SERVICE = -1765328139
KRB5_CC_READONLY = -1765328138
KRB5_CC_NOSUPP = -1765328137
KRB5_DELTAT_BADFORMAT = -1765328136
KRB5_PLUGIN_NO_HANDLE = -1765328135
KRB5_PLUGIN_OP_NOTSUPP = -1765328134
KRB5_ERR_INVALID_UTF8 = -1765328133
KRB5_ERR_FAST_REQUIRED = -1765328132
KRB5_LOCAL_ADDR_REQUIRED = -1765328131
KRB5_REMOTE_ADDR_REQUIRED = -1765328130
KRB5_TRACE_NOSUPP = -1765328129
KRB5_PLUGIN_VER_NOTSUPP = -1750600192
KRB5_PLUGIN_BAD_MODULE_SPEC = -1750600191
KRB5_PLUGIN_NAME_NOTFOUND = -1750600190
KRB5KDC_ERR_DISCARD = -1750600189
KRB5_DCC_CANNOT_CREATE = -1750600188
KRB5_KCC_INVALID_ANCHOR = -1750600187
KRB5_KCC_UNKNOWN_VERSION = -1750600186
KRB5_KCC_INVALID_UID = -1750600185
KRB5_KCM_MALFORMED_REPLY = -1750600184
KRB5_KCM_RPC_ERROR = -1750600183
KRB5_KCM_REPLY_TOO_BIG = -1750600182
KRB5_KCM_NO_SERVER = -1750600181
KRB5_KDB_RCSID = -1780008448
KRB5_KDB_INUSE = -1780008447
KRB5_KDB_UK_SERROR = -1780008446
KRB5_KDB_UK_RERROR = -1780008445
KRB5_KDB_UNAUTH = -1780008444
KRB5_KDB_NOENTRY = -1780008443
KRB5_KDB_ILL_WILDCARD = -1780008442
KRB5_KDB_DB_INUSE = -1780008441
KRB5_KDB_DB_CHANGED = -1780008440
KRB5_KDB_TRUNCATED_RECORD = -1780008439
KRB5_KDB_RECURSIVELOCK = -1780008438
KRB5_KDB_NOTLOCKED = -1780008437
KRB5_KDB_BADLOCKMODE = -1780008436
KRB5_KDB_DBNOTINITED = -1780008435
KRB5_KDB_DBINITED = -1780008434
KRB5_KDB_ILLDIRECTION = -1780008433
KRB5_KDB_NOMASTERKEY = -1780008432
KRB5_KDB_BADMASTERKEY = -1780008431
KRB5_KDB_INVALIDKEYSIZE = -1780008430
KRB5_KDB_CANTREAD_STORED = -1780008429
KRB5_KDB_BADSTORED_MKEY = -1780008428
KRB5_KDB_NOACTMASTERKEY = -1780008427
KRB5_KDB_KVNONOMATCH = -1780008426
KRB5_KDB_STORED_MKEY_NOTCURRENT = -1780008425
KRB5_KDB_CANTLOCK_DB = -1780008424
KRB5_KDB_DB_CORRUPT = -1780008423
KRB5_KDB_BAD_VERSION = -1780008422
KRB5_KDB_BAD_SALTTYPE = -1780008421
KRB5_KDB_BAD_ENCTYPE = -1780008420
KRB5_KDB_BAD_CREATEFLAGS = -1780008419
KRB5_KDB_NO_PERMITTED_KEY = -1780008418
KRB5_KDB_NO_MATCHING_KEY = -1780008417
KRB5_KDB_DBTYPE_NOTFOUND = -1780008416
KRB5_KDB_DBTYPE_NOSUP = -1780008415
KRB5_KDB_DBTYPE_INIT = -1780008414
KRB5_KDB_SERVER_INTERNAL_ERR = -1780008413
KRB5_KDB_ACCESS_ERROR = -1780008412
KRB5_KDB_INTERNAL_ERROR = -1780008411
KRB5_KDB_CONSTRAINT_VIOLATION = -1780008410
KRB5_LOG_CONV = -1780008409
KRB5_LOG_UNSTABLE = -1780008408
KRB5_LOG_CORRUPT = -1780008407
KRB5_LOG_ERROR = -1780008406
KRB5_KDB_DBTYPE_MISMATCH = -1780008405
KRB5_KDB_POLICY_REF = -1780008404
KRB5_KDB_STRINGS_TOOLONG = -1780008403
KV5M_NONE = -1760647424
KV5M_PRINCIPAL = -1760647423
KV5M_DATA = -1760647422
KV5M_KEYBLOCK = -1760647421
KV5M_CHECKSUM = -1760647420
KV5M_ENCRYPT_BLOCK = -1760647419
KV5M_ENC_DATA = -1760647418
KV5M_CRYPTOSYSTEM_ENTRY = -1760647417
KV5M_CS_TABLE_ENTRY = -1760647416
KV5M_CHECKSUM_ENTRY = -1760647415
KV5M_AUTHDATA = -1760647414
KV5M_TRANSITED = -1760647413
KV5M_ENC_TKT_PART = -1760647412
KV5M_TICKET = -1760647411
KV5M_AUTHENTICATOR = -1760647410
KV5M_TKT_AUTHENT = -1760647409
KV5M_CREDS = -1760647408
KV5M_LAST_REQ_ENTRY = -1760647407
KV5M_PA_DATA = -1760647406
KV5M_KDC_REQ = -1760647405
KV5M_ENC_KDC_REP_PART = -1760647404
KV5M_KDC_REP = -1760647403
KV5M_ERROR = -1760647402
KV5M_AP_REQ = -1760647401
KV5M_AP_REP = -1760647400
KV5M_AP_REP_ENC_PART = -1760647399
KV5M_RESPONSE = -1760647398
KV5M_SAFE = -1760647397
KV5M_PRIV = -1760647396
KV5M_PRIV_ENC_PART = -1760647395
KV5M_CRED = -1760647394
KV5M_CRED_INFO = -1760647393
KV5M_CRED_ENC_PART = -1760647392
KV5M_PWD_DATA = -1760647391
KV5M_ADDRESS = -1760647390
KV5M_KEYTAB_ENTRY = -1760647389
KV5M_CONTEXT = -1760647388
KV5M_OS_CONTEXT = -1760647387
KV5M_ALT_METHOD = -1760647386
KV5M_ETYPE_INFO_ENTRY = -1760647385
KV5M_DB_CONTEXT = -1760647384
KV5M_AUTH_CONTEXT = -1760647383
KV5M_KEYTAB = -1760647382
KV5M_RCACHE = -1760647381
KV5M_CCACHE = -1760647380
KV5M_PREAUTH_OPS = -1760647379
KV5M_SAM_CHALLENGE = -1760647378
KV5M_SAM_CHALLENGE_2 = -1760647377
KV5M_SAM_KEY = -1760647376
KV5M_ENC_SAM_RESPONSE_ENC = -1760647375
KV5M_ENC_SAM_RESPONSE_ENC_2 = -1760647374
KV5M_SAM_RESPONSE = -1760647373
KV5M_SAM_RESPONSE_2 = -1760647372
KV5M_PREDICTED_SAM_RESPONSE = -1760647371
KV5M_PASSWD_PHRASE_ELEMENT = -1760647370
KV5M_GSS_OID = -1760647369
KV5M_GSS_QUEUE = -1760647368
KV5M_FAST_ARMORED_REQ = -1760647367
KV5M_FAST_REQ = -1760647366
KV5M_FAST_RESPONSE = -1760647365
KV5M_AUTHDATA_CONTEXT = -1760647364
KRB524_BADKEY = -1750206208
KRB524_BADADDR = -1750206207
KRB524_BADPRINC = -1750206206
KRB524_BADREALM = -1750206205
KRB524_V4ERR = -1750206204
KRB524_ENCFULL = -1750206203
KRB524_DECEMPTY = -1750206202
KRB524_NOTRESP = -1750206201
KRB524_KRB4_DISABLED = -1750206200
ASN1_BAD_TIMEFORMAT = 1859794432
ASN1_MISSING_FIELD = 1859794433
ASN1_MISPLACED_FIELD = 1859794434
ASN1_TYPE_MISMATCH = 1859794435
ASN1_OVERFLOW = 1859794436
ASN1_OVERRUN = 1859794437
ASN1_BAD_ID = 1859794438
ASN1_BAD_LENGTH = 1859794439
ASN1_BAD_FORMAT = 1859794440
ASN1_PARSE_ERROR = 1859794441
ASN1_BAD_GMTIME = 1859794442
ASN1_MISMATCH_INDEF = 1859794443
ASN1_MISSING_EOC = 1859794444
ASN1_OMITTED = 1859794445
__all__ += [
    "KRB5_DEPRECATED",
    "KRB5_INT32_MAX",
    "KRB5_INT16_MAX",
    "FALSE",
    "TRUE",
    "KRB5_NT_UNKNOWN",
    "KRB5_NT_PRINCIPAL",
    "KRB5_NT_SRV_INST",
    "KRB5_NT_SRV_HST",
    "KRB5_NT_SRV_XHST",
    "KRB5_NT_UID",
    "KRB5_NT_X500_PRINCIPAL",
    "KRB5_NT_SMTP_NAME",
    "KRB5_NT_ENTERPRISE_PRINCIPAL",
    "KRB5_NT_WELLKNOWN",
    "KRB5_NT_MS_PRINCIPAL",
    "KRB5_NT_MS_PRINCIPAL_AND_ID",
    "KRB5_NT_ENT_PRINCIPAL_AND_ID",
    "ADDRTYPE_INET",
    "ADDRTYPE_CHAOS",
    "ADDRTYPE_XNS",
    "ADDRTYPE_ISO",
    "ADDRTYPE_DDP",
    "ADDRTYPE_NETBIOS",
    "ADDRTYPE_INET6",
    "ADDRTYPE_ADDRPORT",
    "ADDRTYPE_IPPORT",
    "ENCTYPE_NULL",
    "ENCTYPE_DES_CBC_CRC",
    "ENCTYPE_DES_CBC_MD4",
    "ENCTYPE_DES_CBC_MD5",
    "ENCTYPE_DES_CBC_RAW",
    "ENCTYPE_DES3_CBC_SHA",
    "ENCTYPE_DES3_CBC_RAW",
    "ENCTYPE_DES_HMAC_SHA1",
    "ENCTYPE_DSA_SHA1_CMS",
    "ENCTYPE_MD5_RSA_CMS",
    "ENCTYPE_SHA1_RSA_CMS",
    "ENCTYPE_RC2_CBC_ENV",
    "ENCTYPE_RSA_ENV",
    "ENCTYPE_RSA_ES_OAEP_ENV",
    "ENCTYPE_DES3_CBC_ENV",
    "ENCTYPE_DES3_CBC_SHA1",
    "ENCTYPE_AES128_CTS_HMAC_SHA1_96",
    "ENCTYPE_AES256_CTS_HMAC_SHA1_96",
    "ENCTYPE_AES128_CTS_HMAC_SHA256_128",
    "ENCTYPE_AES256_CTS_HMAC_SHA384_192",
    "ENCTYPE_ARCFOUR_HMAC",
    "ENCTYPE_ARCFOUR_HMAC_EXP",
    "ENCTYPE_CAMELLIA128_CTS_CMAC",
    "ENCTYPE_CAMELLIA256_CTS_CMAC",
    "ENCTYPE_UNKNOWN",
    "CKSUMTYPE_CRC32",
    "CKSUMTYPE_RSA_MD4",
    "CKSUMTYPE_RSA_MD4_DES",
    "CKSUMTYPE_DESCBC",
    "CKSUMTYPE_RSA_MD5",
    "CKSUMTYPE_RSA_MD5_DES",
    "CKSUMTYPE_NIST_SHA",
    "CKSUMTYPE_HMAC_SHA1_DES3",
    "CKSUMTYPE_HMAC_SHA1_96_AES128",
    "CKSUMTYPE_HMAC_SHA1_96_AES256",
    "CKSUMTYPE_HMAC_SHA256_128_AES128",
    "CKSUMTYPE_HMAC_SHA384_192_AES256",
    "CKSUMTYPE_CMAC_CAMELLIA128",
    "CKSUMTYPE_CMAC_CAMELLIA256",
    "CKSUMTYPE_MD5_HMAC_ARCFOUR",
    "CKSUMTYPE_HMAC_MD5_ARCFOUR",
    "KRB5_KEYUSAGE_AS_REQ_PA_ENC_TS",
    "KRB5_KEYUSAGE_KDC_REP_TICKET",
    "KRB5_KEYUSAGE_AS_REP_ENCPART",
    "KRB5_KEYUSAGE_TGS_REQ_AD_SESSKEY",
    "KRB5_KEYUSAGE_TGS_REQ_AD_SUBKEY",
    "KRB5_KEYUSAGE_TGS_REQ_AUTH_CKSUM",
    "KRB5_KEYUSAGE_TGS_REQ_AUTH",
    "KRB5_KEYUSAGE_TGS_REP_ENCPART_SESSKEY",
    "KRB5_KEYUSAGE_TGS_REP_ENCPART_SUBKEY",
    "KRB5_KEYUSAGE_AP_REQ_AUTH_CKSUM",
    "KRB5_KEYUSAGE_AP_REQ_AUTH",
    "KRB5_KEYUSAGE_AP_REP_ENCPART",
    "KRB5_KEYUSAGE_KRB_PRIV_ENCPART",
    "KRB5_KEYUSAGE_KRB_CRED_ENCPART",
    "KRB5_KEYUSAGE_KRB_SAFE_CKSUM",
    "KRB5_KEYUSAGE_APP_DATA_ENCRYPT",
    "KRB5_KEYUSAGE_APP_DATA_CKSUM",
    "KRB5_KEYUSAGE_KRB_ERROR_CKSUM",
    "KRB5_KEYUSAGE_AD_KDCISSUED_CKSUM",
    "KRB5_KEYUSAGE_AD_MTE",
    "KRB5_KEYUSAGE_AD_ITE",
    "KRB5_KEYUSAGE_GSS_TOK_MIC",
    "KRB5_KEYUSAGE_GSS_TOK_WRAP_INTEG",
    "KRB5_KEYUSAGE_GSS_TOK_WRAP_PRIV",
    "KRB5_KEYUSAGE_PA_SAM_CHALLENGE_CKSUM",
    "KRB5_KEYUSAGE_PA_SAM_CHALLENGE_TRACKID",
    "KRB5_KEYUSAGE_PA_SAM_RESPONSE",
    "KRB5_KEYUSAGE_PA_S4U_X509_USER_REQUEST",
    "KRB5_KEYUSAGE_PA_S4U_X509_USER_REPLY",
    "KRB5_KEYUSAGE_PA_REFERRAL",
    "KRB5_KEYUSAGE_AD_SIGNEDPATH",
    "KRB5_KEYUSAGE_IAKERB_FINISHED",
    "KRB5_KEYUSAGE_PA_PKINIT_KX",
    "KRB5_KEYUSAGE_PA_OTP_REQUEST",
    "KRB5_KEYUSAGE_FAST_REQ_CHKSUM",
    "KRB5_KEYUSAGE_FAST_ENC",
    "KRB5_KEYUSAGE_FAST_REP",
    "KRB5_KEYUSAGE_FAST_FINISHED",
    "KRB5_KEYUSAGE_ENC_CHALLENGE_CLIENT",
    "KRB5_KEYUSAGE_ENC_CHALLENGE_KDC",
    "KRB5_KEYUSAGE_AS_REQ",
    "KRB5_KEYUSAGE_CAMMAC",
    "KRB5_KEYUSAGE_SPAKE",
    "KRB5_KEYUSAGE_PA_FX_COOKIE",
    "KRB5_KEYUSAGE_PA_AS_FRESHNESS",
    "KRB5_CRYPTO_TYPE_EMPTY",
    "KRB5_CRYPTO_TYPE_HEADER",
    "KRB5_CRYPTO_TYPE_DATA",
    "KRB5_CRYPTO_TYPE_SIGN_ONLY",
    "KRB5_CRYPTO_TYPE_PADDING",
    "KRB5_CRYPTO_TYPE_TRAILER",
    "KRB5_CRYPTO_TYPE_CHECKSUM",
    "KRB5_CRYPTO_TYPE_STREAM",
    "KDC_OPT_RESERVED",
    "KDC_OPT_FORWARDABLE",
    "KDC_OPT_FORWARDED",
    "KDC_OPT_PROXIABLE",
    "KDC_OPT_PROXY",
    "KDC_OPT_ALLOW_POSTDATE",
    "KDC_OPT_POSTDATED",
    "KDC_OPT_UNUSED",
    "KDC_OPT_RENEWABLE",
    "KDC_OPT_UNUSED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_CNAME_IN_ADDL_TKT",
    "KDC_OPT_CANONICALIZE",
    "KDC_OPT_REQUEST_ANONYMOUS",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_RESERVED",
    "KDC_OPT_DISABLE_TRANSITED_CHECK",
    "KDC_OPT_RENEWABLE_OK",
    "KDC_OPT_ENC_TKT_IN_SKEY",
    "KDC_OPT_UNUSED",
    "KDC_OPT_RENEW",
    "KDC_OPT_VALIDATE",
    "KDC_TKT_COMMON_MASK",
    "AP_OPTS_RESERVED",
    "AP_OPTS_USE_SESSION_KEY",
    "AP_OPTS_MUTUAL_REQUIRED",
    "AP_OPTS_ETYPE_NEGOTIATION",
    "AP_OPTS_USE_SUBKEY",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_RESERVED",
    "AP_OPTS_WIRE_MASK",
    "AD_TYPE_RESERVED",
    "AD_TYPE_EXTERNAL",
    "AD_TYPE_REGISTERED",
    "AD_TYPE_FIELD_TYPE_MASK",
    "TKT_FLG_RESERVED",
    "TKT_FLG_FORWARDABLE",
    "TKT_FLG_FORWARDED",
    "TKT_FLG_PROXIABLE",
    "TKT_FLG_PROXY",
    "TKT_FLG_MAY_POSTDATE",
    "TKT_FLG_POSTDATED",
    "TKT_FLG_INVALID",
    "TKT_FLG_RENEWABLE",
    "TKT_FLG_INITIAL",
    "TKT_FLG_PRE_AUTH",
    "TKT_FLG_HW_AUTH",
    "TKT_FLG_TRANSIT_POLICY_CHECKED",
    "TKT_FLG_OK_AS_DELEGATE",
    "TKT_FLG_ENC_PA_REP",
    "TKT_FLG_ANONYMOUS",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "TKT_FLG_RESERVED",
    "LR_TYPE_THIS_SERVER_ONLY",
    "LR_TYPE_INTERPRETATION_MASK",
    "MSEC_DIRBIT",
    "MSEC_VAL_MASK",
    "KRB5_PVNO",
    "KRB5_LRQ_NONE",
    "KRB5_LRQ_ALL_LAST_TGT",
    "KRB5_LRQ_ONE_LAST_TGT",
    "KRB5_LRQ_ALL_LAST_INITIAL",
    "KRB5_LRQ_ONE_LAST_INITIAL",
    "KRB5_LRQ_ALL_LAST_TGT_ISSUED",
    "KRB5_LRQ_ONE_LAST_TGT_ISSUED",
    "KRB5_LRQ_ALL_LAST_RENEWAL",
    "KRB5_LRQ_ONE_LAST_RENEWAL",
    "KRB5_LRQ_ALL_LAST_REQ",
    "KRB5_LRQ_ONE_LAST_REQ",
    "KRB5_LRQ_ALL_PW_EXPTIME",
    "KRB5_LRQ_ONE_PW_EXPTIME",
    "KRB5_LRQ_ALL_ACCT_EXPTIME",
    "KRB5_LRQ_ONE_ACCT_EXPTIME",
    "KRB5_PADATA_NONE",
    "KRB5_PADATA_AP_REQ",
    "KRB5_PADATA_ENC_TIMESTAMP",
    "KRB5_PADATA_PW_SALT",
    "KRB5_PADATA_ENC_ENCKEY",
    "KRB5_PADATA_ENC_UNIX_TIME",
    "KRB5_PADATA_ENC_SANDIA_SECURID",
    "KRB5_PADATA_SESAME",
    "KRB5_PADATA_OSF_DCE",
    "KRB5_CYBERSAFE_SECUREID",
    "KRB5_PADATA_AFS3_SALT",
    "KRB5_PADATA_ETYPE_INFO",
    "KRB5_PADATA_SAM_CHALLENGE",
    "KRB5_PADATA_SAM_RESPONSE",
    "KRB5_PADATA_PK_AS_REQ_OLD",
    "KRB5_PADATA_PK_AS_REP_OLD",
    "KRB5_PADATA_PK_AS_REQ",
    "KRB5_PADATA_PK_AS_REP",
    "KRB5_PADATA_ETYPE_INFO2",
    "KRB5_PADATA_USE_SPECIFIED_KVNO",
    "KRB5_PADATA_SVR_REFERRAL_INFO",
    "KRB5_PADATA_SAM_REDIRECT",
    "KRB5_PADATA_GET_FROM_TYPED_DATA",
    "KRB5_PADATA_REFERRAL",
    "KRB5_PADATA_SAM_CHALLENGE_2",
    "KRB5_PADATA_SAM_RESPONSE_2",
    "KRB5_PADATA_PAC_REQUEST",
    "KRB5_PADATA_FOR_USER",
    "KRB5_PADATA_S4U_X509_USER",
    "KRB5_PADATA_AS_CHECKSUM",
    "KRB5_PADATA_FX_COOKIE",
    "KRB5_PADATA_FX_FAST",
    "KRB5_PADATA_FX_ERROR",
    "KRB5_PADATA_ENCRYPTED_CHALLENGE",
    "KRB5_PADATA_OTP_CHALLENGE",
    "KRB5_PADATA_OTP_REQUEST",
    "KRB5_PADATA_OTP_PIN_CHANGE",
    "KRB5_PADATA_PKINIT_KX",
    "KRB5_ENCPADATA_REQ_ENC_PA_REP",
    "KRB5_PADATA_AS_FRESHNESS",
    "KRB5_PADATA_SPAKE",
    "KRB5_PADATA_PAC_OPTIONS",
    "KRB5_SAM_USE_SAD_AS_KEY",
    "KRB5_SAM_SEND_ENCRYPTED_SAD",
    "KRB5_SAM_MUST_PK_ENCRYPT_SAD",
    "KRB5_DOMAIN_X500_COMPRESS",
    "KRB5_ALTAUTH_ATT_CHALLENGE_RESPONSE",
    "KRB5_AUTHDATA_IF_RELEVANT",
    "KRB5_AUTHDATA_KDC_ISSUED",
    "KRB5_AUTHDATA_AND_OR",
    "KRB5_AUTHDATA_MANDATORY_FOR_KDC",
    "KRB5_AUTHDATA_INITIAL_VERIFIED_CAS",
    "KRB5_AUTHDATA_OSF_DCE",
    "KRB5_AUTHDATA_SESAME",
    "KRB5_AUTHDATA_CAMMAC",
    "KRB5_AUTHDATA_WIN2K_PAC",
    "KRB5_AUTHDATA_ETYPE_NEGOTIATION",
    "KRB5_AUTHDATA_SIGNTICKET",
    "KRB5_AUTHDATA_FX_ARMOR",
    "KRB5_AUTHDATA_AUTH_INDICATOR",
    "KRB5_KPASSWD_SUCCESS",
    "KRB5_KPASSWD_MALFORMED",
    "KRB5_KPASSWD_HARDERROR",
    "KRB5_KPASSWD_AUTHERROR",
    "KRB5_KPASSWD_SOFTERROR",
    "KRB5_KPASSWD_ACCESSDENIED",
    "KRB5_KPASSWD_BAD_VERSION",
    "KRB5_KPASSWD_INITIAL_FLAG_NEEDED",
    "KRB5_AUTH_CONTEXT_DO_TIME",
    "KRB5_AUTH_CONTEXT_RET_TIME",
    "KRB5_AUTH_CONTEXT_DO_SEQUENCE",
    "KRB5_AUTH_CONTEXT_RET_SEQUENCE",
    "KRB5_AUTH_CONTEXT_PERMIT_ALL",
    "KRB5_AUTH_CONTEXT_USE_SUBKEY",
    "KRB5_AUTH_CONTEXT_GENERATE_LOCAL_ADDR",
    "KRB5_AUTH_CONTEXT_GENERATE_REMOTE_ADDR",
    "KRB5_AUTH_CONTEXT_GENERATE_LOCAL_FULL_ADDR",
    "KRB5_AUTH_CONTEXT_GENERATE_REMOTE_FULL_ADDR",
    "KRB5_TC_MATCH_TIMES",
    "KRB5_TC_MATCH_IS_SKEY",
    "KRB5_TC_MATCH_FLAGS",
    "KRB5_TC_MATCH_TIMES_EXACT",
    "KRB5_TC_MATCH_FLAGS_EXACT",
    "KRB5_TC_MATCH_AUTHDATA",
    "KRB5_TC_MATCH_SRV_NAMEONLY",
    "KRB5_TC_MATCH_2ND_TKT",
    "KRB5_TC_MATCH_KTYPE",
    "KRB5_TC_SUPPORTED_KTYPES",
    "KRB5_TC_OPENCLOSE",
    "KRB5_TC_NOTICKET",
    "MAX_KEYTAB_NAME_LEN",
    "KRB5_INIT_CONTEXT_SECURE",
    "KRB5_INIT_CONTEXT_KDC",
    "KRB5_GC_USER_USER",
    "KRB5_GC_CACHED",
    "KRB5_GC_CANONICALIZE",
    "KRB5_GC_NO_STORE",
    "KRB5_GC_FORWARDABLE",
    "KRB5_GC_NO_TRANSIT_CHECK",
    "KRB5_GC_CONSTRAINED_DELEGATION",
    "KRB5_PRINCIPAL_PARSE_NO_REALM",
    "KRB5_PRINCIPAL_PARSE_REQUIRE_REALM",
    "KRB5_PRINCIPAL_PARSE_ENTERPRISE",
    "KRB5_PRINCIPAL_PARSE_IGNORE_REALM",
    "KRB5_PRINCIPAL_UNPARSE_SHORT",
    "KRB5_PRINCIPAL_UNPARSE_NO_REALM",
    "KRB5_PRINCIPAL_UNPARSE_DISPLAY",
    "KRB5_PRINCIPAL_COMPARE_IGNORE_REALM",
    "KRB5_PRINCIPAL_COMPARE_ENTERPRISE",
    "KRB5_PRINCIPAL_COMPARE_CASEFOLD",
    "KRB5_PRINCIPAL_COMPARE_UTF8",
    "KRB5_TGS_NAME_SIZE",
    "KRB5_RECVAUTH_SKIP_VERSION",
    "KRB5_RECVAUTH_BADAUTHVERS",
    "KRB5_RESPONDER_OTP_FORMAT_DECIMAL",
    "KRB5_RESPONDER_OTP_FORMAT_HEXADECIMAL",
    "KRB5_RESPONDER_OTP_FORMAT_ALPHANUMERIC",
    "KRB5_RESPONDER_OTP_FLAGS_COLLECT_TOKEN",
    "KRB5_RESPONDER_OTP_FLAGS_COLLECT_PIN",
    "KRB5_RESPONDER_OTP_FLAGS_NEXTOTP",
    "KRB5_RESPONDER_OTP_FLAGS_SEPARATE_PIN",
    "KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_COUNT_LOW",
    "KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_FINAL_TRY",
    "KRB5_RESPONDER_PKINIT_FLAGS_TOKEN_USER_PIN_LOCKED",
    "KRB5_GET_INIT_CREDS_OPT_TKT_LIFE",
    "KRB5_GET_INIT_CREDS_OPT_RENEW_LIFE",
    "KRB5_GET_INIT_CREDS_OPT_FORWARDABLE",
    "KRB5_GET_INIT_CREDS_OPT_PROXIABLE",
    "KRB5_GET_INIT_CREDS_OPT_ETYPE_LIST",
    "KRB5_GET_INIT_CREDS_OPT_ADDRESS_LIST",
    "KRB5_GET_INIT_CREDS_OPT_PREAUTH_LIST",
    "KRB5_GET_INIT_CREDS_OPT_SALT",
    "KRB5_GET_INIT_CREDS_OPT_CHG_PWD_PRMPT",
    "KRB5_GET_INIT_CREDS_OPT_CANONICALIZE",
    "KRB5_GET_INIT_CREDS_OPT_ANONYMOUS",
    "KRB5_FAST_REQUIRED",
    "KRB5_INIT_CREDS_STEP_FLAG_CONTINUE",
    "KRB5_TKT_CREDS_STEP_FLAG_CONTINUE",
    "KRB5_VERIFY_INIT_CREDS_OPT_AP_REQ_NOFAIL",
    "KRB5_PROMPT_TYPE_PASSWORD",
    "KRB5_PROMPT_TYPE_NEW_PASSWORD",
    "KRB5_PROMPT_TYPE_NEW_PASSWORD_AGAIN",
    "KRB5_PROMPT_TYPE_PREAUTH",
    "KRB5_PAC_LOGON_INFO",
    "KRB5_PAC_CREDENTIALS_INFO",
    "KRB5_PAC_SERVER_CHECKSUM",
    "KRB5_PAC_PRIVSVR_CHECKSUM",
    "KRB5_PAC_CLIENT_INFO",
    "KRB5_PAC_DELEGATION_INFO",
    "KRB5_PAC_UPN_DNS_INFO",
    "KRB5KDC_ERR_NONE",
    "KRB5KDC_ERR_NAME_EXP",
    "KRB5KDC_ERR_SERVICE_EXP",
    "KRB5KDC_ERR_BAD_PVNO",
    "KRB5KDC_ERR_C_OLD_MAST_KVNO",
    "KRB5KDC_ERR_S_OLD_MAST_KVNO",
    "KRB5KDC_ERR_C_PRINCIPAL_UNKNOWN",
    "KRB5KDC_ERR_S_PRINCIPAL_UNKNOWN",
    "KRB5KDC_ERR_PRINCIPAL_NOT_UNIQUE",
    "KRB5KDC_ERR_NULL_KEY",
    "KRB5KDC_ERR_CANNOT_POSTDATE",
    "KRB5KDC_ERR_NEVER_VALID",
    "KRB5KDC_ERR_POLICY",
    "KRB5KDC_ERR_BADOPTION",
    "KRB5KDC_ERR_ETYPE_NOSUPP",
    "KRB5KDC_ERR_SUMTYPE_NOSUPP",
    "KRB5KDC_ERR_PADATA_TYPE_NOSUPP",
    "KRB5KDC_ERR_TRTYPE_NOSUPP",
    "KRB5KDC_ERR_CLIENT_REVOKED",
    "KRB5KDC_ERR_SERVICE_REVOKED",
    "KRB5KDC_ERR_TGT_REVOKED",
    "KRB5KDC_ERR_CLIENT_NOTYET",
    "KRB5KDC_ERR_SERVICE_NOTYET",
    "KRB5KDC_ERR_KEY_EXP",
    "KRB5KDC_ERR_PREAUTH_FAILED",
    "KRB5KDC_ERR_PREAUTH_REQUIRED",
    "KRB5KDC_ERR_SERVER_NOMATCH",
    "KRB5KDC_ERR_MUST_USE_USER2USER",
    "KRB5KDC_ERR_PATH_NOT_ACCEPTED",
    "KRB5KDC_ERR_SVC_UNAVAILABLE",
    "KRB5PLACEHOLD_30",
    "KRB5KRB_AP_ERR_BAD_INTEGRITY",
    "KRB5KRB_AP_ERR_TKT_EXPIRED",
    "KRB5KRB_AP_ERR_TKT_NYV",
    "KRB5KRB_AP_ERR_REPEAT",
    "KRB5KRB_AP_ERR_NOT_US",
    "KRB5KRB_AP_ERR_BADMATCH",
    "KRB5KRB_AP_ERR_SKEW",
    "KRB5KRB_AP_ERR_BADADDR",
    "KRB5KRB_AP_ERR_BADVERSION",
    "KRB5KRB_AP_ERR_MSG_TYPE",
    "KRB5KRB_AP_ERR_MODIFIED",
    "KRB5KRB_AP_ERR_BADORDER",
    "KRB5KRB_AP_ERR_ILL_CR_TKT",
    "KRB5KRB_AP_ERR_BADKEYVER",
    "KRB5KRB_AP_ERR_NOKEY",
    "KRB5KRB_AP_ERR_MUT_FAIL",
    "KRB5KRB_AP_ERR_BADDIRECTION",
    "KRB5KRB_AP_ERR_METHOD",
    "KRB5KRB_AP_ERR_BADSEQ",
    "KRB5KRB_AP_ERR_INAPP_CKSUM",
    "KRB5KRB_AP_PATH_NOT_ACCEPTED",
    "KRB5KRB_ERR_RESPONSE_TOO_BIG",
    "KRB5PLACEHOLD_53",
    "KRB5PLACEHOLD_54",
    "KRB5PLACEHOLD_55",
    "KRB5PLACEHOLD_56",
    "KRB5PLACEHOLD_57",
    "KRB5PLACEHOLD_58",
    "KRB5PLACEHOLD_59",
    "KRB5KRB_ERR_GENERIC",
    "KRB5KRB_ERR_FIELD_TOOLONG",
    "KRB5KDC_ERR_CLIENT_NOT_TRUSTED",
    "KRB5KDC_ERR_KDC_NOT_TRUSTED",
    "KRB5KDC_ERR_INVALID_SIG",
    "KRB5KDC_ERR_DH_KEY_PARAMETERS_NOT_ACCEPTED",
    "KRB5KDC_ERR_CERTIFICATE_MISMATCH",
    "KRB5KRB_AP_ERR_NO_TGT",
    "KRB5KDC_ERR_WRONG_REALM",
    "KRB5KRB_AP_ERR_USER_TO_USER_REQUIRED",
    "KRB5KDC_ERR_CANT_VERIFY_CERTIFICATE",
    "KRB5KDC_ERR_INVALID_CERTIFICATE",
    "KRB5KDC_ERR_REVOKED_CERTIFICATE",
    "KRB5KDC_ERR_REVOCATION_STATUS_UNKNOWN",
    "KRB5KDC_ERR_REVOCATION_STATUS_UNAVAILABLE",
    "KRB5KDC_ERR_CLIENT_NAME_MISMATCH",
    "KRB5KDC_ERR_KDC_NAME_MISMATCH",
    "KRB5KDC_ERR_INCONSISTENT_KEY_PURPOSE",
    "KRB5KDC_ERR_DIGEST_IN_CERT_NOT_ACCEPTED",
    "KRB5KDC_ERR_PA_CHECKSUM_MUST_BE_INCLUDED",
    "KRB5KDC_ERR_DIGEST_IN_SIGNED_DATA_NOT_ACCEPTED",
    "KRB5KDC_ERR_PUBLIC_KEY_ENCRYPTION_NOT_SUPPORTED",
    "KRB5PLACEHOLD_82",
    "KRB5PLACEHOLD_83",
    "KRB5PLACEHOLD_84",
    "KRB5KRB_AP_ERR_IAKERB_KDC_NOT_FOUND",
    "KRB5KRB_AP_ERR_IAKERB_KDC_NO_RESPONSE",
    "KRB5PLACEHOLD_87",
    "KRB5PLACEHOLD_88",
    "KRB5PLACEHOLD_89",
    "KRB5KDC_ERR_PREAUTH_EXPIRED",
    "KRB5KDC_ERR_MORE_PREAUTH_DATA_REQUIRED",
    "KRB5PLACEHOLD_92",
    "KRB5KDC_ERR_UNKNOWN_CRITICAL_FAST_OPTION",
    "KRB5PLACEHOLD_94",
    "KRB5PLACEHOLD_95",
    "KRB5PLACEHOLD_96",
    "KRB5PLACEHOLD_97",
    "KRB5PLACEHOLD_98",
    "KRB5PLACEHOLD_99",
    "KRB5KDC_ERR_NO_ACCEPTABLE_KDF",
    "KRB5PLACEHOLD_101",
    "KRB5PLACEHOLD_102",
    "KRB5PLACEHOLD_103",
    "KRB5PLACEHOLD_104",
    "KRB5PLACEHOLD_105",
    "KRB5PLACEHOLD_106",
    "KRB5PLACEHOLD_107",
    "KRB5PLACEHOLD_108",
    "KRB5PLACEHOLD_109",
    "KRB5PLACEHOLD_110",
    "KRB5PLACEHOLD_111",
    "KRB5PLACEHOLD_112",
    "KRB5PLACEHOLD_113",
    "KRB5PLACEHOLD_114",
    "KRB5PLACEHOLD_115",
    "KRB5PLACEHOLD_116",
    "KRB5PLACEHOLD_117",
    "KRB5PLACEHOLD_118",
    "KRB5PLACEHOLD_119",
    "KRB5PLACEHOLD_120",
    "KRB5PLACEHOLD_121",
    "KRB5PLACEHOLD_122",
    "KRB5PLACEHOLD_123",
    "KRB5PLACEHOLD_124",
    "KRB5PLACEHOLD_125",
    "KRB5PLACEHOLD_126",
    "KRB5PLACEHOLD_127",
    "KRB5_ERR_RCSID",
    "KRB5_LIBOS_BADLOCKFLAG",
    "KRB5_LIBOS_CANTREADPWD",
    "KRB5_LIBOS_BADPWDMATCH",
    "KRB5_LIBOS_PWDINTR",
    "KRB5_PARSE_ILLCHAR",
    "KRB5_PARSE_MALFORMED",
    "KRB5_CONFIG_CANTOPEN",
    "KRB5_CONFIG_BADFORMAT",
    "KRB5_CONFIG_NOTENUFSPACE",
    "KRB5_BADMSGTYPE",
    "KRB5_CC_BADNAME",
    "KRB5_CC_UNKNOWN_TYPE",
    "KRB5_CC_NOTFOUND",
    "KRB5_CC_END",
    "KRB5_NO_TKT_SUPPLIED",
    "KRB5KRB_AP_WRONG_PRINC",
    "KRB5KRB_AP_ERR_TKT_INVALID",
    "KRB5_PRINC_NOMATCH",
    "KRB5_KDCREP_MODIFIED",
    "KRB5_KDCREP_SKEW",
    "KRB5_IN_TKT_REALM_MISMATCH",
    "KRB5_PROG_ETYPE_NOSUPP",
    "KRB5_PROG_KEYTYPE_NOSUPP",
    "KRB5_WRONG_ETYPE",
    "KRB5_PROG_SUMTYPE_NOSUPP",
    "KRB5_REALM_UNKNOWN",
    "KRB5_SERVICE_UNKNOWN",
    "KRB5_KDC_UNREACH",
    "KRB5_NO_LOCALNAME",
    "KRB5_MUTUAL_FAILED",
    "KRB5_RC_TYPE_EXISTS",
    "KRB5_RC_MALLOC",
    "KRB5_RC_TYPE_NOTFOUND",
    "KRB5_RC_UNKNOWN",
    "KRB5_RC_REPLAY",
    "KRB5_RC_IO",
    "KRB5_RC_NOIO",
    "KRB5_RC_PARSE",
    "KRB5_RC_IO_EOF",
    "KRB5_RC_IO_MALLOC",
    "KRB5_RC_IO_PERM",
    "KRB5_RC_IO_IO",
    "KRB5_RC_IO_UNKNOWN",
    "KRB5_RC_IO_SPACE",
    "KRB5_TRANS_CANTOPEN",
    "KRB5_TRANS_BADFORMAT",
    "KRB5_LNAME_CANTOPEN",
    "KRB5_LNAME_NOTRANS",
    "KRB5_LNAME_BADFORMAT",
    "KRB5_CRYPTO_INTERNAL",
    "KRB5_KT_BADNAME",
    "KRB5_KT_UNKNOWN_TYPE",
    "KRB5_KT_NOTFOUND",
    "KRB5_KT_END",
    "KRB5_KT_NOWRITE",
    "KRB5_KT_IOERR",
    "KRB5_NO_TKT_IN_RLM",
    "KRB5DES_BAD_KEYPAR",
    "KRB5DES_WEAK_KEY",
    "KRB5_BAD_ENCTYPE",
    "KRB5_BAD_KEYSIZE",
    "KRB5_BAD_MSIZE",
    "KRB5_CC_TYPE_EXISTS",
    "KRB5_KT_TYPE_EXISTS",
    "KRB5_CC_IO",
    "KRB5_FCC_PERM",
    "KRB5_FCC_NOFILE",
    "KRB5_FCC_INTERNAL",
    "KRB5_CC_WRITE",
    "KRB5_CC_NOMEM",
    "KRB5_CC_FORMAT",
    "KRB5_CC_NOT_KTYPE",
    "KRB5_INVALID_FLAGS",
    "KRB5_NO_2ND_TKT",
    "KRB5_NOCREDS_SUPPLIED",
    "KRB5_SENDAUTH_BADAUTHVERS",
    "KRB5_SENDAUTH_BADAPPLVERS",
    "KRB5_SENDAUTH_BADRESPONSE",
    "KRB5_SENDAUTH_REJECTED",
    "KRB5_PREAUTH_BAD_TYPE",
    "KRB5_PREAUTH_NO_KEY",
    "KRB5_PREAUTH_FAILED",
    "KRB5_RCACHE_BADVNO",
    "KRB5_CCACHE_BADVNO",
    "KRB5_KEYTAB_BADVNO",
    "KRB5_PROG_ATYPE_NOSUPP",
    "KRB5_RC_REQUIRED",
    "KRB5_ERR_BAD_HOSTNAME",
    "KRB5_ERR_HOST_REALM_UNKNOWN",
    "KRB5_SNAME_UNSUPP_NAMETYPE",
    "KRB5KRB_AP_ERR_V4_REPLY",
    "KRB5_REALM_CANT_RESOLVE",
    "KRB5_TKT_NOT_FORWARDABLE",
    "KRB5_FWD_BAD_PRINCIPAL",
    "KRB5_GET_IN_TKT_LOOP",
    "KRB5_CONFIG_NODEFREALM",
    "KRB5_SAM_UNSUPPORTED",
    "KRB5_SAM_INVALID_ETYPE",
    "KRB5_SAM_NO_CHECKSUM",
    "KRB5_SAM_BAD_CHECKSUM",
    "KRB5_KT_NAME_TOOLONG",
    "KRB5_KT_KVNONOTFOUND",
    "KRB5_APPL_EXPIRED",
    "KRB5_LIB_EXPIRED",
    "KRB5_CHPW_PWDNULL",
    "KRB5_CHPW_FAIL",
    "KRB5_KT_FORMAT",
    "KRB5_NOPERM_ETYPE",
    "KRB5_CONFIG_ETYPE_NOSUPP",
    "KRB5_OBSOLETE_FN",
    "KRB5_EAI_FAIL",
    "KRB5_EAI_NODATA",
    "KRB5_EAI_NONAME",
    "KRB5_EAI_SERVICE",
    "KRB5_ERR_NUMERIC_REALM",
    "KRB5_ERR_BAD_S2K_PARAMS",
    "KRB5_ERR_NO_SERVICE",
    "KRB5_CC_READONLY",
    "KRB5_CC_NOSUPP",
    "KRB5_DELTAT_BADFORMAT",
    "KRB5_PLUGIN_NO_HANDLE",
    "KRB5_PLUGIN_OP_NOTSUPP",
    "KRB5_ERR_INVALID_UTF8",
    "KRB5_ERR_FAST_REQUIRED",
    "KRB5_LOCAL_ADDR_REQUIRED",
    "KRB5_REMOTE_ADDR_REQUIRED",
    "KRB5_TRACE_NOSUPP",
    "KRB5_PLUGIN_VER_NOTSUPP",
    "KRB5_PLUGIN_BAD_MODULE_SPEC",
    "KRB5_PLUGIN_NAME_NOTFOUND",
    "KRB5KDC_ERR_DISCARD",
    "KRB5_DCC_CANNOT_CREATE",
    "KRB5_KCC_INVALID_ANCHOR",
    "KRB5_KCC_UNKNOWN_VERSION",
    "KRB5_KCC_INVALID_UID",
    "KRB5_KCM_MALFORMED_REPLY",
    "KRB5_KCM_RPC_ERROR",
    "KRB5_KCM_REPLY_TOO_BIG",
    "KRB5_KCM_NO_SERVER",
    "KRB5_KDB_RCSID",
    "KRB5_KDB_INUSE",
    "KRB5_KDB_UK_SERROR",
    "KRB5_KDB_UK_RERROR",
    "KRB5_KDB_UNAUTH",
    "KRB5_KDB_NOENTRY",
    "KRB5_KDB_ILL_WILDCARD",
    "KRB5_KDB_DB_INUSE",
    "KRB5_KDB_DB_CHANGED",
    "KRB5_KDB_TRUNCATED_RECORD",
    "KRB5_KDB_RECURSIVELOCK",
    "KRB5_KDB_NOTLOCKED",
    "KRB5_KDB_BADLOCKMODE",
    "KRB5_KDB_DBNOTINITED",
    "KRB5_KDB_DBINITED",
    "KRB5_KDB_ILLDIRECTION",
    "KRB5_KDB_NOMASTERKEY",
    "KRB5_KDB_BADMASTERKEY",
    "KRB5_KDB_INVALIDKEYSIZE",
    "KRB5_KDB_CANTREAD_STORED",
    "KRB5_KDB_BADSTORED_MKEY",
    "KRB5_KDB_NOACTMASTERKEY",
    "KRB5_KDB_KVNONOMATCH",
    "KRB5_KDB_STORED_MKEY_NOTCURRENT",
    "KRB5_KDB_CANTLOCK_DB",
    "KRB5_KDB_DB_CORRUPT",
    "KRB5_KDB_BAD_VERSION",
    "KRB5_KDB_BAD_SALTTYPE",
    "KRB5_KDB_BAD_ENCTYPE",
    "KRB5_KDB_BAD_CREATEFLAGS",
    "KRB5_KDB_NO_PERMITTED_KEY",
    "KRB5_KDB_NO_MATCHING_KEY",
    "KRB5_KDB_DBTYPE_NOTFOUND",
    "KRB5_KDB_DBTYPE_NOSUP",
    "KRB5_KDB_DBTYPE_INIT",
    "KRB5_KDB_SERVER_INTERNAL_ERR",
    "KRB5_KDB_ACCESS_ERROR",
    "KRB5_KDB_INTERNAL_ERROR",
    "KRB5_KDB_CONSTRAINT_VIOLATION",
    "KRB5_LOG_CONV",
    "KRB5_LOG_UNSTABLE",
    "KRB5_LOG_CORRUPT",
    "KRB5_LOG_ERROR",
    "KRB5_KDB_DBTYPE_MISMATCH",
    "KRB5_KDB_POLICY_REF",
    "KRB5_KDB_STRINGS_TOOLONG",
    "KV5M_NONE",
    "KV5M_PRINCIPAL",
    "KV5M_DATA",
    "KV5M_KEYBLOCK",
    "KV5M_CHECKSUM",
    "KV5M_ENCRYPT_BLOCK",
    "KV5M_ENC_DATA",
    "KV5M_CRYPTOSYSTEM_ENTRY",
    "KV5M_CS_TABLE_ENTRY",
    "KV5M_CHECKSUM_ENTRY",
    "KV5M_AUTHDATA",
    "KV5M_TRANSITED",
    "KV5M_ENC_TKT_PART",
    "KV5M_TICKET",
    "KV5M_AUTHENTICATOR",
    "KV5M_TKT_AUTHENT",
    "KV5M_CREDS",
    "KV5M_LAST_REQ_ENTRY",
    "KV5M_PA_DATA",
    "KV5M_KDC_REQ",
    "KV5M_ENC_KDC_REP_PART",
    "KV5M_KDC_REP",
    "KV5M_ERROR",
    "KV5M_AP_REQ",
    "KV5M_AP_REP",
    "KV5M_AP_REP_ENC_PART",
    "KV5M_RESPONSE",
    "KV5M_SAFE",
    "KV5M_PRIV",
    "KV5M_PRIV_ENC_PART",
    "KV5M_CRED",
    "KV5M_CRED_INFO",
    "KV5M_CRED_ENC_PART",
    "KV5M_PWD_DATA",
    "KV5M_ADDRESS",
    "KV5M_KEYTAB_ENTRY",
    "KV5M_CONTEXT",
    "KV5M_OS_CONTEXT",
    "KV5M_ALT_METHOD",
    "KV5M_ETYPE_INFO_ENTRY",
    "KV5M_DB_CONTEXT",
    "KV5M_AUTH_CONTEXT",
    "KV5M_KEYTAB",
    "KV5M_RCACHE",
    "KV5M_CCACHE",
    "KV5M_PREAUTH_OPS",
    "KV5M_SAM_CHALLENGE",
    "KV5M_SAM_CHALLENGE_2",
    "KV5M_SAM_KEY",
    "KV5M_ENC_SAM_RESPONSE_ENC",
    "KV5M_ENC_SAM_RESPONSE_ENC_2",
    "KV5M_SAM_RESPONSE",
    "KV5M_SAM_RESPONSE_2",
    "KV5M_PREDICTED_SAM_RESPONSE",
    "KV5M_PASSWD_PHRASE_ELEMENT",
    "KV5M_GSS_OID",
    "KV5M_GSS_QUEUE",
    "KV5M_FAST_ARMORED_REQ",
    "KV5M_FAST_REQ",
    "KV5M_FAST_RESPONSE",
    "KV5M_AUTHDATA_CONTEXT",
    "KRB524_BADKEY",
    "KRB524_BADADDR",
    "KRB524_BADPRINC",
    "KRB524_BADREALM",
    "KRB524_V4ERR",
    "KRB524_ENCFULL",
    "KRB524_DECEMPTY",
    "KRB524_NOTRESP",
    "KRB524_KRB4_DISABLED",
    "ASN1_BAD_TIMEFORMAT",
    "ASN1_MISSING_FIELD",
    "ASN1_MISPLACED_FIELD",
    "ASN1_TYPE_MISMATCH",
    "ASN1_OVERFLOW",
    "ASN1_OVERRUN",
    "ASN1_BAD_ID",
    "ASN1_BAD_LENGTH",
    "ASN1_BAD_FORMAT",
    "ASN1_PARSE_ERROR",
    "ASN1_BAD_GMTIME",
    "ASN1_MISMATCH_INDEF",
    "ASN1_MISSING_EOC",
    "ASN1_OMITTED",
]
krb5_get_credentials_for_user = _libraries["libkrb5.so"].krb5_get_credentials_for_user
krb5_get_credentials_for_user.restype = krb5_error_code
krb5_get_credentials_for_user.argtypes = (
    krb5_context,
    krb5_flags,
    krb5_ccache,
    POINTER_T(struct__krb5_creds),
    POINTER_T(struct__krb5_data),
    POINTER_T(POINTER_T(struct__krb5_creds)),
)

KRB5_TGS_NAME = "krbtgt"

__all__ += ["krb5_get_credentials_for_user", "KRB5_TGS_NAME"]
