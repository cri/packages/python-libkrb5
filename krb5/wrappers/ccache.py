from .. import lib
from ..utils import to_string
from .base import Krb5WrapperWithContext
from .principal import Krb5PrincipalWrapper
from .creds import Krb5StaticCredsWrapper, Krb5CredsWrapper
from .data import Krb5StaticDataWrapper
from . import exceptions

import ctypes


class Krb5CCacheFullNameWrapper(Krb5WrapperWithContext):
    free_func = lib.krb5_free_string

    def __init__(self, *args, obj=None, **kwargs):
        if obj is None:
            obj = ctypes.POINTER(ctypes.c_char)()
        super().__init__(*args, obj=obj, **kwargs)

    def __str__(self):
        return to_string(self.object)


class Krb5CCacheCursorWrapper(Krb5WrapperWithContext):
    free_func = None

    def __init__(self, *args, cache, **kwargs):
        self._cache = cache
        cache.add_ref(self)
        super().__init__(*args, context=cache.context, obj=None, **kwargs)
        self.restart()

    def __next__(self):
        cred = Krb5StaticCredsWrapper(context=self.context)
        try:
            self.context.check_code(
                code=lib.krb5_cc_next_cred(
                    self.context.object,
                    self.cache.object,
                    ctypes.byref(self.object),
                    cred.object,
                )
            )
        except exceptions.Krb5CcEnd as e:
            raise StopIteration from e
        return cred

    @property
    def cache(self):
        return self._cache

    def restart(self):
        self.end()
        obj = lib.krb5_cc_cursor()
        self.context.check_code(
            lib.krb5_cc_start_seq_get(
                self.context.object,
                self.cache.object,
                ctypes.byref(obj),
            )
        )
        self._obj = obj

    def end(self):
        if self._obj is not None:
            self.context.check_code(
                lib.krb5_cc_end_seq_get(
                    self.context.object,
                    self.cache.object,
                    ctypes.byref(self.object),
                )
            )
            self._obj = None

    def _free(self):
        self.end()


class Krb5CCacheWrapper(Krb5WrapperWithContext):
    free_func = lib.krb5_cc_close

    @classmethod
    def default(cls, context):
        cache = lib.krb5_ccache()
        context.check_code(lib.krb5_cc_default(context.object, ctypes.byref(cache)))
        return cls(context=context, obj=cache)

    @classmethod
    def resolve(cls, context, name):
        cache = lib.krb5_ccache()
        context.check_code(
            lib.krb5_cc_resolve(
                context.object, bytes(name, "utf-8"), ctypes.byref(cache)
            ),
        )
        return cls(context=context, obj=cache)

    @classmethod
    def new_unique(cls, context, ccache_type):
        cache = lib.krb5_ccache()
        context.check_code(
            lib.krb5_cc_new_unique(
                context.object,
                bytes(ccache_type, "utf-8"),
                None,  # hint (unused)
                ctypes.byref(cache),
            ),
        )
        return cls(context=context, obj=cache)

    @property
    def name(self):
        return to_string(lib.krb5_cc_get_name(self.context.object, self.object))

    @property
    def type(self):
        return to_string(lib.krb5_cc_get_type(self.context.object, self.object))

    @property
    def full_name(self):
        full_name = Krb5CCacheFullNameWrapper(context=self.context)
        self.context.check_code(
            lib.krb5_cc_get_full_name(
                self.context.object,
                self.object,
                ctypes.byref(full_name.object),
            )
        )
        return str(full_name)

    @property
    def default_principal(self):
        principal = lib.krb5_principal()
        self.context.check_code(
            lib.krb5_cc_get_principal(
                self.context.object,
                self.object,
                ctypes.byref(principal),
            )
        )
        return Krb5PrincipalWrapper(context=self.context, obj=principal)

    def __str__(self):
        return self.full_name

    def __iter__(self):
        return Krb5CCacheCursorWrapper(cache=self)

    def initialize(self, default_principal):
        default_principal = self.context.to_principal(default_principal)
        self.context.check_code(
            lib.krb5_cc_initialize(
                self.context.object,
                self.object,
                default_principal.object,
            )
        )

    def destroy(self):
        self.context.check_code(
            lib.krb5_cc_destroy(
                self.context.object,
                self.object,
            )
        )
        # krb5_cc_drstroy closes the underlying object
        self.free_refs()
        self._obj = None

    def copy_creds(self, dst_cache):
        self.context.check_code(
            lib.krb5_cc_copy_creds(
                self.context.object,
                self.object,
                dst_cache.object,
            )
        )

    def store_cred(self, cred):
        self.context.check_code(
            lib.krb5_cc_store_cred(
                self.context.object,
                self.object,
                cred.object,
            )
        )

    def remove_cred(self, cred, flags=0):
        self.context.check_code(
            lib.krb5_cc_remove_cred(
                self.context.object,
                self.object,
                flags,
                cred.object,
            )
        )

    def get_config(self, key, principal=None):
        principal = self.context.to_principal(principal)
        data = Krb5StaticDataWrapper(context=self.context)
        self.context.check_code(
            lib.krb5_cc_get_config(
                self.context.object,
                self.object,
                principal.object if principal is not None else None,
                bytes(key, "utf-8"),
                data.object,
            )
        )
        return data

    def set_config(self, key, principal=None, data=None):
        principal = self.context.to_principal(principal)
        data = self.context.to_data(data)
        self.context.check_code(
            lib.krb5_cc_set_config(
                self.context.object,
                self.object,
                principal.object if principal is not None else None,
                bytes(key, "utf-8"),
                data.object if data is not None else None,
            )
        )

    def get_credentials(self, *, in_creds, options=0):
        out_creds = ctypes.POINTER(lib.krb5_creds)()
        self.context.check_code(
            lib.krb5_get_credentials(
                self.context.object,
                options,
                self.object,
                in_creds.object,
                ctypes.byref(out_creds),
            )
        )
        return Krb5CredsWrapper(context=self.context, obj=out_creds)

    def get_credentials_for_user(self, *, in_creds, options=0, subject_cert=None):
        out_creds = ctypes.POINTER(lib.krb5_creds)()
        self.context.check_code(
            lib.krb5_get_credentials_for_user(
                self.context.object,
                options,
                self.object,
                in_creds.object,
                subject_cert.object if subject_cert is not None else None,
                ctypes.byref(out_creds),
            )
        )
        return Krb5CredsWrapper(context=self.context, obj=out_creds)
