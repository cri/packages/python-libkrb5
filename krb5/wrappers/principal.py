from .. import lib
from ..utils import to_string
from .base import Krb5WrapperWithContext

import ctypes


class Krb5UnparsedNameWrapper(Krb5WrapperWithContext):
    free_func = lib.krb5_free_unparsed_name

    @classmethod
    def unparse(cls, context, principal):
        obj = ctypes.POINTER(ctypes.c_char)()
        context.check_code(
            lib.krb5_unparse_name(
                context.object,
                principal.object,
                ctypes.byref(obj),
            )
        )
        return cls(context=context, obj=obj)

    def __str__(self):
        return to_string(self.object)


class Krb5PrincipalWrapper(Krb5WrapperWithContext):
    free_func = lib.krb5_free_principal

    @classmethod
    def parse(cls, context, name):
        principal = lib.krb5_principal()
        context.check_code(
            lib.krb5_parse_name(
                context.object,
                bytes(name, "utf-8"),
                ctypes.byref(principal),
            )
        )
        return cls(context=context, obj=principal)

    @property
    def realm(self):
        from .data import Krb5StaticDataStringWrapper

        return str(
            Krb5StaticDataStringWrapper(
                context=self.context,
                parent=self,
                obj=self.object.contents.realm,
            )
        )

    @realm.setter
    def realm(self, value):
        value = to_string(value)
        self.context.check_code(
            lib.krb5_set_principal_realm(
                self.context.object,
                self.object,
                value,
            )
        )
        return value

    def __str__(self):
        return str(self.unparse())

    def unparse(self):
        return Krb5UnparsedNameWrapper.unparse(self.context, self)

    def is_config(self):
        return lib.krb5_is_config_principal(
            self.context.object,
            self.object,
        )

    def copy(self, principal=None):
        if principal is None:
            obj = lib.krb5_principal()
            principal = Krb5PrincipalWrapper(context=self.context, obj=obj)
        else:
            principal._free()  # pylint: disable=protected-access

        self.context.check_code(
            lib.krb5_copy_principal(
                self.context.object,
                self.object,
                ctypes.byref(principal.object),
            )
        )

        return principal

    def compare(self, principal):
        principal = self.context.to_principal(principal)
        return lib.krb5_principal_compare(
            self.context.object,
            self.object,
            principal.object,
        )


class Krb5StaticPrincipalWrapper(Krb5PrincipalWrapper):
    free_func = None

    def __init__(self, *args, obj=None, **kwargs):
        if obj is None:
            obj = lib.krb5_principal()
            ctypes.memset(ctypes.byref(obj), 0, ctypes.sizeof(obj))
        super().__init__(*args, obj=obj, **kwargs)
