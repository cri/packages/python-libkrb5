import weakref
import ctypes


class Krb5Wrapper:
    _obj = None
    _refs = None
    _data = None
    free_func = None

    class FreedObject(RuntimeError):
        pass

    def __init__(self, obj, parent=None):
        self._obj = obj
        self._refs = set()
        self._data = {}
        self._parent = parent
        if parent is not None:
            parent.add_ref(self)
            self.add_data(parent)

    def __del__(self):
        self.free()

    def add_data(self, data, key=None):
        old = self._data.get(key)
        if key is None:
            self._data.setdefault(None, []).append(data)
        else:
            self._data[key] = data
        return old

    def get_data(self, key=None):
        return self._data.get(key)

    def add_ref(self, obj):
        self._refs.add(weakref.ref(obj))

    def del_ref(self, obj):
        self._refs = set(ref for ref in self._refs if ref() is not obj)

    def free_refs(self):
        while self._refs:
            obj = self._refs.pop()()
            if obj is not None:
                obj.free()

    def replace(self, obj):
        if self._obj is not None:
            self._free()
        ctypes.memmove(
            ctypes.byref(self.object),
            ctypes.byref(obj),
            ctypes.sizeof(self.object),
        )

    def replace_contents(self, obj):
        if self._obj is not None:
            self._free()
        ctypes.memmove(
            ctypes.byref(self.object.contents),
            ctypes.byref(obj),
            ctypes.sizeof(self.object.contents),
        )

    def _free(self):
        if self.free_func is not None:
            # pylint: disable=not-callable
            return self.free_func(self.object)
        return None

    def free(self):
        if self._obj is not None:
            self._free()
        self.free_refs()
        self._data = {}
        self._obj = None
        self._parent = None

    @property
    def object(self):
        if self._obj is None:
            raise self.FreedObject(type(self).__name__)
        return self._obj


class Krb5WrapperWithContext(Krb5Wrapper):
    def __init__(self, context, *args, **kwargs):
        self._context = context
        super().__init__(*args, **kwargs)
        context.wrapper.add_ref(self)

    @property
    def context(self):
        return self._context

    def _free(self):
        if self.free_func is not None:
            # pylint: disable=not-callable
            return self.free_func(self.context.object, self.object)
        return None
