BUILD_DIR := build
BLACK_FLAGS = -q

LIB_BUILD_PATH := $(BUILD_DIR)/lib.py
LIB_PATH := krb5/lib.py

EXCEPTIONS_PATH := krb5/wrappers/exceptions.py
EXCEPTIONS_GENERATOR = ./gen_exceptions.py

EXTRA_CDECL_PATH := krb5/extra_cdecl.ctypes

MACRO_WKRND_PATH := $(BUILD_DIR)/macro_wkrnd.ctypes
MACRO_WKRND_RGXP := '.*\#define\s+([_A-Z0-9]+)\s+\(?(-?0[xX][0-9a-fA-F]+|-?[0-9]+)L?\)?.*'
MACRO_WKRND_RGXPR_DECL := '\1 = \2'
MACRO_WKRND_RGXPR_NAME := '\1'

all: $(LIB_PATH) $(EXCEPTIONS_PATH)

$(EXCEPTIONS_PATH): $(LIB_PATH) $(EXCEPTIONS_GENERATOR)
	$(EXCEPTIONS_GENERATOR) | black $(BLACK_FLAGS) - > "$@"

$(LIB_PATH): $(LIB_BUILD_PATH) $(MACRO_WKRND_PATH) $(EXTRA_CDECL_PATH)
	cat $^ | black $(BLACK_FLAGS) - > "$@"

$(LIB_BUILD_PATH): /usr/include/krb5/krb5.h Makefile
	mkdir -p "$(BUILD_DIR)"
	clang2py --clang-args "-I /usr/include/" -l libkrb5.so --kind cdefstu \
		$< -o "$@"

$(MACRO_WKRND_PATH): /usr/include/krb5/krb5.h Makefile
	sed -nE s/$(MACRO_WKRND_RGXP)/$(MACRO_WKRND_RGXPR_DECL)/p "$<" > "$@"
	echo "__all__ += [" >> "$@"
	sed -nE s/$(MACRO_WKRND_RGXP)/$(MACRO_WKRND_RGXPR_NAME)/p "$<" | \
		xargs -n 1 -I {} echo '"{}",' >> "$@"
	echo "]" >> "$@"


distclean: clean
	rm -f $(LIB_BUILD_PATH)

clean:
	rm -f $(LIB_PATH) $(EXCEPTIONS_PATH) $(MACRO_WKRND_PATH)

.PHONY: all clean distclean
